CONNECT TO WIS;

DELETE FROM WIS.Erststimmen WHERE jahr = 2009;
DELETE FROM WIS.Zweitstimmen WHERE jahr = 2009;
DELETE FROM WIS.ZweitstimmenPartei WHERE jahr = 2009;

INSERT INTO WIS.Erststimmen (
	SELECT wahlkreis, direktkandidat, jahr, count(*)
	FROM WIS.Wahlzettel w
	GROUP BY wahlkreis, direktkandidat, jahr
);

INSERT INTO WIS.Zweitstimmen (
	SELECT Wahlkreis, landesliste, jahr, count(*)
	FROM wis.Wahlzettel w
	GROUP BY Wahlkreis, jahr, landesliste
);

INSERT INTO WIS.ZweitstimmenPartei (
	SELECT p.id, z.jahr, sum(stimmen)
	FROM WIS.Zweitstimmen z, WIS.Partei p, WIS.Landesliste l
	WHERE p.id = l.partei AND l.id = z.landesliste
	GROUP BY z.jahr, p.id
	UNION
	SELECT null, z.jahr, sum(stimmen)
	FROM WIS.Zweitstimmen z
	WHERE landesliste is null
	GROUP BY z.jahr
);

REFRESH TABLE WIS.SITZVERTEILUNG;
REFRESH TABLE WIS.KNAPPSTEGEWINNER;
REFRESH TABLE WIS.SITZEPARTEI;
REFRESH TABLE WIS.ZweitstimmenSieger;
REFRESH TABLE WIS.ANZUEBERHANGMANDATE;
