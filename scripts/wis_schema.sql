-- This CLP file was created using DB2LOOK Version "9.7" 
-- Timestamp: Sun 05 Feb 2012 03:12:14 AM CET
-- Database Name: WIS            
-- Database Manager Version: DB2/LINUX Version 9.7.4       
-- Database Codepage: 1208
-- Database Collating Sequence is: IDENTITY


CONNECT TO WIS;



------------------------------------
-- DDL Statements for TABLESPACES --
------------------------------------


CREATE LARGE TABLESPACE "SYSTOOLSPACE" IN DATABASE PARTITION GROUP IBMCATGROUP 
	 PAGESIZE 4096 MANAGED BY AUTOMATIC STORAGE 
	 AUTORESIZE YES 
	 INITIALSIZE 32 M 
	 MAXSIZE NONE 
	 EXTENTSIZE 4
	 PREFETCHSIZE AUTOMATIC
	 BUFFERPOOL IBMDEFAULTBP
	 OVERHEAD 7.500000
	 TRANSFERRATE 0.060000 
	 NO FILE SYSTEM CACHING  
	 DROPPED TABLE RECOVERY ON;


CREATE USER TEMPORARY TABLESPACE "SYSTOOLSTMPSPACE" IN DATABASE PARTITION GROUP IBMCATGROUP 
	 PAGESIZE 4096 MANAGED BY AUTOMATIC STORAGE 
	 EXTENTSIZE 4
	 PREFETCHSIZE AUTOMATIC
	 BUFFERPOOL IBMDEFAULTBP
	 OVERHEAD 7.500000
	 TRANSFERRATE 0.060000 
	 FILE SYSTEM CACHING  
	 DROPPED TABLE RECOVERY OFF;


-- Mimic tablespace

ALTER TABLESPACE SYSCATSPACE
      PREFETCHSIZE AUTOMATIC
      OVERHEAD 7.500000
      NO FILE SYSTEM CACHING 
      AUTORESIZE YES 
      TRANSFERRATE 0.060000;


ALTER TABLESPACE TEMPSPACE1
      PREFETCHSIZE AUTOMATIC
      OVERHEAD 7.500000
      FILE SYSTEM CACHING 
      TRANSFERRATE 0.060000;


ALTER TABLESPACE USERSPACE1
      PREFETCHSIZE AUTOMATIC
      OVERHEAD 7.500000
      NO FILE SYSTEM CACHING 
      AUTORESIZE YES 
      TRANSFERRATE 0.060000;




------------------------------------------------
-- DDL Statements for table "WIS     "."PARTEI"
------------------------------------------------
 

CREATE TABLE "WIS     "."PARTEI"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "NAME" VARCHAR(50) NOT NULL )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."PARTEI"

ALTER TABLE "WIS     "."PARTEI" 
	ADD CONSTRAINT "CC1321106543531" PRIMARY KEY
		("ID");



------------------------------------------------
-- DDL Statements for table "WIS     "."BUNDESLAND"
------------------------------------------------
 

CREATE TABLE "WIS     "."BUNDESLAND"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "NAME" VARCHAR(50) NOT NULL )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."BUNDESLAND"

ALTER TABLE "WIS     "."BUNDESLAND" 
	ADD CONSTRAINT "CC1321107170553" PRIMARY KEY
		("ID");


-- DDL Statements for unique constraints on Table "WIS     "."BUNDESLAND"


ALTER TABLE "WIS     "."BUNDESLAND" 
	ADD CONSTRAINT "CC1321107953993" UNIQUE
		("NAME");


------------------------------------------------
-- DDL Statements for table "WIS     "."DIREKTKANDIDAT"
------------------------------------------------
 

CREATE TABLE "WIS     "."DIREKTKANDIDAT"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "BEWERBER" INTEGER NOT NULL , 
		  "WAHLKREIS" INTEGER NOT NULL , 
		  "JAHR" DECIMAL(4,0) NOT NULL )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."DIREKTKANDIDAT"

ALTER TABLE "WIS     "."DIREKTKANDIDAT" 
	ADD CONSTRAINT "CC1321216162630" PRIMARY KEY
		("ID");


-- DDL Statements for unique constraints on Table "WIS     "."DIREKTKANDIDAT"


ALTER TABLE "WIS     "."DIREKTKANDIDAT" 
	ADD CONSTRAINT "CC1321216191321" UNIQUE
		("BEWERBER",
		 "JAHR");


-- DDL Statements for indexes on Table "WIS     "."DIREKTKANDIDAT"

CREATE UNIQUE INDEX "WIS     "."DK_UNIQUE" ON "WIS     "."DIREKTKANDIDAT" 
		("WAHLKREIS" ASC,
		 "JAHR" ASC,
		 "BEWERBER" ASC)
		PCTFREE 10 CLUSTER 
		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."DIREKTKANDIDAT"

CREATE INDEX "WIS     "."IDX1112050950110" ON "WIS     "."DIREKTKANDIDAT" 
		("WAHLKREIS" ASC,
		 "JAHR" ASC,
		 "ID" ASC)
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."DIREKTKANDIDAT"

CREATE UNIQUE INDEX "WIS     "."IDX1112050950310" ON "WIS     "."DIREKTKANDIDAT" 
		("ID" ASC)
		INCLUDE ("WAHLKREIS" ,
		 "JAHR" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."DIREKTKANDIDAT"

CREATE UNIQUE INDEX "WIS     "."IDX1112050950340" ON "WIS     "."DIREKTKANDIDAT" 
		("ID" ASC)
		INCLUDE ("JAHR" ,
		 "BEWERBER" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."LANDESLISTE"
------------------------------------------------
 

CREATE TABLE "WIS     "."LANDESLISTE"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "PARTEI" INTEGER NOT NULL , 
		  "BUNDESLAND" INTEGER NOT NULL , 
		  "JAHR" DECIMAL(4,0) NOT NULL )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."LANDESLISTE"

ALTER TABLE "WIS     "."LANDESLISTE" 
	ADD CONSTRAINT "CC1321295044391" PRIMARY KEY
		("ID");


-- DDL Statements for unique constraints on Table "WIS     "."LANDESLISTE"


ALTER TABLE "WIS     "."LANDESLISTE" 
	ADD CONSTRAINT "CC1321302067705" UNIQUE
		("PARTEI",
		 "BUNDESLAND",
		 "JAHR");


-- DDL Statements for indexes on Table "WIS     "."LANDESLISTE"

CREATE INDEX "WIS     "."IDX1112050951000" ON "WIS     "."LANDESLISTE" 
		("JAHR" ASC,
		 "PARTEI" ASC,
		 "ID" ASC)
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LANDESLISTE"

CREATE INDEX "WIS     "."IDX1112050951100" ON "WIS     "."LANDESLISTE" 
		("JAHR" ASC,
		 "ID" DESC)
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LANDESLISTE"

CREATE UNIQUE INDEX "WIS     "."IDX1112050951150" ON "WIS     "."LANDESLISTE" 
		("ID" ASC)
		INCLUDE ("JAHR" ,
		 "PARTEI" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LANDESLISTE"

CREATE UNIQUE INDEX "WIS     "."IDX1112050951570" ON "WIS     "."LANDESLISTE" 
		("PARTEI" ASC,
		 "BUNDESLAND" ASC,
		 "JAHR" ASC)
		INCLUDE ("ID" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LANDESLISTE"

CREATE INDEX "WIS     "."LANDESLISTE_PARTEI" ON "WIS     "."LANDESLISTE" 
		("PARTEI" ASC,
		 "JAHR" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."LISTENKANDIDAT"
------------------------------------------------
 

CREATE TABLE "WIS     "."LISTENKANDIDAT"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "BEWERBER" INTEGER NOT NULL , 
		  "LANDESLISTE" INTEGER NOT NULL , 
		  "RANG" INTEGER NOT NULL )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."LISTENKANDIDAT"

ALTER TABLE "WIS     "."LISTENKANDIDAT" 
	ADD CONSTRAINT "CC1321295294931" PRIMARY KEY
		("ID");



-- DDL Statements for indexes on Table "WIS     "."LISTENKANDIDAT"

CREATE INDEX "WIS     "."IDX1112050952080" ON "WIS     "."LISTENKANDIDAT" 
		("LANDESLISTE" ASC,
		 "RANG" ASC,
		 "ID" ASC,
		 "BEWERBER" ASC)
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LISTENKANDIDAT"

CREATE UNIQUE INDEX "WIS     "."IDX1112050952150" ON "WIS     "."LISTENKANDIDAT" 
		("ID" ASC)
		INCLUDE ("BEWERBER" ,
		 "LANDESLISTE" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."LISTENKANDIDAT"

CREATE UNIQUE INDEX "WIS     "."LL_UNIQUE" ON "WIS     "."LISTENKANDIDAT" 
		("LANDESLISTE" ASC,
		 "RANG" ASC)
		PCTFREE 10 CLUSTER 
		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."BEWERBER"
------------------------------------------------
 

CREATE TABLE "WIS     "."BEWERBER"  (
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "NAME" VARCHAR(100) NOT NULL , 
		  "BERUF" VARCHAR(100) NOT NULL , 
		  "GEBURTSJAHR" DECIMAL(4,0) NOT NULL , 
		  "VORNAME" VARCHAR(100) NOT NULL , 
		  "TITEL" VARCHAR(16) )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."BEWERBER"

ALTER TABLE "WIS     "."BEWERBER" 
	ADD CONSTRAINT "CC1321106478374" PRIMARY KEY
		("ID");


-- DDL Statements for unique constraints on Table "WIS     "."BEWERBER"


ALTER TABLE "WIS     "."BEWERBER" 
	ADD CONSTRAINT "CC1321109041518" UNIQUE
		("NAME",
		 "GEBURTSJAHR",
		 "VORNAME");


-- DDL Statements for indexes on Table "WIS     "."BEWERBER"

CREATE UNIQUE INDEX "WIS     "."IDX1112050953230" ON "WIS     "."BEWERBER" 
		("ID" ASC)
		INCLUDE ("TITEL" ,
		 "VORNAME" ,
		 "GEBURTSJAHR" ,
		 "BERUF" ,
		 "NAME" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."PARTEIZUGEHOERIGKEIT"
------------------------------------------------
 

CREATE TABLE "WIS     "."PARTEIZUGEHOERIGKEIT"  (
		  "BEWERBER" INTEGER NOT NULL , 
		  "PARTEI" INTEGER NOT NULL , 
		  "JAHR" DECIMAL(4,0) NOT NULL WITH DEFAULT 0000 , 
		  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +2147483647  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

ALTER TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" 
	ADD CONSTRAINT "PK_ID" PRIMARY KEY
		("ID");



-- DDL Statements for indexes on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

CREATE INDEX "WIS     "."IDX1112050951410" ON "WIS     "."PARTEIZUGEHOERIGKEIT" 
		("BEWERBER" ASC,
		 "JAHR" ASC,
		 "PARTEI" ASC)
		
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

CREATE INDEX "WIS     "."PZ" ON "WIS     "."PARTEIZUGEHOERIGKEIT" 
		("PARTEI" ASC,
		 "BEWERBER" ASC,
		 "JAHR" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

CREATE UNIQUE INDEX "WIS     "."PZ_UNIQUE" ON "WIS     "."PARTEIZUGEHOERIGKEIT" 
		("BEWERBER" ASC,
		 "JAHR" ASC,
		 "PARTEI" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."WAHLKREIS"
------------------------------------------------
 

CREATE TABLE "WIS     "."WAHLKREIS"  (
		  "ID" INTEGER NOT NULL , 
		  "NAME" VARCHAR(100) NOT NULL , 
		  "BUNDESLAND" INTEGER NOT NULL , 
		  "FLAECHE" BIGINT NOT NULL , 
		  "BEVOELKERUNG" BIGINT NOT NULL , 
		  "WAHLBERECHTIGTE" INTEGER NOT NULL WITH DEFAULT 0 )   
		 IN "USERSPACE1" ; 


-- DDL Statements for primary key on Table "WIS     "."WAHLKREIS"

ALTER TABLE "WIS     "."WAHLKREIS" 
	ADD CONSTRAINT "CC1321107118819" PRIMARY KEY
		("ID");


-- DDL Statements for unique constraints on Table "WIS     "."WAHLKREIS"


ALTER TABLE "WIS     "."WAHLKREIS" 
	ADD CONSTRAINT "CC1321107973312" UNIQUE
		("NAME",
		 "BUNDESLAND");


-- DDL Statements for indexes on Table "WIS     "."WAHLKREIS"

CREATE UNIQUE INDEX "WIS     "."IDX1112050951380" ON "WIS     "."WAHLKREIS" 
		("ID" ASC)
		INCLUDE ("BUNDESLAND" )
		
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."WAHLZETTEL"
------------------------------------------------
 

CREATE TABLE "WIS     "."WAHLZETTEL"  (
		  "ID" BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +9223372036854775807  
		    NO CYCLE  
		    NO CACHE  
		    NO ORDER ) , 
		  "DIREKTKANDIDAT" BIGINT , 
		  "LANDESLISTE" BIGINT , 
		  "JAHR" SMALLINT NOT NULL , 
		  "WAHLKREIS" BIGINT )   
		 IN "USERSPACE1" ; 






-- DDL Statements for indexes on Table "WIS     "."WAHLZETTEL"

CREATE INDEX "WIS     "."DIREKTKANDIDAT" ON "WIS     "."WAHLZETTEL" 
		("DIREKTKANDIDAT" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."WAHLZETTEL"

CREATE INDEX "WIS     "."ERSTSTIMMEN" ON "WIS     "."WAHLZETTEL" 
		("JAHR" ASC,
		 "WAHLKREIS" ASC,
		 "DIREKTKANDIDAT" ASC)
		PCTFREE 10 CLUSTER 
		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."WAHLZETTEL"

CREATE INDEX "WIS     "."ZWEITSTIMMEN" ON "WIS     "."WAHLZETTEL" 
		("LANDESLISTE" ASC,
		 "WAHLKREIS" ASC,
		 "JAHR" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."DIVISOR"
------------------------------------------------
 

CREATE TABLE "WIS     "."DIVISOR"  (
		  "DIVISOR" INTEGER NOT NULL )   
		 IN "USERSPACE1" ; 






------------------------------------------------
-- DDL Statements for table "WIS     "."ERSTSTIMMEN"
------------------------------------------------
 

CREATE TABLE "WIS     "."ERSTSTIMMEN"  (
		  "WAHLKREIS" INTEGER NOT NULL , 
		  "DIREKTKANDIDAT" INTEGER , 
		  "JAHR" SMALLINT NOT NULL , 
		  "STIMMEN" BIGINT NOT NULL )   
		 IN "USERSPACE1" ; 






-- DDL Statements for indexes on Table "WIS     "."ERSTSTIMMEN"

CREATE INDEX "SQLJ    "."ERSTSTIMMEN_STIMMEN" ON "WIS     "."ERSTSTIMMEN" 
		("STIMMEN" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."ERSTSTIMMEN"

CREATE INDEX "WIS     "."ERSTSTIMMEN_DK" ON "WIS     "."ERSTSTIMMEN" 
		("DIREKTKANDIDAT" ASC,
		 "JAHR" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "WIS     "."ERSTSTIMMEN"

CREATE UNIQUE INDEX "WIS     "."ERSTSTIMMEN_UNIQUE" ON "WIS     "."ERSTSTIMMEN" 
		("WAHLKREIS" ASC,
		 "JAHR" ASC,
		 "DIREKTKANDIDAT" ASC)
		PCTFREE 10 MINPCTUSED 10

		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."ZWEITSTIMMEN"
------------------------------------------------
 

CREATE TABLE "WIS     "."ZWEITSTIMMEN"  (
		  "WAHLKREIS" INTEGER NOT NULL , 
		  "LANDESLISTE" INTEGER , 
		  "JAHR" SMALLINT NOT NULL , 
		  "STIMMEN" BIGINT NOT NULL )   
		 IN "USERSPACE1" ; 






-- DDL Statements for indexes on Table "WIS     "."ZWEITSTIMMEN"

CREATE UNIQUE INDEX "WIS     "."ZWEITSTIMMEN_UNIQUE" ON "WIS     "."ZWEITSTIMMEN" 
		("LANDESLISTE" ASC,
		 "WAHLKREIS" ASC,
		 "JAHR" ASC)
		PCTFREE 10 CLUSTER 
		COLLECT SAMPLED DETAILED STATISTICS 
		COMPRESS NO ALLOW REVERSE SCANS;

------------------------------------------------
-- DDL Statements for table "WIS     "."ZWEITSTIMMENPARTEI"
------------------------------------------------
 

CREATE TABLE "WIS     "."ZWEITSTIMMENPARTEI"  (
		  "PARTEI" INTEGER , 
		  "JAHR" SMALLINT NOT NULL , 
		  "STIMMEN" BIGINT NOT NULL )   
		 IN "USERSPACE1" ; 






------------------------------------------------
-- DDL Statements for table "WIS     "."SITZVERTEILUNG"
------------------------------------------------
 SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";

create table WIS.Sitzverteilung (Bewerber, Jahr, Mandatsart) AS ( SELECT * FROM WIS.Abgeordnete WHERE Jahr = 2009) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN "USERSPACE1" ;







------------------------------------------------
-- DDL Statements for table "WIS     "."ANZUEBERHANGMANDATE"
------------------------------------------------
 SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";

CREATE TABLE WIS.ANZUEBERHANGMANDATE (landesliste, ueberhang) as ( SELECT sl.landesliste, -(sl.sitze - dm.direktmandate) as ueberhang FROM wis.sitzelandesliste_2009 sl left join wis.direktmandatelandesliste dm on sl.landesliste = dm.landesliste WHERE (sl.sitze - dm.direktmandate) < 0 AND dm.landesliste is not null ) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN "USERSPACE1" ;







------------------------------------------------
-- DDL Statements for table "WIS     "."WAEHLER"
------------------------------------------------
 

CREATE TABLE "WIS     "."WAEHLER"  (
		  "ID" BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (  
		    START WITH +0  
		    INCREMENT BY +1  
		    MINVALUE +0  
		    MAXVALUE +9223372036854775807  
		    NO CYCLE  
		    CACHE 100  
		    NO ORDER ) , 
		  "NAME" VARCHAR(100) NOT NULL , 
		  "VORNAME" VARCHAR(100) NOT NULL , 
		  "GEBURTSTAG" DATE NOT NULL , 
		  "TOKEN" VARCHAR(128) NOT NULL , 
		  "ABGESTIMMT" TIMESTAMP , 
		  "WAHLKREIS" INTEGER NOT NULL )   
		 IN "USERSPACE1"  
		 ORGANIZE BY ( 
		  ( "WAHLKREIS" ) ) 
		 ; 


-- DDL Statements for primary key on Table "WIS     "."WAEHLER"

ALTER TABLE "WIS     "."WAEHLER" 
	ADD CONSTRAINT "CC1326531436136" PRIMARY KEY
		("ID");



ALTER TABLE "WIS     "."WAEHLER" ALTER COLUMN "ID" RESTART WITH 398;

------------------------------------------------
-- DDL Statements for table "WIS     "."ZWEITSTIMMENSIEGER"
------------------------------------------------
 SET CURRENT SCHEMA = "DB2INST1";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";

create table WIS.ZweitstimmenSieger (Jahr, Wahlkreis, Landesliste) AS ( SELECT ll.Jahr, wk.Id, ll.Id FROM WIS.Wahlkreis wk join WIS.Zweitstimmen zs on (wk.Id = zs.Wahlkreis) JOIN WIS.Landesliste ll on (zs.Landesliste = ll.Id) WHERE zs.stimmen = ( SELECT max(stimmen) FROM WIS.Zweitstimmen zs1 WHERE zs1.jahr = zs.jahr AND zs1.wahlkreis = zs.wahlkreis ) ) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN "USERSPACE1" ;







------------------------------------------------
-- DDL Statements for table "WIS     "."SITZEPARTEI"
------------------------------------------------
 SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";

create table WIS.SitzePartei (Partei, Jahr, Sitze) AS ( SELECT partei, jahr, count(*) as sitze FROM (SELECT partei, jahr, cast (stimmen as double) / divisor as rang FROM wis.zweitstimmenpartei_sperrklausel zsp, wis.divisor WHERE Jahr = 2009 ORDER BY rang desc LIMIT 598) zwerg GROUP BY partei, jahr ) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN "USERSPACE1" ;







------------------------------------------------
-- DDL Statements for table "WIS     "."KNAPPSTEGEWINNER"
------------------------------------------------
 SET CURRENT SCHEMA = "DB2INST1";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";

CREATE table WIS.KNAPPSTEGEWINNER AS (with vorsprung as (SELECT dk.*, case when stimmen is null then 0 else stimmen end - (SELECT max(stimmen) as max FROM wis.erststimmen es2 WHERE es2.wahlkreis = dk.wahlkreis AND es2.direktkandidat <> dk.id AND es2.direktkandidat is not null AND es2.jahr = dk.jahr) as vorsprung FROM wis.direktkandidat dk left join wis.erststimmen es on dk.id = es.direktkandidat WHERE dk.jahr = 2009), rang as (SELECT v.*, pz.partei, RANK() OVER (PARTITION BY pz.partei, pz.jahr ORDER BY case when vorsprung >= 0 then vorsprung else (SELECT max(vorsprung) FROM vorsprung) - vorsprung end ASC) as rang FROM vorsprung v left join wis.parteizugehoerigkeit pz on v.jahr = pz.jahr AND v.bewerber = pz.bewerber) SELECT * FROM rang WHERE rang <= 10) DATA INITIALLY DEFERRED REFRESH DEFERRED ENABLE QUERY OPTIMIZATION MAINTAINED BY SYSTEM IN "USERSPACE1" ;







-- DDL Statements for foreign keys on Table "WIS     "."DIREKTKANDIDAT"

ALTER TABLE "WIS     "."DIREKTKANDIDAT" 
	ADD CONSTRAINT "CC1321216260294" FOREIGN KEY
		("BEWERBER")
	REFERENCES "WIS     "."BEWERBER"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."DIREKTKANDIDAT" 
	ADD CONSTRAINT "CC1321216268117" FOREIGN KEY
		("WAHLKREIS")
	REFERENCES "WIS     "."WAHLKREIS"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."DIREKTKANDIDAT"

ALTER TABLE "WIS     "."DIREKTKANDIDAT" 
	ADD CONSTRAINT "JAHR" CHECK 
		(JAHR = 2005 or JAHR =2009)
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."LANDESLISTE"

ALTER TABLE "WIS     "."LANDESLISTE" 
	ADD CONSTRAINT "CC1321295057081" FOREIGN KEY
		("PARTEI")
	REFERENCES "WIS     "."PARTEI"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."LANDESLISTE" 
	ADD CONSTRAINT "CC1321295066160" FOREIGN KEY
		("BUNDESLAND")
	REFERENCES "WIS     "."BUNDESLAND"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."LANDESLISTE"

ALTER TABLE "WIS     "."LANDESLISTE" 
	ADD CONSTRAINT "CC1321295084773" CHECK 
		(JAHR = 2005 or JAHR = 2009)
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."LISTENKANDIDAT"

ALTER TABLE "WIS     "."LISTENKANDIDAT" 
	ADD CONSTRAINT "CC1321295300394" FOREIGN KEY
		("BEWERBER")
	REFERENCES "WIS     "."BEWERBER"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."LISTENKANDIDAT" 
	ADD CONSTRAINT "CC1321295304629" FOREIGN KEY
		("LANDESLISTE")
	REFERENCES "WIS     "."LANDESLISTE"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

ALTER TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" 
	ADD CONSTRAINT "CC1321106844540" FOREIGN KEY
		("BEWERBER")
	REFERENCES "WIS     "."BEWERBER"
		("ID")
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" 
	ADD CONSTRAINT "CC1321106895090" FOREIGN KEY
		("PARTEI")
	REFERENCES "WIS     "."PARTEI"
		("ID")
	ON DELETE RESTRICT
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."PARTEIZUGEHOERIGKEIT"

ALTER TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" 
	ADD CONSTRAINT "JAHR" CHECK 
		(JAHR = 2005 or JAHR = 2009)
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."WAHLKREIS"

ALTER TABLE "WIS     "."WAHLKREIS" 
	ADD CONSTRAINT "CC1321107355124" FOREIGN KEY
		("BUNDESLAND")
	REFERENCES "WIS     "."BUNDESLAND"
		("ID")
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."ERSTSTIMMEN"

ALTER TABLE "WIS     "."ERSTSTIMMEN" 
	ADD CONSTRAINT "CC1325955238884" FOREIGN KEY
		("DIREKTKANDIDAT")
	REFERENCES "WIS     "."DIREKTKANDIDAT"
		("ID")
	ON DELETE CASCADE
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."ERSTSTIMMEN" 
	ADD CONSTRAINT "CC1325955262621" FOREIGN KEY
		("WAHLKREIS")
	REFERENCES "WIS     "."WAHLKREIS"
		("ID")
	ON DELETE CASCADE
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."ERSTSTIMMEN"

ALTER TABLE "WIS     "."ERSTSTIMMEN" 
	ADD CONSTRAINT "CC1325955343725" CHECK 
		(JAHR in (2005, 2009))
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."ZWEITSTIMMEN"

ALTER TABLE "WIS     "."ZWEITSTIMMEN" 
	ADD CONSTRAINT "CC1325959983353" FOREIGN KEY
		("WAHLKREIS")
	REFERENCES "WIS     "."WAHLKREIS"
		("ID")
	ON DELETE CASCADE
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

ALTER TABLE "WIS     "."ZWEITSTIMMEN" 
	ADD CONSTRAINT "CC1325960001721" FOREIGN KEY
		("LANDESLISTE")
	REFERENCES "WIS     "."LANDESLISTE"
		("ID")
	ON DELETE CASCADE
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."ZWEITSTIMMEN"

ALTER TABLE "WIS     "."ZWEITSTIMMEN" 
	ADD CONSTRAINT "CC1325960020505" CHECK 
		(JAHR in (2005, 2009))
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."ZWEITSTIMMENPARTEI"

ALTER TABLE "WIS     "."ZWEITSTIMMENPARTEI" 
	ADD CONSTRAINT "CC1325963850808" FOREIGN KEY
		("PARTEI")
	REFERENCES "WIS     "."PARTEI"
		("ID")
	ON DELETE CASCADE
	ON UPDATE RESTRICT
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "WIS     "."ZWEITSTIMMENPARTEI"

ALTER TABLE "WIS     "."ZWEITSTIMMENPARTEI" 
	ADD CONSTRAINT "CC1325963876393" CHECK 
		(JAHR in (2005, 2009))
	ENFORCED
	ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "WIS     "."WAEHLER"

ALTER TABLE "WIS     "."WAEHLER" 
	ADD CONSTRAINT "FK_WAHLKREIS" FOREIGN KEY
		("WAHLKREIS")
	REFERENCES "WIS     "."WAHLKREIS"
		("ID")
	ON DELETE CASCADE
	ON UPDATE NO ACTION
	ENFORCED
	ENABLE QUERY OPTIMIZATION;






----------------------------

-- DDL Statements for Views

----------------------------
SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.ZWEITSTIMMENPARTEI_SPERRKLAUSEL (partei, jahr, stimmen)
AS SELECT * FROM wis.zweitstimmenpartei zsp WHERE partei is not null and
stimmen >= (SELECT 0.05 * sum(Stimmen) FROM wis.zweitstimmenpartei zsp2
WHERE partei is not null AND zsp2.jahr = zsp.jahr);


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
create view wis.SitzePartei_2009 as ( SELECT partei, count(*) as sitze FROM
(SELECT partei, cast (stimmen as double) / divisor as rang FROM wis.zweitstimmenpartei_sperrklausel
zsp, wis.divisor WHERE JAHR=2009 ORDER BY rang desc LIMIT 598) zwerg GROUP
BY partei);


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.LANDESLISTESTIMMEN (landesliste, stimmen) AS  SELECT landesliste,
sum(stimmen) FROM wis.zweitstimmen zs, wis.divisor d group by landesliste;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.SITZELANDESLISTE_2009 (landesliste, sitze) AS SELECT landesliste,
count(*) FROM wis.sitzepartei_2009 sp, ( SELECT partei, landesliste, cast
(stimmen as double) / divisor as erg, ROW_NUMBER() OVER(PARTITION BY partei
ORDER BY cast (stimmen as double) / divisor DESC) AS rownumber FROM wis.landeslistestimmen
ls left join wis.landesliste ll on id=landesliste, wis.divisor d WHERE
jahr = 2009 ) zwerg WHERE sp.partei = zwerg.partei AND zwerg.rownumber
<= sp.sitze GROUP BY landesliste;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.WKZWEITSTIMMENVERGLEICH AS SELECT z.wahlkreis, ll.partei,
z.stimmen as stimmen2009, z2.stimmen as stimmen2005, z.stimmen - z2.stimmen
as differenz FROM wis.zweitstimmen z join wis.landesliste ll on z.landesliste
= ll.id, wis.zweitstimmen z2 join wis.landesliste ll2 on z2.landesliste
= ll2.id WHERE z.wahlkreis = z2.wahlkreis and ll.partei = ll2.partei and
ll.jahr = 2009 and ll2.jahr = 2005;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.DIREKTMANDAT (wahlkreis, direktkandidat, jahr, stimmen)
as SELECT dk1.wahlkreis, min(dk1.id) as direktkandidat, dk1.jahr, stimmen
FROM wis.erststimmen es1 left join wis.direktkandidat dk1 on dk1.id = es1.direktkandidat
WHERE stimmen = (SELECT max(stimmen) from wis.erststimmen es2 left join
wis.direktkandidat dk2 on dk2.id = es2.direktkandidat where dk2.wahlkreis
= dk1.wahlkreis and dk1.jahr = dk2.jahr) GROUP BY dk1.wahlkreis, stimmen,
dk1.jahr;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.DIREKTMANDATELANDESLISTE (landesliste, direktmandate) AS
SELECT ll.id as landeliste, count(*) as direktmandate FROM wis.direktmandat
dm left join wis.direktkandidat dk on dk.id = dm.direktkandidat left join
wis.bewerber b on dk.bewerber = b.id left join wis.parteizugehoerigkeit
pz on pz.bewerber = b.id and pz.jahr = dk.jahr left join wis.wahlkreis
wk on dm.wahlkreis = wk.id inner join wis.landesliste ll on ll.bundesland
= wk.bundesland and ll.partei = pz.partei and ll.jahr = dk.jahr GROUP BY
ll.id;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.LISTENKANDIDATENLANDESLISTE (landesliste, sitze) AS SELECT
sl.landesliste, case when dm.landesliste is null then sl.sitze else (case
when (sl.sitze - dm.direktmandate) < 0 then 0 else (sl.sitze - dm.direktmandate)
end) end as sitze FROM wis.sitzelandesliste_2009 sl left join wis.direktmandatelandesliste
dm on sl.landesliste = dm.landesliste;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.LISTENMANDATELANDESLISTE (landesliste, sitze) AS SELECT
sl.landesliste, case when dm.landesliste is null then sl.sitze else (case
when (sl.sitze - dm.direktmandate) < 0 then 0 else (sl.sitze - dm.direktmandate)
end) end as sitze FROM wis.sitzelandesliste_2009 sl left join wis.direktmandatelandesliste
dm on sl.landesliste = dm.landesliste;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.POSSIBLE_LISTENMANDAT (id, bewerber, landesliste, rang)
AS SELECT lk.id, lk.bewerber, lk.landesliste, lk.rang FROM (( SELECT bewerber,
jahr FROM wis.listenkandidat lk left join wis.landesliste ll on lk.landesliste
= ll.id ) minus ( SELECT bewerber, dk.jahr FROM wis.direktmandat dm left
join wis.direktkandidat dk on dm.direktkandidat = dk.id )) zwerg LEFT JOIN
(wis.listenkandidat lk left join wis.landesliste ll on ll.id = lk.landesliste)
on zwerg.bewerber = lk.bewerber AND zwerg.jahr = ll.jahr;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.LISTENMANDAT (listenkandidat, landesliste) AS  SELECT listenkandidat,
landesliste FROM ( SELECT sl.landesliste, sitze, plm.id as listenkandidat,
ROW_NUMBER() OVER (PARTITION BY plm.landesliste ORDER BY rang ASC) as rownumber
FROM wis.listenmandatelandesliste sl, wis.possible_listenmandat plm WHERE
sl.landesliste = plm.landesliste) zwerg WHERE rownumber <= sitze;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.MITGLIED_DES_BUNDESTAGES AS SELECT b.* FROM ( (SELECT bewerber
FROM wis.listenmandat lm left join wis.listenkandidat lk on lm.listenkandidat
= lk.id left join wis.landesliste ll on ll.id = lk.landesliste WHERE jahr
= 2009) UNION ALL (SELECT bewerber FROM wis.direktmandat dm left join wis.direktkandidat
dk on dm.direktkandidat = dk.id WHERE dk.jahr = 2009)  )mp left join wis.bewerber
b on mp.bewerber = b.id;


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.ABGEORDNETE (Bewerber, Jahr, Mandatsart) AS (SELECT lk.bewerber,
ll.jahr, 'LM' FROM wis.listenmandat lm left join wis.listenkandidat lk
on lm.listenkandidat = lk.id left join wis.landesliste ll on ll.id = lk.landesliste)
UNION ALL (SELECT dk.bewerber, dk.jahr, 'DM' FROM wis.direktmandat dm left
join wis.direktkandidat dk on dm.direktkandidat = dk.id);


SET CURRENT SCHEMA = "WIS     ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","WIS";
CREATE view WIS.UEBERHANGMANDATE_PARTEI (Partei, Jahr, Ueberhang) as SELECT
p.id, jahr, sum(ueberhang) as ueberhang  FROM wis.anzueberhangmandate ue
LEFT JOIN wis.landesliste ll ON ll.id = ue.landesliste LEFT JOIN wis.partei
p ON ll.partei = p.id  GROUP BY p.id, jahr;


SET CURRENT SCHEMA = "DB2INST1";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";
CREATE view WIS.ZWEITSTIMMEN_DIREKT AS SELECT Wahlkreis, landesliste, jahr,
count(*)  as stimmen  FROM wis.Wahlzettel w   GROUP BY Wahlkreis, jahr,
landesliste;


SET CURRENT SCHEMA = "DB2INST1";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";
CREATE view WIS.ERSTSTIMMEN_DIREKT AS  SELECT wahlkreis, direktkandidat,
jahr, count(*)  as stimmen  FROM WIS.Wahlzettel w   GROUP BY wahlkreis,
direktkandidat, jahr;


SET CURRENT SCHEMA = "DB2INST1";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","DB2INST1";
CREATE view WIS.ZWEITSTIMMENPARTEI_DIREKT AS  SELECT p.id, z.jahr, sum(stimmen)
 as stimmen  FROM WIS.Zweitstimmen_direkt z, WIS.Partei p, WIS.Landesliste
l   WHERE p.id = l.partei AND l.id = z.landesliste   GROUP BY z.jahr, p.id;




--------------------------------------------
-- Authorization Statements on Tables/Views 
--------------------------------------------

 
GRANT SELECT ON TABLE "WIS     "."ABGEORDNETE" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."ABGEORDNETE" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."ANZUEBERHANGMANDATE" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ANZUEBERHANGMANDATE" TO USER "DB2INST1" ;

GRANT CONTROL ON TABLE "WIS     "."BEWERBER" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."BEWERBER" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."BUNDESLAND" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."BUNDESLAND" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."DIREKTKANDIDAT" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."DIREKTKANDIDAT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."DIREKTMANDAT" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."DIREKTMANDAT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."DIREKTMANDATELANDESLISTE" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."DIVISOR" TO USER "WIS     " ;

GRANT DELETE ON TABLE "WIS     "."DIVISOR" TO USER "DB2INST1" ;

GRANT INSERT ON TABLE "WIS     "."DIVISOR" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."DIVISOR" TO USER "DB2INST1" ;

GRANT UPDATE ON TABLE "WIS     "."DIVISOR" TO USER "DB2INST1" ;

GRANT DELETE ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "DB2INST1" ;

GRANT INSERT ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "DB2INST1" ;

GRANT UPDATE ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "DB2INST1" ;

GRANT DELETE ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "WIS     " ;

GRANT INSERT ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "WIS     " ;

GRANT UPDATE ON TABLE "WIS     "."ERSTSTIMMEN" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ERSTSTIMMEN_DIREKT" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."KNAPPSTEGEWINNER" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."LANDESLISTE" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."LANDESLISTE" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."LANDESLISTESTIMMEN" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."LISTENKANDIDAT" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."LISTENKANDIDAT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."LISTENKANDIDATENLANDESLISTE" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."LISTENMANDAT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."LISTENMANDATELANDESLISTE" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."MITGLIED_DES_BUNDESTAGES" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."PARTEI" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."PARTEI" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."PARTEIZUGEHOERIGKEIT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."POSSIBLE_LISTENMANDAT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."SITZELANDESLISTE_2009" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."SITZEPARTEI" TO USER "WIS     " ;

GRANT INSERT ON TABLE "WIS     "."SITZEPARTEI" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."SITZEPARTEI" TO USER "DB2INST1" ;

GRANT UPDATE ON TABLE "WIS     "."SITZEPARTEI" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."SITZEPARTEI_2009" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."SITZEPARTEI_2009" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."SITZVERTEILUNG" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."SITZVERTEILUNG" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."UEBERHANGMANDATE_PARTEI" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."UEBERHANGMANDATE_PARTEI" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."WAEHLER" TO USER "WIS     " ;

GRANT UPDATE ON TABLE "WIS     "."WAEHLER" TO USER "WIS     " ;

GRANT DELETE ON TABLE "WIS     "."WAEHLER" TO USER "DB2INST1" ;

GRANT INSERT ON TABLE "WIS     "."WAEHLER" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."WAEHLER" TO USER "DB2INST1" ;

GRANT UPDATE ON TABLE "WIS     "."WAEHLER" TO USER "DB2INST1" ;

GRANT CONTROL ON TABLE "WIS     "."WAHLKREIS" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."WAHLKREIS" TO USER "WIS     " ;

GRANT INSERT ON TABLE "WIS     "."WAHLZETTEL" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."WAHLZETTEL" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."WKZWEITSTIMMENVERGLEICH" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."WKZWEITSTIMMENVERGLEICH" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMEN" TO USER "DB2INST1" ;

GRANT DELETE ON TABLE "WIS     "."ZWEITSTIMMEN" TO USER "WIS     " ;

GRANT INSERT ON TABLE "WIS     "."ZWEITSTIMMEN" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMEN" TO USER "WIS     " ;

GRANT UPDATE ON TABLE "WIS     "."ZWEITSTIMMEN" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI" TO USER "DB2INST1" ;

GRANT DELETE ON TABLE "WIS     "."ZWEITSTIMMENPARTEI" TO USER "WIS     " ;

GRANT INSERT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI" TO USER "WIS     " ;

GRANT UPDATE ON TABLE "WIS     "."ZWEITSTIMMENPARTEI" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI_DIREKT" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI_SPERRKLAUSEL" TO USER "DB2INST1" ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMENPARTEI_SPERRKLAUSEL" TO USER "WIS     " ;

GRANT CONTROL ON TABLE "WIS     "."ZWEITSTIMMENSIEGER" TO USER "WIS     " ;

GRANT SELECT ON TABLE "WIS     "."ZWEITSTIMMEN_DIREKT" TO USER "WIS     " ;

----------------------------------------
-- Authorization Statements on Database 
----------------------------------------

 
GRANT CONNECT ON DATABASE  TO USER "WIS     " ;

---------------------------------------
-- Authorization statement on table space 
---------------------------------------

 
GRANT USE OF TABLESPACE "SYSTOOLSTMPSPACE" TO  PUBLIC   ;

COMMIT WORK;

CONNECT RESET;

TERMINATE;

