package edu.tum.cs.wis.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import edu.tum.cs.wis.dao.DVoteDAO;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Bewerber;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.IErststimmen;
import edu.tum.cs.wis.model.IZweitstimmenLandesliste;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Wahlkreis;
import edu.tum.cs.wis.model.ZweitstimmenSieger;

@ManagedBean(name = "wahlkreisBean")
@SessionScoped
public class WahlkreisBean {
	private final Logger log = Logger.getLogger(WahlkreisBean.class);

	private List<Wahlkreis> wahlkreise;
	private List<Direktkandidat> direktkandidaten;
	private List<Direktkandidat> wahlkreisDirektkandidaten;
	private List<Landesliste> wahlkreisLandeslisten;

	private String selectedId;
	private String zweitstimmenCacheSelectedId, erststimmenCacheSelectedId;
	private Wahlkreis selected;
	private int totalSecondVotes;
	private int totalFirstVotes;
	
	private List<IZweitstimmenLandesliste> zweitstimmenCache;
	private List<IErststimmen> erststimmenCache;
	
	private Map<Bewerber, Integer> erststimmen2005;
	private Map<Bewerber, Integer> erststimmen2009;
	private Map<Partei, Integer> zweitstimmen2005;
	private Map<Partei, Integer> zweitstimmen2009;
	
	private Map<Integer, Double> wahlbeteiligung = new HashMap<Integer, Double>(2);
	private List<ZweitstimmenSieger> zweitstimmenSieger2009;
	
	private boolean useAggregatedResults = true;

	private VoteDAO db;
	
	@Inject
	private VoteDAO aggregatedDAO;
	
	@Inject @DVoteDAO
	private VoteDAO directDAO;
	
	
	@PostConstruct
	public void init(){
		// Use aggregated results
		db = aggregatedDAO;
	}
	
	/**
	 * Return how the results are calculated (using aggregated tables or not)
	 * @return true if aggregated results are used
	 */
	public boolean getUsingAggregatedResults(){
		return useAggregatedResults;
	}
	
	/**
	 * Switch calculation method, base on aggregated results / direct calculation
	 */
	public void processSwitchEvent() {
		useAggregatedResults = !useAggregatedResults;
		if (useAggregatedResults)
			db = aggregatedDAO;
		else
			db = directDAO;
		if (useAggregatedResults)
			log.info("Using aggregated DAO");
		else
			log.info("Using direct DAO");

		clear();
	}
	
	private void clear() {
		wahlbeteiligung = new HashMap<Integer, Double>(2);
		erststimmenCache = null;
		zweitstimmenCache = null;
		erststimmenCacheSelectedId = null;
		zweitstimmenCacheSelectedId = null;
		wahlkreisDirektkandidaten = null;
		wahlkreisLandeslisten = null;
		erststimmen2005 = null;
		erststimmen2009 = null;
		zweitstimmen2005 = null;
		zweitstimmen2009 = null;
		
		Collections.sort(getWahlkreisLandeslisten(), new Comparator<Landesliste>() {
			@Override
			public int compare(Landesliste l1, Landesliste l2) {
				final Map<Partei, Integer> results = getZweitstimmen2009();
				final Partei p1 = l1.getPartei();
				final Partei p2 = l2.getPartei();
				
				if (p1 == null || p2 == null) {
					return 0;
				}
				return results.get(p1).compareTo(results.get(p2));
			}
		});
	}

	public String loadWahlkreis() {
		log.info("loadWahlkreis(), selectedId=" + selectedId);
		if (selectedId == null) {
			return "pretty:home";
		}
		try {
			long id = Long.parseLong(selectedId);
			selected = db.getWahlkreis(id);
			
			if (selected == null) {
				return "pretty:home";
			}
			
			clear();
			return null;
		} catch (NumberFormatException e) {
			return "pretty:home";
		}
	}

	public List<IErststimmen> getErststimmen() {
		// Avoid bug of accordion causing several queries when closing a panel
		if(selectedId == erststimmenCacheSelectedId){
			return erststimmenCache;
		}
		
		erststimmenCache = db.fetchErststimmen(selected, 2009);
		erststimmenCacheSelectedId = selectedId;

		totalFirstVotes = 0;
		for (IErststimmen zs : erststimmenCache) {
			totalFirstVotes += zs.getStimmen();
		}

		return erststimmenCache;
	}
	
	private List<IErststimmen> getErststimmen(int year) {
		return db.fetchErststimmen(selected, year);
	}

	public double getWahlbeteiligung(int year) {
		if (!wahlbeteiligung.containsKey(year)) {
			wahlbeteiligung.put(year, db.getWahlbeteiligung(selected, year));
		}
		return wahlbeteiligung.get(year);
	}
	
	public double getWahlbeteiligungsDelta() {
		return getWahlbeteiligung(2009) - getWahlbeteiligung(2005);
	}

	public List<IZweitstimmenLandesliste> getZweitstimmen() {
		// Avoid bug of accordion causing several queries when closing a panel
		if(selectedId == zweitstimmenCacheSelectedId){
			return zweitstimmenCache;
		}
		
		
		zweitstimmenCache = db.fetchZweitstimmen(selected, 2009);
		zweitstimmenCacheSelectedId = selectedId;

		totalSecondVotes = 0;
		for (IZweitstimmenLandesliste zs : zweitstimmenCache) {
			totalSecondVotes += zs.getStimmen();
		}

		return zweitstimmenCache;
	}
	
	private List<IZweitstimmenLandesliste> getZweitstimmen(int year) {
		return db.fetchZweitstimmen(selected, year);
	}

	public List<Direktkandidat> getDirektkandidaten() {
		if (direktkandidaten == null) {
			direktkandidaten = db.getDirektkandidaten(2009);
		}
		return direktkandidaten;
	}
	
	public List<Direktkandidat> getWahlkreisDirektkandidaten() {
		if (wahlkreisDirektkandidaten == null) {
			wahlkreisDirektkandidaten = db.getDirektkandidaten(selected, 2009); 
		}
		return wahlkreisDirektkandidaten;
	}
	
	public List<Landesliste> getWahlkreisLandeslisten() {
		if (wahlkreisLandeslisten == null) {
			wahlkreisLandeslisten = db.getLandeslisten(selected, 2009);
		}
		return wahlkreisLandeslisten;
	}

	public List<Wahlkreis> getWahlkreise() {
		if (wahlkreise == null) {
			wahlkreise = db.getWahlkreise();
		}
		return wahlkreise;
	}

	public ZweitstimmenSieger getZweitstimmenSieger2009() {
		return db.getZweitstimmenSieger(2009, selected);
	}
	
	public ZweitstimmenSieger getZweitstimmenSieger2009(Wahlkreis kreis) {
		if (zweitstimmenSieger2009 == null) {
			zweitstimmenSieger2009 = db.getZweitstimmenSieger(2009);
		}
		
		for (ZweitstimmenSieger zs : zweitstimmenSieger2009) {
			if (zs.getWahlkreis().equals(kreis)) {
				return zs;
			}
		}
		return null;
	}

	public List<SelectItem> getBundeslaender() {
		List<Bundesland> laender = db.getBundeslaender();
		List<SelectItem> result = new ArrayList<SelectItem>(17);
		result.add(new SelectItem(""));
		for (Bundesland bl : laender) {
			result.add(new SelectItem(bl.getName()));
		}
		return result;
	}
	
	public List<Partei> getParteien() {
		return db.getParteien();
	}

	public int getZweitstimmen(Partei partei, int jahr) {
		return db.getZweitstimmen(partei, jahr);
	}

	public String getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}

	public Wahlkreis getSelected() {
		return selected;
	}

	public int getTotalSecondVotes() {
		return totalSecondVotes;
	}

	public int getTotalFirstVotes() {
		return totalFirstVotes;
	}

	public Map<Bewerber, Integer> getErststimmen2005() {
		if (erststimmen2005 == null) {
			erststimmen2005 = new HashMap<Bewerber, Integer>();
			for (IErststimmen es : getErststimmen(2005)) {
				erststimmen2005.put(es.getKandidat().getBewerber(), es.getStimmen());
			}
		}
		return erststimmen2005;
	}

	public Map<Bewerber, Integer> getErststimmen2009() {
		if (erststimmen2009 == null) {
			erststimmen2009 = new HashMap<Bewerber, Integer>();
			totalFirstVotes = 0;
			for (IErststimmen es : getErststimmen(2009)) {
				erststimmen2009.put(es.getKandidat().getBewerber(), es.getStimmen());
				totalFirstVotes += es.getStimmen();
			}
		}
		return erststimmen2009;
	}
	
	public Map<Partei, Integer> getZweitstimmen2005() {
		if (zweitstimmen2005 == null) {
			zweitstimmen2005 = new HashMap<Partei, Integer>();
			for (IZweitstimmenLandesliste zs : getZweitstimmen(2005)) {
				zweitstimmen2005.put(zs.getLandesliste().getPartei(), zs.getStimmen());
			}
		}
		return zweitstimmen2005;
	}

	public Map<Partei, Integer> getZweitstimmen2009() {
		if (zweitstimmen2009 == null) {
			totalSecondVotes = 0;
			zweitstimmen2009 = new HashMap<Partei, Integer>();
			for (IZweitstimmenLandesliste zs : getZweitstimmen(2009)) {
				zweitstimmen2009.put(zs.getLandesliste().getPartei(), zs.getStimmen());
				totalSecondVotes += zs.getStimmen();
			}
		}
		return zweitstimmen2009;
	}
}
