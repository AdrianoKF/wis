package edu.tum.cs.wis.view;

import java.util.List;
import java.math.BigInteger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.dao.DefaultVoteDAO;
import edu.tum.cs.wis.model.Bewerber;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Quadruple;

@ManagedBean(name = "gewinnerBean")
@SessionScoped
public class GewinnerBean {
	
	// List of winners with number of additional votes and rank
	private List<Quadruple<Bewerber, Partei, BigInteger, Integer>> winners;
	
	private VoteDAO db;
	
	@PostConstruct
	public void init() {
		db = new DefaultVoteDAO();
		winners = db.getKnappsteGewinner(2009);
	}
	
	public List<Quadruple<Bewerber,Partei,BigInteger, Integer>> getWinner() {
		return winners;
	}
}
