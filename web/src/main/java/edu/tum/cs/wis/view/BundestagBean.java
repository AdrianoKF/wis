package edu.tum.cs.wis.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Mandat;
import edu.tum.cs.wis.model.Partei;

@ManagedBean(name = "bundestagBean")
@SessionScoped
public class BundestagBean {
	@Inject
	private VoteDAO db;
	
	private List<Mandat> abgeordnete;
	private List<Entry<Partei, Integer>> seatingCache;

	public List<Mandat> getAbgeordnete() {
		if (abgeordnete == null) {
			abgeordnete = db.getAbgeordnete(2009);
		}
		return abgeordnete;
	}
	
	public List<Entry<Partei, Integer>> getTotalSeating() {
		if (seatingCache == null) {
			seatingCache = new ArrayList<Entry<Partei, Integer>>(db.getTotalSeating(2009).entrySet());
		}
		return seatingCache;
	}
}
