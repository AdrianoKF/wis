package edu.tum.cs.wis.remote;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class WISApplication extends Application {

}
