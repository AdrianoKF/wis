package edu.tum.cs.wis.remote;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import edu.tum.cs.wis.dao.DefaultVoteDAO;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Partei;

@Path("/charts")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
public class ChartService {
	private VoteDAO db = new DefaultVoteDAO();
	private GsonBuilder gsonBuilder = new GsonBuilder();
	private Logger log = Logger.getLogger(getClass());

	@PostConstruct
	public void postInit() {
		/* Register a custom JSON serializer for Partei */
		gsonBuilder.setPrettyPrinting().enableComplexMapKeySerialization()
				.registerTypeAdapter(Partei.class, new JsonSerializer<Partei>() {
					@Override
					public JsonElement serialize(Partei partei, Type typeOfSrc,
							JsonSerializationContext context) {
						if (partei == null) {
							return new JsonObject();
						}
						return new JsonPrimitive(partei.getName());
					}
				});
	}

	@GET
	@Path("/totalSeating/{year}")
	public String totalSeating(@PathParam("year") int year) {
		log.info("totalSeating, year=" + year);
		
		Map<Partei, Integer> mapping = db.getTotalSeating(year);
		return gsonBuilder.create().toJson(mapping);
	}

	@GET
	@Path("/ueberhangMandateBL/{year}")
	public String uberhangMandateBL(@PathParam("year") int year) {
		log.info("uberhangMandateBL, year=" + year);

		// Fetch Überhangmandate per Bundesland
		Map<Bundesland, Integer> ueberhangMandateBundesland = db.getUeberhangmandateProBundesland(
				year, true);

		Map<String, Integer> mapping = new HashMap<String, Integer>();
		for (Entry<Bundesland, Integer> entry : ueberhangMandateBundesland.entrySet()) {
			mapping.put(entry.getKey().getName(), entry.getValue());
		}

		return gsonBuilder.create().toJson(mapping);
	}

	@GET
	@Path("/ueberhangMandatePartei/{year}")
	public String uberhangMandatePartei(@PathParam("year") int year) {
		log.info("uberhangMandatePartei, year=" + year);

		// Fetch Überhangmandate per Bundesland
		Map<Partei, Integer> ueberhangMandatePartei = db.getUeberhangmandateProPartei(year);

		Map<String, Integer> mapping = new HashMap<String, Integer>();
		for (Entry<Partei, Integer> entry : ueberhangMandatePartei.entrySet()) {
			mapping.put(entry.getKey().getName(), entry.getValue());
		}

		return gsonBuilder.create().toJson(mapping);
	}
}
