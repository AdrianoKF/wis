package edu.tum.cs.wis.view;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Default;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import edu.tum.cs.wis.dao.UpdateDAO;
  
@ManagedBean
@ViewScoped
public class UpdateBean implements Serializable {  
  
    private Integer progress;  
    private boolean started = false;
    private UpdateThread thread;
    
	@Inject @Default
	private UpdateDAO updateDAO;
  
	@PostConstruct
	public void init(){
		thread = new UpdateThread(updateDAO);
	}
	
    public Integer getProgress() {  
    	if(thread.getProgress() == 100)
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Update Completed", "Update Completed"));  
    	return thread.getProgress();
    }  
      
    public String getStatus(){
    	return thread.getStatus();
    }
    public void onUpdate() {  

    }  
    public String start(){
    	if(!started){
    		thread.start();
    		started = true;
    	}
    	return null;
    }
    
    public boolean getStopPoll(){
    	return !(thread.getState() == Thread.State.RUNNABLE);
    }
} 