package edu.tum.cs.wis.remote;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ballot")
public class BallotBox {
	@GET
	@Path("/vote/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String vote(@PathParam("id") int id) {
		return "Cast a vote for " + id;
	}
}
