package edu.tum.cs.wis.service;

import java.util.Date;

import javax.enterprise.inject.Any;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.dao.WaehlerDAO;
import edu.tum.cs.wis.dao.WahlzettelDAO;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Waehler;
import edu.tum.cs.wis.model.Wahlkreis;
import edu.tum.cs.wis.model.Wahlzettel;

@Named("votingService")
@ApplicationScoped
public class VotingService {
	@Inject
	@Any
	private WahlzettelDAO wahlzettelDAO;
	
	@Inject
	@Any
	private WaehlerDAO waehlerDAO;
	
	@Inject
	private VoteDAO voteDAO;
	
	public void saveVote(int year, Waehler waehler, Wahlkreis wahlkreis, Direktkandidat direktkandidat, Landesliste landesliste) {
		voteDAO.addStimmen(year, wahlkreis, direktkandidat, landesliste, waehler);
	}

	public WahlzettelDAO getWahlzettelDAO() {
		return wahlzettelDAO;
	}

	public void setWahlzettelDAO(WahlzettelDAO wahlzettelDAO) {
		this.wahlzettelDAO = wahlzettelDAO;
	}

	public WaehlerDAO getWaehlerDAO() {
		return waehlerDAO;
	}

	public void setWaehlerDAO(WaehlerDAO waehlerDAO) {
		this.waehlerDAO = waehlerDAO;
	}
}
