package edu.tum.cs.wis.jsf;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;

/**
 * A servlet filter which logs the time elapsed for each request to a log4j logger.
 * @author Adriano
 *
 */
public class RequestTimeFilter implements Filter {
	private static final Logger log = Logger.getLogger(RequestTimeFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		StopWatch watch = new StopWatch();
		watch.start();
		
		chain.doFilter(request, response);
		
		if (log.isInfoEnabled()) {
			if (!((HttpServletRequest) request).getRequestURI().contains("javax")) {
				log.info(String.format("Request to %s took %d ms", ((HttpServletRequest) request).getRequestURI(), watch.getTime()));
			}
		}
	}

	@Override
	public void destroy() {
	}
}