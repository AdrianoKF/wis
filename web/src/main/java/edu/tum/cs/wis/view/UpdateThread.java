package edu.tum.cs.wis.view;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import edu.tum.cs.wis.dao.UpdateDAO;

public class UpdateThread extends Thread {

	private String status = "";
	private int progress = 0;
	private UpdateDAO updateDAO;
	private Logger logger = Logger.getLogger(UpdateThread.class);
	
	public UpdateThread(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}
	@Override
	public void run() {
		status = "";
		progress = 0;
		try {
			status += "Aktualisierung gestartet ...<br/>";
			status += "Aktualisiere Erststimmen...<br/>";
			updateDAO.updateErststimmen();
			step();
			status += "Aktualisiere Zweitstimmen...<br/>";
			updateDAO.updateZweitstimmen();
			step();
			status += "Aktualisiere ZweitstimmenPartei...<br/>";
			updateDAO.updateZweitstimmenPartei();
			step();
			status += "Aktualisiere ZweitstimmenSieger...<br/>";
			updateDAO.updateZweitstimmenSieger();
			step();
			status += "Aktualisiere Sitze pro Partei...<br/>";
			updateDAO.updateSitzePartei();
			step();
			status += "Aktualisiere Überhangmandate...<br/>";
			updateDAO.updateAnzUeberhangmandate();
			step();
			status += "Aktualisiere KnappsteGewinner...<br/>";
			updateDAO.updateKnappsteGewinner();
			step();
			status += "Aktualisiere Sitzverteilung...<br/>";
			updateDAO.updateSitzverteilung();
		} catch (Exception e) {
			logger.log(Level.ERROR, "Aktualsierung fehlgeschlagen!",e);
			status += "Aktualisierung fehlgeschlagen!<br/>";
			progress = 100;
			return;
		}
		
		status += "Aktualisierung abgeschlossen<br/>";
		progress = 100;
	}
	/**
	 * Progress
	 */
	private void step(){
		progress += 12;
	}
	
	/**
	 * Returns a status message
	 * @return message
	 */
	public String getStatus(){
		return status;
	}
	/**
	 * Returns the progress in percent (0 - 100)
	 * @return progress
	 */
	public int getProgress(){
		return progress;
	}
}
