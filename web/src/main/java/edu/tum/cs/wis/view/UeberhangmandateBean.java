package edu.tum.cs.wis.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Partei;

@ManagedBean(name = "ueberhangmandateBean")
@SessionScoped
public class UeberhangmandateBean {

	// DB session
	@Inject
	private VoteDAO db;
	
	private Map<Partei, Integer> ueberhangMandatePartei;
	private Map<Bundesland, Integer> ueberhangMandateBundesland;

	/**
	 * Get parties with Überhangmandate
	 * 
	 * @return
	 */
	public Collection<Partei> getParteien() {
		return new ArrayList<Partei>(getUeberhangmandateProPartei().keySet());
	}

	/**
	 * Get the map of parties and their number of Überhangmandate (Only parties
	 * with at least one Überhangmandat are returned)
	 * 
	 * @return
	 */
	public Map<Partei, Integer> getUeberhangmandateProPartei() {
		if (ueberhangMandatePartei == null) {
			ueberhangMandatePartei = db.getUeberhangmandateProPartei(2009);
		}
		return ueberhangMandatePartei;
	}

	/**
	 * Get the map of Bundesländer and their number of Überhangmandate (Only
	 * Bundesländer with at least one Überhangmandat are returned)
	 * 
	 * @return
	 */
	public Collection<Bundesland> getBundeslaender() {
		return new ArrayList<Bundesland>(getUeberhangmandateProBundesland().keySet());
	}

	/**
	 * Get the number of Überhangmandate for a specific Bundesland from
	 * getBundeslaender
	 * 
	 * @return
	 */
	public Map<Bundesland, Integer> getUeberhangmandateProBundesland() {
		if (ueberhangMandateBundesland == null) {
			ueberhangMandateBundesland = db.getUeberhangmandateProBundesland(2009,false);
		}
		return ueberhangMandateBundesland;
	}
	
	/**
	 * Create Json mapping for the chart showing the number of Überhangmandate per party
	 * @param jahr
	 * @return
	 */
	public String getJsonUeberhangmandateParteiMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		for(Partei partei : getParteien()){
			mapping.put(partei.getName(), getUeberhangmandateProPartei().get(partei));
		}
		return new Gson().toJson(mapping, new TypeToken<Map<String, Integer>>() {}.getType());
	}
}
