package edu.tum.cs.wis.view;

import java.util.List;

import javax.enterprise.inject.Any;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.dao.WaehlerDAO;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Waehler;
import edu.tum.cs.wis.model.Wahlkreis;
import edu.tum.cs.wis.service.VotingService;

@ManagedBean(name = "votingBean")
@SessionScoped
public class VotingBean {
	private static final int YEAR = 2009;
	private static final int MAX_INVALID_AUTH_ATTEMPTS = 3;
	private static Logger log = Logger.getLogger(VotingBean.class);

	// DAOs
	@Inject
	private VoteDAO voteDAO;
	@Inject
	@Any
	private WaehlerDAO waehlerDAO;
	
	@Inject
	private VotingService votingService;

	// Data members
	private Wahlkreis wahlkreis;
	private Direktkandidat direktkandidat;
	private Landesliste landesliste;
	private Waehler waehler;
	private String token;
	private int invalidAuthAttempts;
	
	private List<Direktkandidat> direktkandidaten = null;
	private List<Landesliste> landeslisten = null;

	public String onFlowEvent(FlowEvent event) {
		if (waehler == null && !"auth".equals(event.getNewStep())) {
			String message = "Sie müssen sich vor der Abstimmung authentifizieren!";
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
			return "auth";
		}

		if ("auth".equals(event.getOldStep())) {
			if (waehler == null || !validateToken()) {
				++invalidAuthAttempts;
				if (waehler != null && invalidAuthAttempts >= MAX_INVALID_AUTH_ATTEMPTS) {
					log.warn("Too many invalid auth attempts for voter " + waehler);
					clear();

					FacesContext fc = FacesContext.getCurrentInstance();
					fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Zu viele ungültige Authentifizierungsversuche!", null));
				} else {
					log.warn("Authentication error for voter " + waehler);

					String message;
					if (waehler != null && waehler.getAbgestimmt() != null) {
						message = "Sie haben Ihren Wahlzettel bereits abgeschickt.";
					} else {
						message = "Die eingegebene Wählernummer oder der Sicherheitscode sind ungültig.";
					}
					FacesContext fc = FacesContext.getCurrentInstance();
					fc.addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
				}

				return event.getOldStep();
			} else {
				invalidAuthAttempts = 0;
				direktkandidat = null;
				landesliste = null;
			}
		}

		return event.getNewStep();
	}

	public void clear() {
		direktkandidaten = null;
		landeslisten = null;
		
		direktkandidat = null;
		landesliste = null;
		wahlkreis = null;
		waehler = null;
		token = null;
	}

	public String saveVote() {
		log.info("Saving vote for " + waehler);

		votingService.saveVote(YEAR, waehler, wahlkreis, direktkandidat, landesliste);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Ihre Wahl wurde erfolgreich gespeichert.", null));
		
		clear();
		
		return "/WIS/index.html";
	}

	private boolean validateToken() {
		if (waehler == null) {
			return false;
		}
		return waehler.validateVotingPermission(token);
	}

	public List<Direktkandidat> getDirektkandidaten() {
		if (direktkandidaten == null) {
			direktkandidaten = voteDAO.getDirektkandidaten(wahlkreis, YEAR);
		}
		return direktkandidaten;
	}

	public List<Landesliste> getLandeslisten() {
		if (landeslisten == null) {
			landeslisten = voteDAO.getLandeslisten(wahlkreis, YEAR);
		}
		return landeslisten;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public Direktkandidat getDirektkandidat() {
		return direktkandidat;
	}

	public void setDirektkandidat(Direktkandidat direktkandidat) {
		this.direktkandidat = direktkandidat;
	}

	public Landesliste getLandesliste() {
		return landesliste;
	}

	public void setLandesliste(Landesliste landesliste) {
		this.landesliste = landesliste;
	}

	public Long getVoterId() {
		if (waehler == null) {
			return null;
		}
		return waehler.getId();
	}

	public void setVoterId(Long voterId) {
		waehler = waehlerDAO.get(voterId);
		log.info("Waehler: " + waehler);
		if (waehler != null) {
			wahlkreis = waehler.getWahlkreis();
			direktkandidaten = voteDAO.getDirektkandidaten(wahlkreis, YEAR);
			landeslisten = voteDAO.getLandeslisten(wahlkreis, YEAR);
		}
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
