// Load the Visualization API and the appropriate chart packages.
google.load('visualization', '1.0', {
	'packages' : [ 'corechart', 'geomap' ]
});

var Colors = {
		'CDU' : '#000000',
		'CSU' : '#0051FF',
		'SPD' : '#CC3131',
		'GRÜNE' : '#00B300',
		'FDP' : '#EDED21',
		'DIE LINKE' : '#962667',
		'Übrige' : '#A8A69D'
};

function createChart(colDefs, chartData, options, targetId) {
	var data = new google.visualization.DataTable();
	for (var col in colDefs) {
		data.addColumn(col, colDefs[col]);
	}
	data.addRows(chartData);

	var chart = new google.visualization.PieChart(document
			.getElementById(targetId));
	chart.draw(data, options);
}

function drawChart(json) {
	var chartData = [];
	var colors = [];
	var otherCount = 0;

	$.getJSON("/WIS/rest/charts/totalSeating/2009", null, function(json) {
		$.each(json, function(key, value) {
			if (Colors[key]) {
				chartData.push([ key, value ]);
				colors.push({ 'color': Colors[key] });
			} else {
				if (value.constructor === Number) {
					otherCount += value;
				}
			}
		});

		chartData.push([ 'Übrige', otherCount ]);
		colors.push({ 'color': Colors['Übrige']});

		var colDefs = {
			'string' : 'Name',
			'number' : 'Sitze'
		};
		var options = {
			'title' : 'Sitzverteilung',
			'width' : 720,
			'height' : 450,
			'slices' : colors
		};
		createChart(colDefs, chartData, options, 'chart_div');

	});
}

function drawUMPartyChart(json) {
	var chartData = [];
	var colors = [];

	// Fetch data
	$.getJSON("/WIS/rest/charts/ueberhangMandatePartei/2009", null, function(json) {
		$.each(json, function(key, value) {
			chartData.push([ key, value ]);
			colors.push({ 'color': Colors[key] });
		});

		var colDefs = {
			'string' : 'Name',
			'number' : 'Überhangmandate'
		};
		var options = {
			'title' : 'Überhangmandate pro Partei',
			'width' : 600,
			'height' : 400,
			'slices' : colors
		};
		createChart(colDefs, chartData, options, 'partychart_div');
		// Call draw UMBundesland after UMParty chart was drawed (quick fix to avoid two parallel ajax calls, were buggy)
		//drawUMBundesland();
	});
}

function drawUMBundesland() {
	
	// Init graph
	var data = new google.visualization.DataTable();

	// Set names of axes
	data.addColumn('string', 'Bundesland');
	data.addColumn('number', 'Überhangmandate');

	$.getJSON("/WIS/rest/charts/ueberhangMandateBL/2009", null, function(json) {
		$.each(json, function(key, value) {
			// add row
		    if(key == "Brandenburg")// Fix for Brandenburg
		    	data.addRow([{f: 'Brandenburg', v: 'DE-BB'}, value]);
		    else
		    	data.addRow([key,value]);
		});

		// Generate chart
		var geochart = new google.visualization.GeoChart(document.getElementById('ueberhangmandateBL'));
		geochart.draw(data, {
			width : 600,
			height : 400,
			region : "DE",
			displayMode : 'regions',
			sizeAxis : {
				minValue : 0
			},
			colorAxis : {
				minValue : 0,
				colors : [ '#eee', '#00aa00' ]
			},
			resolution : 'provinces'
		});
	});
	
	

	
}
