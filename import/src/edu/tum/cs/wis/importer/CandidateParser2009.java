package edu.tum.cs.wis.importer;

import java.util.HashMap;
import java.util.Map;

public class CandidateParser2009 extends Parser {

	// Constants
	final Integer vornamePos = 0;
	final Integer nachnamePos = 1;
	final Integer jahrgangPos = 2;
	final Integer parteiPos = 3;
	final Integer wahlkreisPos = 4;
	final Integer bundeslandPos = 5;
	final Integer platzPos = 6;

	// Rename certain parties
	Map<String, String> parteiMapping = new HashMap<String, String>();

	// Constructor
	public CandidateParser2009(String filename, Database db) {
		super(filename, db);

		parteiMapping.put("Anderer KWV", "Übrige");
		parteiMapping.put("Volksabst.", "Volksabstimmung");
		parteiMapping.put("VIOLETTEN", "DIE VIOLETTEN");
		parteiMapping.put("Tierschutz", "Die Tierschutzpartei");
	}

	Integer count = 0, countOld = 0;
	boolean skipFirstLine = true;

	@Override
	void parseLine(String[] line) throws Exception {

		if (skipFirstLine) {
			skipFirstLine = false;
			return;
		}

		// Add Kandidat
		String name = line[nachnamePos];
		String vorname = line[vornamePos];
		String titel = "";
		while (vorname.contains("Dr.")) {
			vorname = vorname.replaceFirst("Dr[.]", "");
			titel += "Dr. ";
		}
		vorname = vorname.trim();
		titel = titel.trim();

		Integer platz = null, wahlkreis = null;
		try {
			platz = Integer.valueOf(line[platzPos]);
		} catch (Exception e) {
		}
		;
		try {
			wahlkreis = Integer.valueOf(line[wahlkreisPos]);
		} catch (Exception e) {
		}
		;

		String partei = translatePartei(line[parteiPos].trim());
		db.addKandidat(name, vorname, titel, "", Integer.valueOf(line[jahrgangPos]), partei,
				wahlkreis, line[bundeslandPos].trim(), platz, 2009);

		// Status
		if (++count - countOld >= 1000) {
			System.out.println("   " + count + " added so far");
			countOld = count;
		}

	}

	String translatePartei(String partei) {
		if (partei != null && parteiMapping.containsKey(partei)) {
			return parteiMapping.get(partei);
		}
		return partei;
	}
}
