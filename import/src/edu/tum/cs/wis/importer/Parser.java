package edu.tum.cs.wis.importer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

// CSV parser
public abstract class Parser {
	// The filename of the file to be parsed
	String filename;
	// Database object
	Database db;
	// Use UTF-8 flag
	boolean useUTF8;

	// Constructor
	public Parser(String filename, Database db) {
		this(filename, db, true);
	}

	// Constructor
	public Parser(String filename, Database db, boolean useUTF8) {
		this.db = db;
		this.filename = filename;
		this.useUTF8 = useUTF8;
	}

	// Abstract parse method
	abstract void parseLine(String[] line) throws Exception;

	// Done callback
	void done() throws Exception {
	};

	// Parse the csv file
	public void parse() throws Exception {

		System.out.println("Importing " + filename + " ...");

		// Open file
		BufferedReader input;
		if (useUTF8)
			input = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename), "UTF-8"));
		else
			input = new BufferedReader(new InputStreamReader(
					new FileInputStream(filename), "ISO-8859-1"));
		// Read line
		String line;
		while ((line = input.readLine()) != null) {
			line = cleanSpecialChars(line);
			// ignore empty lines and comments
			if (line.length() >= 0 && !line.startsWith("//"))
				parseLine(line.split(";", -1));// -1 avoids discarding of
												// trailing empty strings
		}
		done();
		System.out.println("-> done!");
	}
	
	private String cleanSpecialChars(String line) {
		return line.replace('', '-');	// weird dashes
	}
}
