package edu.tum.cs.wis.importer;

public class StructureParser extends Parser {

	// Constants
	final Integer landPos = 0;
	final Integer widPos = 1;
	final Integer wkPos = 2;
	final Integer flaechePos = 4;
	final Integer bevoelkerungPos = 5;
	final Integer wahlberPos = 6;
	
	// Constructor
	public StructureParser(String filename, Database db) {
		super(filename, db, false);
	}

	@Override
	void parseLine(String[] line) throws Exception {
		// Try to get wahlkreis id, else skip line
		Integer wahlkreisId;
		try {
			wahlkreisId = Integer.valueOf(line[widPos]);
		} catch (Exception e) {
			return;
		}
		// Skip Bundeslaender
		if (wahlkreisId > 900)
			return;

		// Get bundesland id
		Integer bundeslandID = db.getBundeslandID(line[landPos]);

		// Add wahlkreis
		Integer flaeche = Integer.valueOf(line[flaechePos].split(",")[0]
				.replaceAll(" ", ""));
		Integer bevoelkerung = (int) (Float.valueOf(line[bevoelkerungPos]
				.replace(",", ".").replace(" ", "")) * 1000);
		Integer wahlberechtigte = Integer.valueOf(line[wahlberPos]);
		
		db.addWahlkreis(bundeslandID, Integer.valueOf(line[widPos]),
				line[wkPos], flaeche, bevoelkerung, wahlberechtigte);

	}

}
