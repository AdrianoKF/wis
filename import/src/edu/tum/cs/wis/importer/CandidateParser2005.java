package edu.tum.cs.wis.importer;

import java.util.HashMap;
import java.util.Map;

public class CandidateParser2005 extends Parser {
	// Constants
	final Integer namePos = 0;
	final Integer berufPos = 1;
	final Integer jahrgangPos = 2;
	final Integer parteiPos = 7;
	final Integer wahlkreisPos = 8;
	final Integer bundeslandPos = 9;
	final Integer platzPos = 10;

	// Bundesland translation
	Map<String, String> bundeslandAbk = new HashMap<String, String>();

	// Rename certain parties
	Map<String, String> parteiMapping = new HashMap<String, String>();

	// Constructor
	public CandidateParser2005(String filename, Database db) {
		super(filename, db);
		bundeslandAbk.put("BW", "Baden-Württemberg");
		bundeslandAbk.put("HE", "Hessen");
		bundeslandAbk.put("SN", "Sachsen");
		bundeslandAbk.put("SH", "Schleswig-Holstein");
		bundeslandAbk.put("TH", "Thüringen");
		bundeslandAbk.put("NI", "Niedersachsen");
		bundeslandAbk.put("HH", "Hamburg");
		bundeslandAbk.put("MV", "Mecklenburg-Vorpommern");
		bundeslandAbk.put("SL", "Saarland");
		bundeslandAbk.put("HB", "Bremen");
		bundeslandAbk.put("RP", "Rheinland-Pfalz");
		bundeslandAbk.put("BB", "Brandenburg");
		bundeslandAbk.put("ST", "Sachsen-Anhalt");
		bundeslandAbk.put("BY", "Bayern");
		bundeslandAbk.put("NW", "Nordrhein-Westfalen");
		bundeslandAbk.put("BE", "Berlin");

		parteiMapping.put("Anderer KWV", "Übrige");
		parteiMapping.put("Die Linke.", "LINKE");
	}

	Integer count = 0, countOld = 0;

	@Override
	void parseLine(String[] line) throws Exception {
		// Add Kandidat
		String[] fullName = line[namePos].split(",");
		String name = fullName[0];
		String vorname = fullName[1];
		String titel = "";

		while (vorname.contains("Dr.")) {
			vorname = vorname.replaceFirst("Dr[.]", "");
			titel += "Dr. ";
		}
		vorname = vorname.trim();
		titel = titel.trim();

		String beruf = line[berufPos];

		Integer platz = null, wahlkreis = null;
		try {
			platz = Integer.valueOf(line[platzPos]);
		} catch (Exception e) {
		}
		;
		try {
			wahlkreis = Integer.valueOf(line[wahlkreisPos]);
		} catch (Exception e) {
		}
		;
		String bundesland = translateBundesland(line[bundeslandPos].trim());
		String partei = translatePartei(line[parteiPos].trim());
		db.addKandidat(name, vorname, titel, beruf, Integer.valueOf(line[jahrgangPos]), partei,
				wahlkreis, bundesland, platz, 2005);
		// Status
		if (++count - countOld >= 1000) {
			System.out.println("   " + count + " added so far");
			countOld = count;
		}
	}

	String translateBundesland(String Abk) throws Exception {
		if (Abk != null && bundeslandAbk.containsKey(Abk)) {
			return bundeslandAbk.get(Abk);
		}
		return Abk;
	}

	String translatePartei(String partei) {
		if (partei != null && parteiMapping.containsKey(partei)) {
			return parteiMapping.get(partei);
		}
		return partei;
	}
}
