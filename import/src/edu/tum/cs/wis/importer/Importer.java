package edu.tum.cs.wis.importer;


public class Importer {
	// The database object
	Database db;

	// Constructor
	public Importer() throws Exception {
		// Create database object
		db = new Database();
		// Connect
		db.connect();
		// Clear database
		db.clear();
	  // Populate divisor table
    db.generateDivisor();
	}

	// Import data
	public void startImport() throws Exception {
		// Import structure
		StructureParser sp = new StructureParser("data/StruktBtwkr2009.csv", db);
		sp.parse();
		
		// Import candidates
		Parser cp = new CandidateParser2005("data/wahlbewerber2005.csv", db);
		cp.parse();
		cp = new CandidateParser2009("data/wahlbewerber2009_mod.csv", db);
		cp.parse();
	}

	// Main function
	public static void main(String[] args) throws Exception {
		Importer imp = new Importer();

		imp.startImport();

	}
}
