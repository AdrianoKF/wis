package edu.tum.cs.wis.importer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/*
 * Controls the database connection
 */
public class Database {

	// Properties object
	Properties properties;

	// The connection object
	Connection con;

	// Prepared statments
	private PreparedStatement stmtInsertBewerber, stmtInsertBundesland, stmtInsertPartei,
			stmtInsertParteizugehoerigkeit, stmtInsertWahlkreis, stmtInsertDirektkandidat,
			stmtInsertLandesliste, stmtInsertListenkandidat, stmtInsertDivisor;

	// Cache for id resolution
	Map<String, Integer> bundeslandID = new HashMap<String, Integer>();
	Map<String, Integer> parteiID = new HashMap<String, Integer>();
	// Cache for Bewerber (Key = "Name"+"Vorname"+"Geburtsjahr")
	Map<String, Integer> bewerberID = new HashMap<String, Integer>();
	// Cache for Landesliste (Key = ParteiID*200 + (BundeslandID%100)*2 + 1 (for
	// year 2009)) quick "hack" to avoid separate class for key object
	Map<Integer, Integer> landeslisteID = new HashMap<Integer, Integer>();

	// Constructor
	public Database() throws FileNotFoundException, IOException {
		// Load properties
		properties = new Properties();
		properties.load(new FileInputStream("data/config.properties"));
	}

	// Connect to the database
	public void connect() throws SQLException, ClassNotFoundException {

		// Get parameters
		String driver = properties.getProperty("driver", "");
		String url = properties.getProperty("url", "");
		String user = properties.getProperty("user", "");
		String password = properties.getProperty("password", "");

		// Connect to database
		System.out.println("Connecting to database ...");
		Class.forName(driver);
		con = DriverManager.getConnection(url, user, password);
		System.out.println("-> done!");

		// Prepare statements
		System.out.println("Prepare statements ...");
		prepareStatements();
		System.out.println("-> done!");

	}

	// Empty database
	public void clear() throws SQLException {
		System.out.println("Clearing database ...");

		// Info: TRUNCATE does not work because of the foreign key definitions
		// in db2
		Statement clear = con.createStatement();
		clear.executeUpdate("DELETE FROM wis.direktkandidat");
		clear.executeUpdate("DELETE FROM wis.listenkandidat");
		clear.executeUpdate("DELETE FROM wis.landesliste");
		clear.executeUpdate("DELETE FROM wis.parteizugehoerigkeit");
		clear.executeUpdate("DELETE FROM wis.partei");
		clear.executeUpdate("DELETE FROM wis.bewerber");
		clear.executeUpdate("DELETE FROM wis.wahlkreis");
		clear.executeUpdate("DELETE FROM wis.bundesland");
		clear.executeUpdate("DELETE FROM wis.divisor");
		clear.close();
		System.out.println("-> done!");
	}

	// Prepare statements
	private void prepareStatements() throws SQLException {
		// Prepare statements -> Performance and Security

		stmtInsertBundesland = con.prepareStatement("INSERT INTO wis.bundesland (name) VALUES (?)",
				Statement.RETURN_GENERATED_KEYS);
		stmtInsertBewerber = con
				.prepareStatement(
						"INSERT INTO wis.bewerber (name, vorname, titel, beruf, geburtsjahr) VALUES (?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
		stmtInsertPartei = con.prepareStatement("INSERT INTO wis.partei (name) VALUES (?)",
				Statement.RETURN_GENERATED_KEYS);
		stmtInsertParteizugehoerigkeit = con
				.prepareStatement("INSERT INTO wis.parteizugehoerigkeit (bewerber, partei, jahr) VALUES (?,?,?)");
		stmtInsertWahlkreis = con
				.prepareStatement("INSERT INTO wis.wahlkreis (id, name, bundesland, flaeche, bevoelkerung, wahlberechtigte) VALUES (?,?,?,?,?,?)");
		stmtInsertDirektkandidat = con
				.prepareStatement("INSERT INTO wis.direktkandidat (bewerber, wahlkreis, jahr) VALUES (?,?,?)");
		stmtInsertLandesliste = con.prepareStatement(
				"INSERT INTO wis.landesliste (partei, bundesland, jahr) VALUES (?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		stmtInsertListenkandidat = con
				.prepareStatement("INSERT INTO wis.listenkandidat (bewerber, landesliste, rang) VALUES (?,?,?)");
		stmtInsertDivisor = con
        .prepareStatement("INSERT INTO wis.divisor (divisor) VALUES (?)");

	}
	
	// Populate divisor table
	public void generateDivisor() throws SQLException{
	  System.out.println("Generating divisor...");
	  // Does not need to be highly performant
	  for(Integer i = 1; i < 1200; i+=2){// At least 598 divisors
	    stmtInsertDivisor.setInt(1, i);
	    stmtInsertDivisor.executeUpdate();
	  }
	  System.out.println(" -> done");
	}

	// Add Bewerber or return his id
	public int addBewerber(String Name, String Vorname, String titel, String Beruf, Integer Jahr)
			throws SQLException {
		final String bewerberKey = Name + Vorname + Jahr;

		// Already present?
		if (bewerberID.containsKey(bewerberKey)) {
			return bewerberID.get(bewerberKey);
		}

		// Set parameters
		stmtInsertBewerber.setString(1, Name);
		stmtInsertBewerber.setString(2, Vorname);
		stmtInsertBewerber.setString(3, titel);
		stmtInsertBewerber.setString(4, Beruf);
		stmtInsertBewerber.setInt(5, Jahr);
		stmtInsertBewerber.executeUpdate();

		// Get id
		ResultSet generatedKeys = stmtInsertBewerber.getGeneratedKeys();
		if (!generatedKeys.next())
			throw new SQLException();
		int kandidatId = generatedKeys.getInt(1);
		generatedKeys.close();

		// Remember bewerber
		bewerberID.put(bewerberKey, kandidatId);
		return kandidatId;
	}

	// Disconnect from the database
	protected void finalize() throws Exception {
		System.out.println("Disconnecting ...");
		con.close();
		System.out.println("-> done!");
	}

	// Get the id of the Bundesland and add it if it does not exist
	public Integer getBundeslandID(String name) throws SQLException {
		// Lookup id in cache
		Integer landID = bundeslandID.get(name);
		if (landID != null)
			return landID;

		// Else insert Bundesland
		stmtInsertBundesland.setString(1, name);
		stmtInsertBundesland.executeUpdate();
		ResultSet generatedKeys = stmtInsertBundesland.getGeneratedKeys();
		if (!generatedKeys.next())
			throw new SQLException();
		landID = generatedKeys.getInt(1);
		generatedKeys.close();

		// Remember object
		bundeslandID.put(name, landID);
		return landID;

	}

	// Add Partei or return its id
	public Integer addPartei(String name) throws SQLException {
		// Skip empty Partei
		if (name == null || name.trim().equals(""))
			return null;

		// Already present?
		if (parteiID.containsKey(name)) {
			return parteiID.get(name);
		}

		// Set parameters
		stmtInsertPartei.setString(1, name);
		stmtInsertPartei.executeUpdate();
		// Get id
		ResultSet generatedKeys = stmtInsertPartei.getGeneratedKeys();
		if (!generatedKeys.next())
			throw new SQLException();
		int parteiId = generatedKeys.getInt(1);
		generatedKeys.close();
		// Remember bewerber
		parteiID.put(name, parteiId);
		return parteiId;
	}

	// Add Parteizugehoerigkeit
	public void addParteizugehoerigkeit(Integer bewerber, Integer partei, Integer jahr)
			throws SQLException {

		// Set parameters
		stmtInsertParteizugehoerigkeit.setInt(1, bewerber);
		stmtInsertParteizugehoerigkeit.setInt(2, partei);
		stmtInsertParteizugehoerigkeit.setInt(3, jahr);
		// Insert
		stmtInsertParteizugehoerigkeit.execute();

	}

	// Add Kandidat -> Add Bewerber object, Partei object ...
	public void addKandidat(String name, String vorname, String titel, String beruf,
			Integer jahrgang, String partei, Integer wahlkreis, String bundesland,
			Integer listenplatz, Integer jahr) throws SQLException {
		// Add Bewerber
		Integer bewerberID = addBewerber(name, vorname, titel, beruf, jahrgang);

		// Add Partei
		Integer parteiID = addPartei(partei);

		// Add Parteizugehoerigkeit
		if (parteiID != null)
			addParteizugehoerigkeit(bewerberID, parteiID, jahr);

		// Add Direktkandidaten
		if (wahlkreis != null)
			addDirektkandidat(bewerberID, wahlkreis, jahr);

		// Add Listenkandidat
		if (bundesland != null && !bundesland.trim().equals("")) {
			Integer bundeslandID = getBundeslandID(bundesland);
			Integer landeslisteID = addLandesliste(parteiID, bundeslandID, jahr);
			addListenkandidat(bewerberID, landeslisteID, listenplatz);
		}
	}

	// Add Direktkandidat
	public void addDirektkandidat(Integer bewerber, Integer wahlkreis, Integer jahr)
			throws SQLException {
		// Set parameters
		stmtInsertDirektkandidat.setInt(1, bewerber);
		stmtInsertDirektkandidat.setInt(2, wahlkreis);
		stmtInsertDirektkandidat.setInt(3, jahr);

		// Insert
		stmtInsertDirektkandidat.execute();

	}

	// Add Landesliste or return its id
	public Integer addLandesliste(Integer partei, Integer bundesland, Integer jahr)
			throws SQLException {
		// Skip invalid input
		if (bundesland == null || partei == null)
			return null;

		// Calculate search key
		Integer key = partei * 200 + (bundesland % 100) * 2;
		if (jahr == 2009)
			key += 1;

		// Already present?
		if (landeslisteID.containsKey(key)) {
			return landeslisteID.get(key);
		}

		// Set parameters
		stmtInsertLandesliste.setInt(1, partei);
		stmtInsertLandesliste.setInt(2, bundesland);
		stmtInsertLandesliste.setInt(3, jahr);
		stmtInsertLandesliste.executeUpdate();

		// Get id
		ResultSet generatedKeys = stmtInsertLandesliste.getGeneratedKeys();
		if (!generatedKeys.next())
			throw new SQLException();
		int landesliste = generatedKeys.getInt(1);
		generatedKeys.close();
		// Remember bewerber
		landeslisteID.put(key, landesliste);
		return landesliste;
	}

	// Add Listenkandidat
	public void addListenkandidat(Integer bewerber, Integer landesliste, Integer rang)
			throws SQLException {
		// Set parameters
		stmtInsertListenkandidat.setInt(1, bewerber);
		stmtInsertListenkandidat.setInt(2, landesliste);
		stmtInsertListenkandidat.setInt(3, rang);

		// Insert
		stmtInsertListenkandidat.execute();

	}

	// Add Wahlkreis
	public void addWahlkreis(Integer bundesland, Integer wahlkreisID, String name,
			Integer flaeche, Integer bevoelkerung, Integer wahlberechtigte) throws SQLException {
		// Set parameters
		stmtInsertWahlkreis.setInt(1, wahlkreisID);
		stmtInsertWahlkreis.setString(2, name);
		stmtInsertWahlkreis.setInt(3, bundesland);
		stmtInsertWahlkreis.setInt(4, flaeche);
		stmtInsertWahlkreis.setInt(5, bevoelkerung);
		stmtInsertWahlkreis.setInt(6, wahlberechtigte);
		// Insert
		stmtInsertWahlkreis.execute();

	}
}
