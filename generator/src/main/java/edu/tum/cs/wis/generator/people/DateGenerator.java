package edu.tum.cs.wis.generator.people;

import java.util.Date;
import java.util.Iterator;
import java.util.Random;

public class DateGenerator implements Iterable<Date> {
	private static final long MS_PER_DAY = 3600 * 24 * 1000;
	
	private final long start;
	private final int maxDaysOffset;
	
	public DateGenerator(Date start, Date end) {
		if (start == null || (end != null && end.before(start))) {
			throw new IllegalArgumentException();
		}
		
		this.start = start.getTime();
		if (end == null) {
			end = new Date();
		}

		// Determine number of days between start and end date
		this.maxDaysOffset = (int) ((end.getTime() - this.start) / MS_PER_DAY);
	}
	
	public DateGenerator(Date start) {
		this(start, null);
	}
	
	@Override
	public Iterator<Date> iterator() {
		return new Iterator<Date>() {
			Random random = new Random();
			
			@Override
			public boolean hasNext() {
				return true;
			}
			
			@Override
			public void remove() { }
			
			@Override
			public Date next() {
				final long offset = random.nextInt(maxDaysOffset) * MS_PER_DAY;
				return new Date(start + offset);
			}
		};
	}
}
