package edu.tum.cs.wis.generator.votes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Bewerber;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Wahlkreis;

public abstract class ResultCsvParser {
	protected final int year;
	protected int lineNumber = 0;
	protected long lastId = -1L;

	protected enum FieldType {
		WAHLKREIS_ID, WAHLKREIS_NAME, FIRST_PARTY, TOTAL_FIRSTVOTES, INVALID_FIRSTVOTES, INVALID_SECONDVOTES
	};

	protected enum LineType {
		PARTY_NAMES
	};

	protected CSVReader reader;
	protected VoteDAO db;

	protected Map<String, Partei> parteien;
	protected Map<Long, Wahlkreis> wahlkreise;

	protected Map<Wahlkreis, Integer> totalFirstVotes = new HashMap<Wahlkreis, Integer>(300);
	protected Map<Wahlkreis, Integer> invalidFirstVotes = new HashMap<Wahlkreis, Integer>(300);
	protected Map<Wahlkreis, Integer> invalidSecondVotes = new HashMap<Wahlkreis, Integer>(300);
	
	protected MultiKeyMap landeslistenMapping = MultiKeyMap.decorate(new LRUMap(200));

	/** (Partei + Wahlkreis-ID) --> Direktkandidat in Wahlkreis */
	protected Map<Pair<Partei, Long>, Bewerber> kandidaten;
	protected List<Direktkandidat> direktkandidaten;

	/** Direktkandidat --> Erststimmenzahl */
	protected Map<Direktkandidat, Integer> firstVotesMapping = new HashMap<Direktkandidat, Integer>(
			2500);

	/** Landeslisten --> Liste (Wahlkreis + Zweitstimmenzahl) */
	protected Map<Landesliste, List<Pair<Wahlkreis, Integer>>> secondVotesMapping = new HashMap<Landesliste, List<Pair<Wahlkreis, Integer>>>(
			100);

	/** Partei --> Spalte in CSV */
	protected Map<Partei, Integer> partyFieldMapping;

	protected Long rowsWritten = 0L;

	/**
	 * Erzeugt eine neue Instanz des Parsers für die Ergebnisse der
	 * Bundestagswahl im CSV-Format.
	 * 
	 * @param in
	 * @param Wahljahr
	 *            für das zu verarbeitende Wahlergebnis
	 * @param db
	 * @param useUtf8
	 */
	protected ResultCsvParser(InputStream in, VoteDAO db, int year, boolean useUtf8) {
		this.year = year;
		try {
			if (useUtf8) {
				reader = new CSVReader(new InputStreamReader(in, "UTF-8"), ';');
			} else {
				reader = new CSVReader(new InputStreamReader(in, "ISO-8859-1"), ';');
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			reader = new CSVReader(new InputStreamReader(in), ';', CSVWriter.NO_QUOTE_CHARACTER);
		}
		this.db = db;
	}

	/**
	 * Liest die Wahlergebnisse aus der gewählten CSV-Datei ein und bereitet die
	 * Erzeugung von einzelnen Wahlzetteln im CSV-Format vor.
	 */
	public void parseFile() {
		System.out.println("Parsing input file");
		fetchStructuralData();

		String[] line;
		try {
			while ((line = reader.readNext()) != null) {
				++lineNumber;
				if (lineNumber == getLineNumber(LineType.PARTY_NAMES)) {
					parseHeaderFields(line);
				} else if (lineNumber > getLineNumber(LineType.PARTY_NAMES)) {
					// Skip empty lines
					if (StringUtils.isEmpty(line[getFieldPosition(FieldType.WAHLKREIS_ID)])) {
						continue;
					}
					long currentId = Long.parseLong(line[getFieldPosition(FieldType.WAHLKREIS_ID)]);

					// Summary lines for states have duplicate indices 1-16
					// or indices out of range, skip those
					if (currentId < lastId || currentId > wahlkreise.size()) {
						continue;
					}
					lastId = currentId;

					Wahlkreis currentWk = wahlkreise.get(currentId);
					if (currentWk == null) {
						continue;
					}
					parseWahlkreis(line, currentWk);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Erzeugt einzelne Wahlzettel für das gesamte Bundesgebiet. Die Wahlzettel
	 * werden im CSV-Format in der übergebenen Datei gespeichert.
	 * 
	 * @param outFile
	 * @throws IOException
	 */
	public void createBulkFile(File outFile) throws IOException {
		CSVWriter out = new CSVWriter(new FileWriter(outFile), ';', CSVWriter.NO_QUOTE_CHARACTER);
		out.writeNext(new String[] { "ID", "Direktkandidat", "Landesliste", "Jahr", "Wahlkreis" });

		for (Wahlkreis wahlkreis : wahlkreise.values()) {
			System.out.println("Writing votes for " + wahlkreis);
			writeBulkVotes(out, wahlkreis);
		}
	}

	/**
	 * Erzeugt einzelne Wahlzettel für den gewählten Wahlkreis. Die Wahlzettel
	 * werden im CSV-Format in der übergebenen Datei gespeichert.
	 * 
	 * @param outFile
	 * @param wahlkreis
	 * @throws IOException
	 */
	public void createBulkFile(File outFile, Wahlkreis wahlkreis) throws IOException {
		CSVWriter out = new CSVWriter(new FileWriter(outFile), ';', CSVWriter.NO_QUOTE_CHARACTER);
		out.writeNext(new String[] { "Direktkandidat", "Landesliste", "Jahr", "Wahlkreis" });

		writeBulkVotes(out, wahlkreis);
	}

	private void writeBulkVotes(CSVWriter out, Wahlkreis wahlkreis) throws IOException {
		Map<Long, Integer> direktkandidatenVotes = makeDirektkandidatenVoteMap(db
				.getDirektkandidaten(wahlkreis, year));
		Map<Long, Integer> landeslistenVotes = makeLandeslistenVoteMap(
				db.getLandeslisten(wahlkreis, year), Collections.singletonList(wahlkreis));

		addInvalidVotes(Collections.singletonList(wahlkreis), direktkandidatenVotes,
				landeslistenVotes);

		Iterator<Long> itKand = direktkandidatenVotes.keySet().iterator();
		Iterator<Long> itList = landeslistenVotes.keySet().iterator();
		Long kandId = itKand.next();
		Long listenId = itList.next();

		int numFirst = direktkandidatenVotes.get(kandId);
		int numSecond = landeslistenVotes.get(listenId);
		while (itKand.hasNext() || itList.hasNext() || numFirst > 0 || numSecond > 0) {
			if (numFirst == 0) {
				kandId = itKand.hasNext() ? itKand.next() : null;
				numFirst = direktkandidatenVotes.get(kandId);
			}

			if (numSecond == 0) {
				listenId = itList.hasNext() ? itList.next() : null;
				numSecond = landeslistenVotes.get(listenId);
			}

			out.writeNext(new String[] { 
					kandId != null && numFirst >= 0 ? kandId.toString() : "",
					listenId != null && numSecond >= 0 ? listenId.toString() : "",
					Integer.toString(year), wahlkreis.getId().toString() });

			if (numFirst > 0) {
				--numFirst;
				direktkandidatenVotes.put(kandId, numFirst);
			}

			if (numSecond > 0) {
				--numSecond;
				landeslistenVotes.put(listenId, numSecond);
			}

			++rowsWritten;
		}
		out.flush();
	}

	protected void addInvalidVotes(Collection<Wahlkreis> wahlkreise,
			Map<Long, Integer> direktkandidatenVotes, Map<Long, Integer> landeslistenVotes) {
		for (Wahlkreis wahlkreis : wahlkreise) {
			int numInvalidFirstVotes = 0;
			int numInvalidSecondVotes = 0;

			if (invalidSecondVotes.containsKey(wahlkreis)) {
				numInvalidFirstVotes = invalidFirstVotes.get(wahlkreis);
				direktkandidatenVotes.put(null, numInvalidFirstVotes);
			}

			if (invalidSecondVotes.containsKey(wahlkreis)) {
				numInvalidSecondVotes = invalidSecondVotes.get(wahlkreis);
				landeslistenVotes.put(null, numInvalidSecondVotes);
			}
		}
	}

	protected void parseInvalidVotes(String[] line, Wahlkreis wahlkreis) {
		totalFirstVotes.put(wahlkreis,
				Integer.parseInt(line[getFieldPosition(FieldType.TOTAL_FIRSTVOTES)]));
		invalidFirstVotes.put(wahlkreis,
				Integer.parseInt(line[getFieldPosition(FieldType.INVALID_FIRSTVOTES)]));
		invalidSecondVotes.put(wahlkreis,
				Integer.parseInt(line[getFieldPosition(FieldType.INVALID_SECONDVOTES)]));
	}

	protected abstract int getFieldPosition(FieldType fieldType);

	protected abstract int getLineNumber(LineType lineType);

	protected abstract void parseHeaderFields(String[] line);

	protected abstract void parseWahlkreis(String[] line, Wahlkreis wahlkreis);

	protected void fetchStructuralData() {
		System.out.print("Fetching structural data... ");
		direktkandidaten = db.getDirektkandidaten(year);

		parteien = new HashMap<String, Partei>(50);
		for (Partei p : db.getParteien()) {
			parteien.put(p.getName(), p);
		}

		wahlkreise = new TreeMap<Long, Wahlkreis>();
		for (Wahlkreis k : db.getWahlkreise()) {
			wahlkreise.put(k.getId(), k);
		}

		Map<Bewerber, Partei> kandPart = db.getParteizugehoerigkeiten(year);
		kandidaten = new HashMap<Pair<Partei, Long>, Bewerber>(3000);
		for (Direktkandidat d : direktkandidaten) {
			Bewerber b = d.getBewerber();
			Partei p = kandPart.get(b);
			kandidaten.put(new ImmutablePair<Partei, Long>(p, d.getWahlkreis().getId()), b);
		}
		System.out.println("done.");
	}

	protected void saveVotes(Wahlkreis wahlkreis, Partei partei, int erststimmen, int zweitstimmen) {
		if (erststimmen > 0) {
			Bewerber b = kandidaten.get(new ImmutablePair<Partei, Long>(partei, wahlkreis.getId()));
			Direktkandidat d = findDirektkandidat(b);
			firstVotesMapping.put(d, erststimmen);
		}
		if (zweitstimmen > 0) {
			if (!landeslistenMapping.containsKey(partei.getId(), wahlkreis.getBundesland().getId(), year)) {
				landeslistenMapping.put(partei.getId(), wahlkreis.getBundesland().getId(), year,
						db.findLandesliste(partei, wahlkreis.getBundesland(), year));
			}
			
			Landesliste l = (Landesliste) landeslistenMapping.get(partei.getId(), wahlkreis.getBundesland().getId(), year);

			List<Pair<Wahlkreis, Integer>> liste = secondVotesMapping.get(l);
			if (liste == null) {
				liste = new ArrayList<Pair<Wahlkreis, Integer>>();
			}
			liste.add(new ImmutablePair<Wahlkreis, Integer>(wahlkreis, zweitstimmen));
			secondVotesMapping.put(l, liste);
		}
	}

	protected Map<Long, Integer> makeDirektkandidatenVoteMap(Collection<Direktkandidat> kandidaten) {
		Map<Long, Integer> result = new HashMap<Long, Integer>();

		for (Direktkandidat kandidat : kandidaten) {
			if (kandidat == null) {
				continue;
			}
			final Long kandId = kandidat.getId();
			if (firstVotesMapping.containsKey(kandidat)) {
				final int numVotes = firstVotesMapping.get(kandidat);
				result.put(kandId, numVotes);
			} else {
				result.put(kandId, 0);
			}
		}

		return result;
	}

	protected Map<Long, Integer> makeLandeslistenVoteMap(Collection<Landesliste> listen,
			Collection<Wahlkreis> wahlkreise) {
		Map<Long, Integer> result = new HashMap<Long, Integer>();

		for (Landesliste liste : listen) {
			final Long listenId = liste.getId();
			if (secondVotesMapping.containsKey(liste)) {
				// Alle Stimmen für eine Landesliste, umfasst alle Wahlkreise
				// des Bundeslandes
				List<Pair<Wahlkreis, Integer>> allVotes = secondVotesMapping.get(liste);

				for (Pair<Wahlkreis, Integer> kreisVotes : allVotes) {
					// Nur relevante Wahlkreise kommen ins Ergebnis
					if (wahlkreise.contains(kreisVotes.getLeft())) {
						final int numVotes = kreisVotes.getRight();
						result.put(listenId, numVotes);
					}
				}
			} else {
				result.put(listenId, 0);
			}
		}

		return result;
	}

	private Direktkandidat findDirektkandidat(Bewerber b) {
		if (b == null) {
			return null;
		}

		for (Direktkandidat d : direktkandidaten) {
			if (d.getBewerber().getId().equals(b.getId())) {
				return d;
			}
		}

		return null;
	}
}
