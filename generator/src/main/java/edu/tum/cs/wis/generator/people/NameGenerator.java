package edu.tum.cs.wis.generator.people;

import java.util.Iterator;
import java.util.Random;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class NameGenerator implements Iterable<Pair<String, String>> {
	private static final float DOUBLE_NAMES_PERCENT = 0.02f;
	
	private static final String[] FIRST_NAMES = {
		"Adrian", "Anna",
		"Andreas", "Alice",
		"Alexander", "Alexandra",
		"Benedikt", "Beatrice",
		"Christoph", "Carla",
		"Constantin", "Clara",
		"Daniel", "Dora",
		"Dominik", "Daniela",
		"Erik", "Edith",
		"Florian", "Fabienne",
		"Fabian", "Felicia",
		"Gerhard", "Gundula",
		"Gustav", "Gabriele",
		"Helmut", "Helga",
		"Ingo", "Isabella",
		"Justus", "Julia",
		"Jonas", "Janette",
		"Kai", "Kristina",
		"Ludwig", "Lisa",
		"Linus", "Laura",
		"Lukas", "Leonie",
		"Martin", "Maria",
		"Michael", "Mona",
		"Marcel", "Miriam",
		"Nathan", "Nadine",
		"Oliver", "Olivia",
		"Peter", "Patricia",
		"Patrick", "Paula",
		"Quirin",
		"Robert", "Rita",
		"Stephan", "Samantha",
		"Simon", "Susanne",
		"Tim", "Tamara",
		"Thomas", "Tina",
		"Udo", "Ursula",
		"Viorel", "Vanessa",
		"Walter", "Wanda",
		"Xaver",
		"Yvette",
		"Zara"
	};
	
	private static final String[] LAST_NAMES = {
		"Müller", "Meier", "Maier", "Schmidt",
		"Schulze", "Friedrich", "Wolf", "Fuchs",
		"Bär", "Thoma", "Endhardt", "Kluge",
		"Neuer", "Merkel", "Kohl", "Beck",
		"Engel", "Meißner", "Strobel", "Schöler",
		"Ostermann", "Hermann", "Gerstl", "Ullmann",
		"Hamann", "Bauer", "Ernst", "Gruber",
		"Schweiger", "Sandler", "Kuhn", "Lindner",
		"Riedler", "Fritsch", "Haider", "Strache",
	};
	
	
	
	@Override
	public Iterator<Pair<String, String>> iterator() {
		return new Iterator<Pair<String,String>>() {
			Random random = new Random();
			
			@Override
			public boolean hasNext() {
				return true;
			}
			
			public void remove() {};
			
			@Override
			public Pair<String, String> next() {
				String firstName = FIRST_NAMES[random.nextInt(FIRST_NAMES.length)];
				String lastName = LAST_NAMES[random.nextInt(LAST_NAMES.length)];
				
				// Randomly generate composite last names
				if (random.nextFloat() < DOUBLE_NAMES_PERCENT) {
					lastName += "-" + LAST_NAMES[random.nextInt(LAST_NAMES.length)];
				}
				return new ImmutablePair<String, String>(firstName, lastName);
			}
		};
	}
}
