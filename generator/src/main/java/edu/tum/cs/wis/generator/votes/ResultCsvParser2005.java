package edu.tum.cs.wis.generator.votes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Wahlkreis;

public class ResultCsvParser2005 extends ResultCsvParser {
	private final String stmtDeleteErststimmen = "DELETE FROM WIS.Erststimmen WHERE jahr = 2005;";
	private final String stmtDeleteZweitstimmen = "DELETE FROM WIS.Zweitstimmen WHERE jahr = 2005;";
	private final String stmtDeleteZweitstimmenPartei = "DELETE FROM WIS.ZweitstimmenPartei WHERE jahr = 2005;";
	
	private final String stmtInsertErststimmen = "INSERT INTO WIS.Erststimmen (Wahlkreis, Direktkandidat, Jahr, Stimmen) VALUES ?;";
	private final String stmtInsertZweitstimmen = "INSERT INTO WIS.Zweitstimmen (Wahlkreis, Landesliste, Jahr, Stimmen) VALUES ?;";
	private final String stmtInsertZweitstimmenPartei = "INSERT INTO WIS.ZweitstimmenPartei (Partei, Jahr, Stimmen) VALUES ?;";

	public ResultCsvParser2005(InputStream in, VoteDAO db, boolean useUtf8) {
		super(in, db, 2005, useUtf8);
	}
	
	private String normalizePartyName(String name) {
		if ("Die Linke.".equals(name)) {
			return "LINKE";
		} else if ("Tierschutz".equals(name)) {
			return "Die Tierschutzpartei";
		}
		return name;
	}

	@Override
	protected void parseHeaderFields(String[] line) {
		partyFieldMapping = new HashMap<Partei, Integer>();
		for (int field = getFieldPosition(FieldType.FIRST_PARTY); field < line.length - 1; field += 2) {
			String party = normalizePartyName(line[field]);
			if (StringUtils.isEmpty(party)) {
				continue;
			}

			if (parteien.containsKey(party)) {
				partyFieldMapping.put(parteien.get(party), field);
			}
		}
	}

	@Override
	protected void parseWahlkreis(String[] line, Wahlkreis wahlkreis) {
		parseInvalidVotes(line, wahlkreis);

		for (Partei p : partyFieldMapping.keySet()) {
			try {
				Integer firstField = partyFieldMapping.get(p);
				if (firstField == null) {
					throw new IllegalStateException("Missing party mapping: " + p);
				}

				int erststimmen = Integer.parseInt(StringUtils
						.defaultIfEmpty(line[firstField], "0"));
				int zweitstimmen = Integer.parseInt(StringUtils.defaultIfEmpty(
						line[firstField + 1], "0"));

				saveVotes(wahlkreis, p, erststimmen, zweitstimmen);
			} catch (NumberFormatException ex) {
				ex.printStackTrace();
				continue;
			}
		}
	}

	protected Map<Long, Integer> makeLandeslistenVotePartyMap(Collection<Landesliste> listen,
			Collection<Wahlkreis> wahlkreise) {
		Map<Long, Integer> result = new HashMap<Long, Integer>();

		for (Landesliste liste : listen) {
			final Long parteiId = liste.getPartei().getId();
			if (secondVotesMapping.containsKey(liste)) {
				// Alle Stimmen für eine Landesliste, umfasst alle Wahlkreise
				// des Bundeslandes
				List<Pair<Wahlkreis, Integer>> allVotes = secondVotesMapping.get(liste);

				for (Pair<Wahlkreis, Integer> kreisVotes : allVotes) {
					// Nur relevante Wahlkreise kommen ins Ergebnis
					if (wahlkreise.contains(kreisVotes.getLeft())) {
						// Addiere Stimmen für die Liste aus diesem Wahlkreis
						// zum Parteiergebnis
						final int numVotes = kreisVotes.getRight();
						Integer oldVotes = result.get(parteiId);
						if (oldVotes == null) {
							oldVotes = 0;
						}
						result.put(parteiId, oldVotes + numVotes);
					}
				}
			}
		}
		return result;
	}

	private String formatFirstVotes(Wahlkreis wahlkreis, Map<Long, Integer> votesMap) {
		if (wahlkreis == null) {
			return "";
		}
		
		StringBuffer result = new StringBuffer();
		boolean first = true;
		for (Entry<Long, Integer> e : votesMap.entrySet()) {
			if (!first) {
				result.append(", (");
			} else {
				result.append("(");
				first = false;
			}
			result.append(wahlkreis.getId());
			result.append(", ");
			result.append(e.getKey());
			result.append(", ");
			result.append(2005);
			result.append(", ");
			result.append(e.getValue());
			result.append(")");
		}
		return result.toString();
	}

	private String formatSecondVotes(Wahlkreis kreis, Map<Long, Integer> votesMap) {
		if (kreis == null) {
			return "";
		}

		StringBuffer result = new StringBuffer();
		boolean first = true;
		for (Entry<Long, Integer> e : votesMap.entrySet()) {
			if (!first) {
				result.append(", (");
			} else {
				result.append("(");
				first = false;
			}
			result.append(kreis.getId());
			result.append(", ");
			result.append(e.getKey());
			result.append(", ");
			result.append(2005);
			result.append(", ");
			result.append(e.getValue());
			result.append(")");
		}
		return result.toString();
	}

	private String formatSecondVotesParty(Map<Long, Integer> votesMap) {
		StringBuffer result = new StringBuffer();
		boolean first = true;
		for (Entry<Long, Integer> e : votesMap.entrySet()) {
			if (!first) {
				result.append(", (");
			} else {
				result.append("(");
				first = false;
			}
			result.append(e.getKey());
			result.append(", ");
			result.append(2005);
			result.append(", ");
			result.append(e.getValue());
			result.append(")");
		}
		return result.toString();
	}

	@Override
	public void createBulkFile(File outFile) throws IOException {
		PrintWriter out = new PrintWriter(new FileWriter(outFile));
		
		out.println(stmtDeleteErststimmen);
		out.println(stmtDeleteZweitstimmen);
		out.println(stmtDeleteZweitstimmenPartei);
		
		for (Wahlkreis wahlkreis : wahlkreise.values()) {
			System.out.println("Generating aggregate SQL for " + wahlkreis);
			generateSQL(out, wahlkreis);
		}
		System.out.println("Generating aggregate SQL for all parties");
		generatePartySQL(out, wahlkreise.values());
		out.close();
	}

	@Override
	public void createBulkFile(File outFile, Wahlkreis wahlkreis) throws IOException {
		PrintWriter out = new PrintWriter(new FileWriter(outFile));
		generateSQL(out, wahlkreis);
		generatePartySQL(out, Collections.singletonList(wahlkreis));
		out.close();
	}

	private void generateSQL(PrintWriter out, Wahlkreis wahlkreis) {
		Map<Long, Integer> direktkandidatenVotes = makeDirektkandidatenVoteMap(db
				.getDirektkandidaten(wahlkreis, 2005));
		Map<Long, Integer> landeslistenVotes = makeLandeslistenVoteMap(
				db.getLandeslisten(wahlkreis, 2005), Collections.singletonList(wahlkreis));

		addInvalidVotes(Collections.singletonList(wahlkreis), direktkandidatenVotes,
				landeslistenVotes);

		out.println(stmtInsertErststimmen.replace("?", formatFirstVotes(wahlkreis, direktkandidatenVotes)));
		out.println(stmtInsertZweitstimmen.replace("?",
				formatSecondVotes(wahlkreis, landeslistenVotes)));

	}

	private void generatePartySQL(PrintWriter out, Collection<Wahlkreis> wahlkreise) {
		HashSet<Landesliste> allLists = new HashSet<Landesliste>();
		for (Wahlkreis wahlkreis : wahlkreise) {
			allLists.addAll(db.getLandeslisten(wahlkreis, 2005));
		}

		Map<Long, Integer> parteiVotes = makeLandeslistenVotePartyMap(allLists, wahlkreise);
		out.println(stmtInsertZweitstimmenPartei.replace("?", formatSecondVotesParty(parteiVotes)));
	}

	@Override
	protected int getFieldPosition(FieldType fieldType) {
		switch (fieldType) {
		case WAHLKREIS_ID:
			return 0;
		case WAHLKREIS_NAME:
			return 2;
		case TOTAL_FIRSTVOTES:
			return 4;
		case INVALID_FIRSTVOTES:
			return 5;
		case INVALID_SECONDVOTES:
			return 6;
		case FIRST_PARTY:
			return 9;
		default:
			return -1;
		}
	}

	@Override
	protected int getLineNumber(LineType lineType) {
		switch (lineType) {
		case PARTY_NAMES:
			return 3;
		default:
			return -1;
		}
	}
}
