package edu.tum.cs.wis.generator.people;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.Pair;

import edu.tum.cs.wis.dao.DefaultVoteDAO;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.dao.WaehlerDAO;
import edu.tum.cs.wis.model.Waehler;
import edu.tum.cs.wis.model.Wahlkreis;

public class VotersGenerator {
	private static final int TOKEN_LENGTH = 24;

	private final VoteDAO voteDAO = new DefaultVoteDAO();
	private final WaehlerDAO waehlerDAO = new WaehlerDAO();

	public void generateVoters(int count) {
		final DateFormat df = SimpleDateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN);
		Iterator<Pair<String, String>> names = new NameGenerator().iterator();
		Iterator<Date> birthdays;
		try {
			birthdays = new DateGenerator(df.parse("01.01.1945"), df.parse("31.12.1990"))
					.iterator();
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}

		Waehler waehler;
		Pair<String, String> name;
		Random random = new Random();
		List<Wahlkreis> wahlkreise = voteDAO.getWahlkreise();

		for (int i = 0; i < count; ++i) {
			waehler = new Waehler();
			name = names.next();

			waehler.setVorname(name.getLeft());
			waehler.setName(name.getRight());
			waehler.setGeburtstag(birthdays.next());
			waehler.setToken(RandomStringUtils.randomAlphanumeric(TOKEN_LENGTH));
			waehler.setWahlkreis(wahlkreise.get(random.nextInt(wahlkreise.size())));
			waehlerDAO.saveOrUpdate(waehler);

			System.out.println(waehler);
		}
	}

	public static void main(String[] args) {
		StopWatch watch = new StopWatch();

		if (args.length != 1) {
			System.err.println("Wrong parameter count!");
			System.err.println("Syntax: VotersGenerator <count>");
			System.exit(-1);
		}
		try {
			int count = Integer.parseInt(args[0]);
			new VotersGenerator().generateVoters(count);
		} catch (NumberFormatException e) {
			System.err.println("Invalid parameter format!");
			System.exit(-1);
		}

		System.out.printf("Operation took %.1f s%n", watch.getTime() / 1000.0);
	}
}
