package edu.tum.cs.wis.generator.votes;

import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Wahlkreis;

public class ResultCsvParser2009 extends ResultCsvParser {
	public ResultCsvParser2009(InputStream in, VoteDAO db, boolean useUtf8) {
		super(in, db, 2009, useUtf8);
	}

	@Override
	protected void parseHeaderFields(String[] line) {
		partyFieldMapping = new HashMap<Partei, Integer>();
		for (int field = getFieldPosition(FieldType.FIRST_PARTY); field < line.length - 1; field += 4) {
			String party = line[field];
			if (StringUtils.isEmpty(party)) {
				continue;
			}

			if (parteien.containsKey(party)) {
				partyFieldMapping.put(parteien.get(party), field);
			}
		}
	}

	@Override
	protected void parseWahlkreis(String[] line, Wahlkreis wahlkreis) {
		parseInvalidVotes(line, wahlkreis);

		for (Partei p : partyFieldMapping.keySet()) {
			try {
				Integer firstField = partyFieldMapping.get(p);
				if (firstField == null) {
					throw new IllegalStateException("Missing party mapping: " + p);
				}

				int erststimmen = Integer.parseInt(StringUtils
						.defaultIfEmpty(line[firstField], "0"));
				int zweitstimmen = Integer.parseInt(StringUtils.defaultIfEmpty(
						line[firstField + 2], "0"));

				saveVotes(wahlkreis, p, erststimmen, zweitstimmen);
			} catch (NumberFormatException ex) {
				ex.printStackTrace();
				continue;
			}
		}
	}

	@Override
	protected int getFieldPosition(FieldType fieldType) {
		switch (fieldType) {
		case WAHLKREIS_ID:
			return 0;
		case WAHLKREIS_NAME:
			return 1;
		case TOTAL_FIRSTVOTES:
			return 7;
		case INVALID_FIRSTVOTES:
			return 11;
		case INVALID_SECONDVOTES:
			return 13;
		case FIRST_PARTY:
			return 19;
		default:
			return -1;
		}
	}

	@Override
	protected int getLineNumber(LineType lineType) {
		switch (lineType) {
		case PARTY_NAMES:
			return 3;
		default:
			return -1;
		}
	}
}
