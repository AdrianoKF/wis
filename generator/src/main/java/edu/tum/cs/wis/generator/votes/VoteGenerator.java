package edu.tum.cs.wis.generator.votes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.commons.lang3.time.StopWatch;

import edu.tum.cs.wis.dao.DefaultVoteDAO;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Wahlkreis;

public class VoteGenerator {
	private File outFile;
	private static VoteDAO db;
	private ResultCsvParser parser;

	public VoteGenerator(File inFile, File outFile, int year) {
		this.outFile = outFile;
		try {
			switch (year) {
			case 2005:
				parser = new ResultCsvParser2005(new FileInputStream(inFile), db, false);
				break;

			case 2009:
				parser = new ResultCsvParser2009(new FileInputStream(inFile), db, false);
				break;

			default:
				throw new IllegalArgumentException("Election year must be either 2005 or 2009.");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void generateAllVotes() {
		StopWatch watch = new StopWatch();
		watch.start();
		System.out.println("Generating all votes... Time to get some coffee :)");
		
		try {
			parser.parseFile();
			parser.createBulkFile(outFile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.printf("Operation finished in %.2f sec\n", watch.getTime() / 1000.0);
		}
		
	}

	public void generateVotes(Wahlkreis wahlkreis) {
		StopWatch watch = new StopWatch();
		watch.start();
		System.out.println("Generating votes for electoral district: " + wahlkreis);
		
		try {
			parser.parseFile();
			parser.createBulkFile(outFile, wahlkreis);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.printf("Operation finished in %.2f sec\n", watch.getTime() / 1000.0);
		}
	}

	public static void main(String[] args) {
		switch (args.length) {
		case 3:
			File inFile = new File(args[0]);
			File outFile = new File(args[1]);
			int year = parseYear(args[2]);
			
			db = new DefaultVoteDAO();
			VoteGenerator voteGenerator = new VoteGenerator(inFile, outFile, year);
			voteGenerator.generateAllVotes();
			break;

		case 4:
			inFile = new File(args[0]);
			outFile = new File(args[1]);
			year = parseYear(args[2]);

			Wahlkreis wahlkreis = null;
			db = new DefaultVoteDAO();
			try {
				long wkId = Long.parseLong(args[3]);
				wahlkreis = db.getWahlkreis(wkId);
			} catch (NumberFormatException e) {
				wahlkreis = db.findWahlkreisByName(args[3]);
			}

			if (wahlkreis == null) {
				System.err.println("Could not find electoral district via name or ID: " + args[3]);
				System.exit(-1);
			}

			voteGenerator = new VoteGenerator(inFile, outFile, year);
			voteGenerator.generateVotes(wahlkreis);
			break;

		default:
			usage();
		}
	}

	private static int parseYear(String year) {
		try {
			return Integer.parseInt(year);
		} catch (NumberFormatException e) {
			System.err.println("Election year invalid: " + year);
			System.exit(-1);
			return 0;
		}
	}

	private static void usage() {
		System.err
				.println("Usage: VoteGenerator <infile> <outfile> <year> [<electoral distict name or id>]");
		System.exit(-1);
	}
}
