package de.tum.in.wis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/*
 * Controls the database connection
 */
public class Database {

	// Properties object
	Properties properties;

	// The connection object
	Connection con;

	// Prepared statments
	private PreparedStatement stmtSelectParties, stmtUpdateErststimmen, stmtUpdateZweitstimmen,
			stmtUpdateZweitstimmenPartei,
			stmtSelectZweistimmenSperrklausel, stmtSelectZweistimmenPartei, stmtSelectDirektkandidaten,
			stmtSelectListenkandidaten;

	// Constructor
	public Database() throws FileNotFoundException, IOException {
		// Load properties
		properties = new Properties();
		properties.load(new FileInputStream("data/config.properties"));
	}

	// Connect to the database
	public void connect() throws SQLException {

		// Get parameters
		String driver = properties.getProperty("driver", "");
		String url = properties.getProperty("url", "");
		String user = properties.getProperty("user", "");
		String password = properties.getProperty("password", "");

		// Connect to database
		System.out.println("Connecting to database ...");
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Database driver not found...");
			throw new SQLException();
		}
		con = DriverManager.getConnection(url, user, password);
		System.out.println("-> done!");

		// Prepare statements
		System.out.println("Prepare statements ...");
		prepareStatements();
		System.out.println("-> done!");

	}

	// Prepare statements
	private void prepareStatements() throws SQLException {
		// Prepare statements -> Performance and Security

		// Get the parties
		stmtSelectParties = con.prepareStatement("SELECT id, name FROM wis.Partei");

		// Get zweitstimmen aggregations for 2009, respect the 5% rule
		stmtSelectZweistimmenSperrklausel = con
				.prepareStatement("SELECT partei, stimmen FROM wis.ZweitstimmenPartei WHERE jahr = 2009 AND stimmen >= (SELECT sum(stimmen) FROM wis.ZweitstimmenPartei)*5/100 ;");

		// Get zweitstimmen of landeslisten for a specific party
		stmtSelectZweistimmenPartei = con
				.prepareStatement("SELECT landesliste, stimmen FROM wis.Zweitstimmen LEFT JOIN wis.Landesliste ON id = landesliste WHERE partei = ? and JAHR = 2009");

		// Get direktkandidaten for a specific landeslisten id
		stmtSelectDirektkandidaten = con
				.prepareStatement("Select b.name "
						+
						"FROM wis.bewerber bo, wis.landesliste lo, wis.direktkandidat dko, wis.parteizugehoerigkeit pz "
						+
						"WHERE pz.jahr = 2009 AND pz.bewerber = bo.id AND dko.bewerber = bo.id AND pz.partei = lo.partei "
						+
						"AND dko.JAHR = 2009 AND lo.id = ? AND dko.id in (SELECT e.direktkandidat "
						+
						"FROM wis.erststimmen e, wis.direktkandidat dk "
						+
						"WHERE e.direktkandidat = dk.id "
						+
						"AND JAHR = 2009 AND stimmen = (SELECT max(stimmen) FROM wis.erststimmen e2, wis.direktkandidat dk2 WHERE dk2.jahr = 2009 AND dk2.wahlkreis = dk.wahlkreis AND e2.direktkandidat = dk2.id) "
						+
						"AND dk.wahlkreis in (SELECT wk.id FROM wahlkreis wk, landesliste ll WHERE ll.id = ? and wk.bundesland = landesliste.bundesland)"
						+
						"GROUP BY e.direktkandidat)"
				);

		// Get listenkandidaten for a specific landesliste
		stmtSelectListenkandidaten = con.prepareStatement("SELECT b.name " +
				"FROM wis.bewerber b, wis.listenkandidat lk " +
				"WHERE b.id = lk.bewerber AND lk.jahr = 2009 AND lk.landesliste = ? " +
				"ORDER BY rang ASC");

		// Statements to refresh MQTs
		stmtUpdateErststimmen = con.prepareStatement("REFRESH TABLE wis.erststimmen");
		stmtUpdateZweitstimmen = con.prepareStatement("REFRESH TABLE wis.zweitstimmen");
		stmtUpdateZweitstimmenPartei = con.prepareStatement("REFRESH TABLE wis.zweitstimmenpartei");
	}

	// Get all parties
	public Map<Integer, String> getAllParties() throws SQLException {
		Map<Integer, String> parties = new HashMap<Integer, String>();

		ResultSet rs = stmtSelectParties.executeQuery();
		while (rs.next()) {
			parties.put(rs.getInt("id"), rs.getString("name"));
		}
		stmtSelectParties.close();
		con.close();
		return parties;
	}

	// Disconnect from the database
	@Override
	protected void finalize() throws Exception {
		System.out.println("Disconnecting ...");
		con.close();
		System.out.println("-> done!");
	}

	// Get zweitstimmen aggregations
	public BigInteger getZweitstimmenSperrklausel(Map<Integer, BigInteger> parties) throws SQLException {
		ResultSet rs = stmtSelectZweistimmenSperrklausel.executeQuery();
		BigInteger total = BigInteger.ZERO;
		while (rs.next()) {
			total.add(rs.getBigDecimal("stimmen").toBigInteger());
			parties.put(rs.getInt("id"), rs.getBigDecimal("stimmen").toBigInteger());
		}
		stmtSelectZweistimmenSperrklausel.close();
		con.close();
		return total;
	}

	public Map<Integer, BigInteger> getZweitstimmenForParty(Integer partyID) throws SQLException {
		Map<Integer, BigInteger> landeslisteVotes = new HashMap<Integer, BigInteger>();
		stmtSelectZweistimmenPartei.setInt(1, partyID);
		ResultSet rs = stmtSelectZweistimmenPartei.executeQuery();
		while (rs.next()) {
			landeslisteVotes.put(rs.getInt("landesliste"), rs.getBigDecimal("stimmen").toBigInteger());
		}
		stmtSelectZweistimmenPartei.close();
		con.close();
		return landeslisteVotes;
	}

	public List<String> getDirektkandidatenForLandesliste(Integer landeslisteID) throws SQLException {
		List<String> direktkandidaten = new LinkedList<String>();
		stmtSelectDirektkandidaten.setInt(1, landeslisteID);
		stmtSelectDirektkandidaten.setInt(2, landeslisteID);
		ResultSet rs = stmtSelectDirektkandidaten.executeQuery();
		while (rs.next()) {
			direktkandidaten.add(rs.getString("name"));
		}
		stmtSelectDirektkandidaten.close();
		con.close();

		return direktkandidaten;
	}

	public List<String> getListenkandidatenForLandesliste(Integer landeslisteID) throws SQLException {
		List<String> listenkandidaten = new LinkedList<String>();
		stmtSelectListenkandidaten.setInt(1, landeslisteID);
		ResultSet rs = stmtSelectListenkandidaten.executeQuery();
		while (rs.next()) {
			listenkandidaten.add(rs.getString("name"));
		}
		stmtSelectListenkandidaten.close();
		con.close();

		return listenkandidaten;
	}

	public void updateErststimmen() throws SQLException {
		stmtUpdateErststimmen.execute();
		stmtUpdateErststimmen.close();
	}

	public void updateZweitstimmen() throws SQLException {
		stmtUpdateZweitstimmen.execute();
		stmtUpdateZweitstimmenPartei.execute();
		stmtUpdateZweitstimmen.close();
		stmtUpdateZweitstimmenPartei.close();
	}
}
