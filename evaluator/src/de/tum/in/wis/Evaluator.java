package de.tum.in.wis;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Evaluator {

	final static Integer NUM_SEATS = 598;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		// Connect to database
		Database db;
		try {
			db = new Database();
			db.connect();
		} catch (Exception e) {
			System.out.println("Could not connect to/create to database!");
			e.printStackTrace();
			return;
		}

		// Update the vote aggregation for 2009
		updateVoteAggregation(db);

		// Compute seating
		try {
			computeSeating(db);
		} catch (SQLException e) {
			System.out.println("SQL Exception triggered...");
			e.printStackTrace();
			return;
		}

	}

	// Update the vote aggregation for 2009
	private static void updateVoteAggregation(Database db) throws SQLException {
		// Update Erststimmen aggregation
		db.updateErststimmen();

		// Update Zweitstimmen aggregation
		db.updateZweitstimmen();
	}

	public static void computeSeating(Database db) throws Exception {

		// Map of the parties
		Map<Integer, String> parties = db.getAllParties();

		// Map party id -> number of Zweitstimmen
		Map<Integer, BigInteger> zweitstimmen = new HashMap<Integer, BigInteger>();

		// Get zweitstimmen aggregations
		BigInteger total = db.getZweitstimmenSperrklausel(zweitstimmen);

		// Ensure that each party is in the map
		for (Integer party : parties.keySet()) {
			if (!zweitstimmen.containsKey(party)) {
				zweitstimmen.put(party, BigInteger.ZERO);
			}
		}

		// Compute seat distribution on party level
		Map<Integer, Integer> partySeats = computeSainteLague(zweitstimmen, NUM_SEATS, total);

		// Save number of Überhangmandate
		Integer ueberhangmandate = 0;

		// Compute members of parliament
		for (Entry<Integer, String> partyEntry : parties.entrySet()) {
			Integer seats = partySeats.get(partyEntry.getKey());

			System.out.println("Seat distribution for " + partyEntry.getValue() + " (" + seats.toString() + " seats)");

			// Skip party if no seats were assigned
			if (seats <= 0) {
				continue;
			}

			// Compute seat distribution on landesliste level
			Map<Integer, BigInteger> landeslisteVotes = db.getZweitstimmenForParty(partyEntry.getKey());
			Map<Integer, Integer> landeslisteSeatsMap = computeSainteLague(landeslisteVotes, seats,
					zweitstimmen.get(partyEntry.getKey()));

			// Populate seats
			for (Entry<Integer, Integer> landeslisteEntry : landeslisteSeatsMap.entrySet()) {
				Integer landeslisteSeats = landeslisteEntry.getValue();
				Integer landeslisteID = landeslisteEntry.getKey();
				System.out.println("- Candidates for Landesliste with ID " + landeslisteID + "(" + landeslisteSeats
						+ " seats)");

				// Get direktkandidaten
				List<String> direktkandidaten = db.getDirektkandidatenForLandesliste(landeslisteID);
				System.out.println("Direktkandidaten: " + direktkandidaten);

				// Skip empty landeslisten
				if (landeslisteSeats <= 0) {
					// All directly elected candidates --> Überhangmandate
					ueberhangmandate += direktkandidaten.size();
					continue;
				}

				// Get listenkandidaten
				Integer seatsLeft = landeslisteSeats - direktkandidaten.size();
				if (seatsLeft > 0) {
					List<String> listenkandidaten = db.getListenkandidatenForLandesliste(landeslisteID);

					// Account for candidates who are elected with a Direktmandat
					listenkandidaten.removeAll(direktkandidaten);

					// Fill remaining seats with list candidates
					listenkandidaten = listenkandidaten.subList(0, seatsLeft);

					System.out.println("Listenkandidaten: " + listenkandidaten);
				} else {
					ueberhangmandate += Math.abs(seatsLeft);
				}
			}
		}

	}

	private static Map<Integer, Integer> computeSainteLague(Map<Integer, BigInteger> votes, Integer seats,
			BigInteger totalVotes) throws Exception {

		Map<Integer, Integer> seatsMap = new HashMap<Integer, Integer>();
		// Fill seats map
		for (Integer party : votes.keySet()) {
			seatsMap.put(party, 0);
		}

		// Calculate first divisor
		Double divisor = totalVotes.doubleValue() / seats;

		// Try to find the solution
		while (true) {
			// Number of assigned seats
			Integer totalSeats = 0;

			// Calculate seats distribution
			for (Entry<Integer, Integer> partyEntry : seatsMap.entrySet()) {
				// Calculate party seats, ignoring
				Double partySeatsNotRounded = votes.get(partyEntry.getKey()).doubleValue() / divisor;
				// Round
				Integer partySeats = (int) Math.round(partySeatsNotRounded);

				// If difference is 0.5 then randomly round up/down
				// NB: Equality check for double values uses threshold!
				if (Math.abs(Math.abs(partySeats - partySeatsNotRounded) - 0.5) < 1e-5) {
					if (Math.random() >= 0.5) {
						// Round down
						partySeats = partySeats - 1;
					}
				}

				// Sum seats up and remember value
				totalSeats += partySeats;
				partyEntry.setValue(partySeats);
			}

			Integer factor;
			Boolean selectMin;
			// Check if finished
			if (totalSeats == NUM_SEATS) {
				return seatsMap;
			} else if (totalSeats > NUM_SEATS) {
				factor = -1;
				selectMin = true;
			} else {
				factor = +1;
				selectMin = false;
			}

			// Determine next divisor
			List<Double> divisorList = new LinkedList<Double>();

			for (Entry<Integer, Integer> partyEntry : seatsMap.entrySet()) {
				// Once minus/plus 0.5
				Double seatDivisor = partyEntry.getValue().doubleValue() + 0.5 * factor;
				if (seatDivisor <= 0) {
					continue;
				}
				Double partyDivisor = votes.get(partyEntry.getKey()).doubleValue() / seatDivisor;
				divisorList.add(partyDivisor);

				// Once minus/plus 1.5
				seatDivisor = partyEntry.getValue().doubleValue() + 1.5 * factor;
				if (seatDivisor <= 0) {
					continue;
				}
				partyDivisor = votes.get(partyEntry.getKey()).doubleValue() / seatDivisor;
				divisorList.add(partyDivisor);
			}

			// Should not be possible
			if (divisorList.isEmpty() || divisorList.size() < 2) {
				throw new Exception();
			}

			// Sort list
			Collections.sort(divisorList);

			if (selectMin) {
				// Get mean between minimum and second minimum
				divisor = (divisorList.get(1) + divisorList.get(0)) / 2;
			} else {
				// Get mean between maximum and second maximum
				divisor = (divisorList.get(divisorList.size() - 1) + divisorList.get(divisorList.size() - 2)) / 2;
			}

		}
	}

}
