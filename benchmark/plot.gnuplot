#!/opt/local/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.4 patchlevel 3
#    	last modified March 2011
#    	System: Darwin 10.8.0
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2010
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help seeking-assistance"
#    	immediate help:   type "help"
#    	plot window:      hit 'h'
# set terminal aqua 0 title "Figure 0" size 1000,500 font "Times-Roman,14" noenhanced solid
# set output
set terminal postscript eps enhanced defaultplex \
   leveldefault color colortext \
   solid dashlength 1.0 linewidth 1.0 butt noclip \
   palfuncparam 2000,0.003 \
   "Helvetica" 14
set output 'evaluation.eps'
#GNUTERM = "aqua"
# set yrange [0:1000]
set datafile separator ';'
set style data lines
set xlabel "Time t in ms (t=0 start of benchmark)"
set logscale y
set ylabel "Load time in ms"
set title t
plot pf using 1:2 title "Q1", pf using 3:4 title "Q2", pf using 5:6 title "Q3", pf using 5:6 title "Q4", pf using 7:8 title "Q5"
