package edu.tum.cs.wis.benchmark;

import java.math.BigInteger;
import java.util.List;

public class FrequencyNormalizer {
	
	/**
	 * Normalize frequency according to assignement
	 * @param frequencies
	 */
	public static void normalize(List<Integer> frequencies) {
		if(frequencies.size()<=1) return;
		Integer gcd = computeGCD(frequencies);
		for(Integer i = 0; i < frequencies.size(); i++){
			frequencies.set(i, frequencies.get(i)/gcd);
		}
			
	}
	/**
	 * Compute greatest common divisor for a set
	 * @return 
	 */
	private static Integer computeGCD(List<Integer> input){
		if(input.size() < 1) return 1;
		if(input.size() == 1) return input.get(0);
		// Split the set in two equal parts
		List<Integer> left = input.subList(0, input.size()/2);
		List<Integer> right = input.subList(input.size()/2, input.size());
		// Return gcd of the gcd of both sublists
		return computeGCD(computeGCD(left), computeGCD(right));
	}

	/**
	 * Compute greatest common divisor for two integers
	 * @param one
	 * @param two
	 */
	private static Integer computeGCD(Integer one, Integer two) {
		// Using the algorithm of BigInteger
		return BigInteger.valueOf(one).gcd(BigInteger.valueOf(two)).intValue();	
	}

}
