package edu.tum.cs.wis.benchmark;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * Benchmark application
 * 
 */
public class Benchmark {
	public static void main(String[] args){
		if(args.length < 4 || args.length % 2 != 0){
			System.out.println("usage: benchmark <num_terminals> <mean_waittime_in_s> <page1> <frequency1> <page2> <frequency 2> ...");
			return;
		}
		
		
		// Fetch parameters
		Integer numTerminals = Integer.valueOf(args[0]);
		Float meanWaitTime = Float.valueOf(args[1]);
		
		// Compute wait time in milliseconds
		Integer minWaitTime = (int)(meanWaitTime * 1000 * 0.8);
		Integer maxWaitTime = (int)(meanWaitTime * 1000 * 1.2);
		
		
		
		// Fetch workload
		List<Page> workload = new ArrayList<Page>();
		List<Integer> frequencies = new ArrayList<Integer>();
		for(int i = 2; i < args.length; i = i + 2){
			Page p = new Page(args[i]);
			Integer frequency = Integer.valueOf(args[i+1]);
			workload.add(p);
			frequencies.add(frequency);
		}
		
		// Normalize frequency
		FrequencyNormalizer.normalize(frequencies);
		System.out.println("Computed 'deck of cards': "+frequencies);
		
		// Benchmark
		System.out.println("Starting Benchmark (it will stop after 15 seconds)");
		StopWatch timestampWatch = new StopWatch();
		timestampWatch.start();
		
		// Create and start threads
		List<Terminal> terminals = new ArrayList<Terminal>();
		for(int i = 0; i < numTerminals; i++){
			Terminal t = new Terminal(new ArrayList<Page>(workload), new ArrayList<Integer>(frequencies), minWaitTime, maxWaitTime, timestampWatch,i+1);
			terminals.add(t);
			t.start();
		}
		
		// Wait 5 minutes
		try {
			Thread.sleep(15*1000);
		} catch (InterruptedException e) {}
		
		System.out.println("Stopping all threads");
		// Stop all threads
		for(Terminal t : terminals){
			t.closeTerminal();
		}
		// Wait until all threads stopped
		for(Terminal t : terminals){
			try {
				t.join();
			} catch (InterruptedException e) {
			}
		}
		
		System.out.println("Writing output to evaluation.csv");
		try {
			writeEvaluationFile(workload, terminals);
		} catch (IOException e) {
			System.out.println("Error writing evaluation file!");
		}
		
		// Trying to plot file
		String title = "Benchmark_{n="+numTerminals+";t="+meanWaitTime+"s}";
		String command = "gnuplot -p  -e pf=\"evaluation.csv\";t=\""+title+"\" plot.gnuplot";
		System.out.println("Trying to plot evaluation: "+ command);
		try {
		    // Execute command
			Process child = Runtime.getRuntime().exec(command);
			BufferedReader input = new BufferedReader(new InputStreamReader(child.getErrorStream()));
            String line=null;
            while((line=input.readLine()) != null) {
                System.out.println(line);
            }
            int exitVal = child.waitFor();
		    if(exitVal!=0)
		    	System.out.println("Plotting FAILED! Exit code:"+exitVal);
		} catch (IOException e) {
			System.out.println("Plotting FAILED!");
		} catch (InterruptedException e) {
			System.out.println("Plotting FAILED!");
		}
		System.out.println("Finished!");
		
		
	}

	private static void writeEvaluationFile(List<Page> workload,
			List<Terminal> terminals) throws IOException {
		FileWriter fw;
		fw = new FileWriter(new File("evaluation.csv"),false);
		// Fetch data
		Map<Page, List<ImmutablePair<Long,Long>>> evaluation = new HashMap<Page, List<ImmutablePair<Long,Long>>>();
		// For each page
		for(Page p : workload){
			// Put all measurements in one list
			List<ImmutablePair<Long,Long>> list = new LinkedList<ImmutablePair<Long,Long>>();
			for(Terminal t : terminals){
				list.addAll(t.getMeasurements().get(p));
			}
			// Sort it
			Collections.sort(list);
			// Put it into the Map
			evaluation.put(p, list);
		}
		// Write the csv
		int line = 0;
		// Write header
		for(Page p: workload){
			String caption = p.toString();
			if(caption.indexOf("/WIS/")>=0) caption = caption.substring(caption.indexOf("/WIS/")+4);
			fw.write(caption+";;");
		}
		fw.write("\n");
		line++;
		boolean allNull = false;
		while(!allNull){
			allNull = true;
			for(Page p: workload){	
				if(evaluation.get(p).size()>line-1){
					allNull = false;
					ImmutablePair<Long, Long> pair = evaluation.get(p).get(line-1);
					fw.write(pair.left+";"+pair.right+";");
				}
				else
					fw.write(";;");
			}
			line++;
			fw.write("\n");
		}
		fw.flush();
	}


}
