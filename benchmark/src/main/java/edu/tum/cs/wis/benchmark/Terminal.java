package edu.tum.cs.wis.benchmark;

import java.io.IOException;
import java.net.URI;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

/**
 * Simulates an interactive terminal
 * 
 * @author wolfst
 * 
 */
public class Terminal extends Thread {
	// The HTTP Client
	HttpClient client = new DefaultHttpClient();
	// The watch
	StopWatch watch = new StopWatch();
	
	// Timestamp watch
	StopWatch timestampWatch;

	// The workload
	List<Page> workload;
	List<Page> deck;

	// Waiting time
	Integer minWaitTime, maxWaitTime;

	// Random number generator
	Random rnd = new Random();
	
	// Indicates wether the thread should be stopped
	boolean stopped = false;
	
	// The terminal id
	Integer id;
	
	
	// The Result object
	Map<Page, List<ImmutablePair<Long, Long>>> measurements = new HashMap<Page, List<ImmutablePair<Long, Long>>>();

	/**
	 * Constructor
	 * 
	 * @param workload
	 * @param frequency
	 * @param minWaitTime
	 * @param maxWaitTime
	 */
	public Terminal(ArrayList<Page> workload, ArrayList<Integer> frequencies,
			Integer minWaitTime, Integer maxWaitTime, StopWatch timestampWatch, Integer id) {
		this.workload = workload;
		this.minWaitTime = minWaitTime;
		this.maxWaitTime = maxWaitTime;
		this.timestampWatch = timestampWatch;
		this.id = id;

		// Generate deck
		deck = new ArrayList<Page>();
		for (int i = 0; i < frequencies.size(); i++) {
			for (int num = 0; num < frequencies.get(i); num++)
				deck.add(workload.get(i));
		}
		// Fill measurments map
		for(Page p : workload){
			measurements.put(p, new ArrayList<ImmutablePair<Long, Long>>());
		}
		
		// Set timeout to 60 seconds (otherwise it will wait forever)
		HttpParams params = client.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 60000);
		HttpConnectionParams.setSoTimeout(params, 60000);
	}

	/**
	 * Run function
	 */
	public void run() {

		while (!stopped) {
			// Copy deck
			List<Page> currentDeck = new ArrayList<Page>(deck);

			while (currentDeck.size() > 0 && !stopped) {
				// Draw one card
				int card = rnd.nextInt(currentDeck.size());
				Page page = currentDeck.get(card);
				currentDeck.remove(card);

				// Start watch
				watch.start();

				String error = null;
				Integer bytesLoaded = 0;
				for (URI uri : page.getRequests()) {
					// Send request
					HttpGet httpget = new HttpGet(uri);
					// Receive response
					try {
						HttpResponse response = client.execute(httpget);
						// Get the number of Bytes
		                HttpEntity entity = response.getEntity();
		                if (entity != null) {
		                	bytesLoaded += EntityUtils.toByteArray(entity).length;
		                }
		                // Release connection
		                httpget.releaseConnection();
					} catch (ClientProtocolException e) {
						error = "Client Protocol Exception";
						httpget.abort();
						break;
					} catch (IOException e) {
						error = "Connection problem";
						httpget.abort();
						break;
					}
				}

				// Stop watch
				watch.stop();
				
				// Register measurement
				measurements.get(page).add(new ImmutablePair<Long, Long>(timestampWatch.getTime(), watch.getTime()));
				// Output
				DecimalFormat outputFormat = new DecimalFormat("#.##");
				Float KB = bytesLoaded/1024.0f;
				Float loadTime = watch.getTime() / 1000.0f;
				Float KBs = (bytesLoaded/1024f/loadTime);
				System.out.print("Terminal " + id + ": Loaded " + page + "("+outputFormat.format(KB)+"KB) in "
						+ loadTime.toString() + " s ("+ outputFormat.format(KBs)+"kB/s)");
				if(error == null)		
					System.out.println("");
				else
					System.out.println(" WITH ERROR: '"+error+"'!");
				watch.reset();
				
				// Wait [minWaitTime, maxWaitTime] milliseconds
				try {
					sleep(rnd.nextInt(maxWaitTime-minWaitTime)+minWaitTime);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	/**
	 * Get evaulation
	 */
	public Map<Page, List<ImmutablePair<Long, Long>>> getMeasurements() {
		return measurements;
	}
	
	/**
	 * Stop the thread
	 */
	public void closeTerminal(){
		stopped = true;
	}
}
