package edu.tum.cs.wis.benchmark;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a page
 * 
 * @author wolfst
 * 
 */
public class Page {

	// All request, which are executed when the page is loaded
	List<URI> requests = new ArrayList<URI>();

	/**
	 * Constructor
	 * 
	 * @param uri
	 */
	public Page(String uri) {
		// Add page request
		try {
			requests.add(new URI(uri));
			// Add the AJAX request
			addAjaxRequests(uri);
		} catch (URISyntaxException e) {
			System.out.println("ERROR: URI is not valid: '" + uri
					+ "'. Ignoring request.");
		}

		

	}

	/**
	 * Add the AJAX request, which are executed when the page is loaded
	 */
	private void addAjaxRequests(String uri) {
		try {
			// Main page
			if (uri.contains("WIS/index.html")) {
				requests.add(new URI(uri.replace("WIS/index.html",
						"WIS/rest/charts/totalSeating/2009")));
			}else if (uri.contains("WIS/ueberhangmandate.html")) {
				requests.add(new URI(uri.replace("WIS/ueberhangmandate.html",
						"WIS/rest/charts/ueberhangMandatePartei/2009")));
				requests.add(new URI(uri.replace("WIS/ueberhangmandate.html",
						"WIS/rest/charts/ueberhangMandateBL/2009")));
			}
		} catch (URISyntaxException e) {
			System.out.println("ERROR: Wrong AJAX requests defined in code!");
		}
	}
	
	/**
	 * Converts object to String
	 */
	public String toString() {
		if(requests.size()<1) return "N/A";
		else return requests.get(0).toString();
	}
	
	public List<URI> getRequests(){
		return requests;
	}

}
