package edu.tum.cs.wis.model;

import java.io.Serializable;

public interface IZweitstimmenLandesliste extends Serializable {
	public Landesliste getLandesliste();

	public void setLandesliste(Landesliste landesliste);

	public Integer getStimmen();

	public void setStimmen(Integer stimmen);

	public Wahlkreis getWahlkreis();

	public void setWahlkreis(Wahlkreis wahlkreis);
}
