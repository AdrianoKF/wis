package edu.tum.cs.wis.model;

/**
 * Class represents Triples of different types
 * 
 * @author wolfst
 * 
 * @param <A>
 * @param <B>
 * @param <C>
 */
public class Quadruple<A, B, C, D> {
	private A one;
	private B two;
	private C three;
	private D four;

	/**
	 * Constructor: Pass an object array of at least 4 elements. Implicit cast.
	 */
	@SuppressWarnings("unchecked")
	public Quadruple(Object[] in) {
		one = (A) in[0];
		two = (B) in[1];
		three = (C) in[2];
		four = (D) in[3];
	}

	public Quadruple(A a, B b, C c, D d) {
		one = a;
		two = b;
		three = c;
		four = d;
	}

	// Setter and getter
	public void setFirst(A one) {
		this.one = one;
	}

	public void setSecond(B two) {
		this.two = two;
	}

	public void setThird(C three) {
		this.three = three;
	}

	public void setFour(D four) {
		this.four = four;
	}

	public A getFirst() {
		return one;
	}

	public B getSecond() {
		return two;
	}

	public C getThird() {
		return three;
	}

	public D getFourth() {
		return four;
	}

	@Override
	public String toString() {
		return one.toString() + ", " + two.toString() + ", " + three.toString() + ", "
				+ four.toString();
	}

}
