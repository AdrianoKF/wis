package edu.tum.cs.wis.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Mandatsart", discriminatorType = DiscriminatorType.STRING)
@Table(schema = "WIS", name = "Sitzverteilung")
public class Mandat implements Serializable {
	private static final long serialVersionUID = -7311540000810720503L;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Bewerber", updatable = false)
	@Id
	private Bewerber bewerber;
	
	private String mandatsArt;
	private Integer jahr;

	public Bewerber getBewerber() {
		return bewerber;
	}

	public void setBewerber(Bewerber bewerber) {
		this.bewerber = bewerber;
	}

	public String getMandatsArt() {
		return mandatsArt;
	}

	public void setMandatsArt(String mandatsArt) {
		this.mandatsArt = mandatsArt;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
}
