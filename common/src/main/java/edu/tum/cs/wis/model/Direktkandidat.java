package edu.tum.cs.wis.model;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(schema = "WIS", name = "Direktkandidat")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Cacheable
public class Direktkandidat {
	@Id
	@GeneratedValue
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Bewerber")
	private Bewerber bewerber;

	@ManyToOne
	@JoinColumn(name = "Wahlkreis")
	private Wahlkreis wahlkreis;
	
	private Integer jahr;
	
	@Override
	public String toString() {
		return String.format("[ Direktkandidat, id=%d, jahr=%d ], %s", id, jahr, bewerber);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bewerber getBewerber() {
		return bewerber;
	}

	public void setBewerber(Bewerber bewerber) {
		this.bewerber = bewerber;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
	
	public String getFullName() {
		return bewerber.getFullName();
	}
	
	public String getParteiName() {
		return bewerber.getParteiName(jahr);
	}
	
	public Partei getPartei() {
		return bewerber.getPartei(jahr);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bewerber == null) ? 0 : bewerber.hashCode());
		result = prime * result + ((jahr == null) ? 0 : jahr.hashCode());
		result = prime * result + ((wahlkreis == null) ? 0 : wahlkreis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Direktkandidat)) {
			return false;
		}
		Direktkandidat other = (Direktkandidat) obj;
		if (getBewerber() == null) {
			if (other.getBewerber() != null) {
				return false;
			}
		} else if (!getBewerber().equals(other.getBewerber())) {
			return false;
		}
		if (jahr == null) {
			if (other.jahr != null) {
				return false;
			}
		} else if (!jahr.equals(other.jahr)) {
			return false;
		}
		if (getWahlkreis() == null) {
			if (other.getWahlkreis() != null) {
				return false;
			}
		} else if (!getWahlkreis().equals(other.getWahlkreis())) {
			return false;
		}
		return true;
	}	
}
