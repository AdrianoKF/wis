package edu.tum.cs.wis.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "Listenkandidat")
public class Listenkandidat {
	@Id
	@GeneratedValue
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Landesliste")
	private Landesliste landesliste;

	private Integer rang;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Landesliste getLandesliste() {
		return landesliste;
	}

	public void setLandesliste(Landesliste landesliste) {
		this.landesliste = landesliste;
	}

	public Integer getRang() {
		return rang;
	}

	public void setRang(Integer rang) {
		this.rang = rang;
	}
}
