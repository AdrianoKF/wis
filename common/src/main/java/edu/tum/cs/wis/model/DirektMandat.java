package edu.tum.cs.wis.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "DM")
public class DirektMandat extends Mandat {
	private static final long serialVersionUID = -3364930357677779926L;

}
