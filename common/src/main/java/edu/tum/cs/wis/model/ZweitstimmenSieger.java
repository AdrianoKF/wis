package edu.tum.cs.wis.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(schema = "WIS", name = "ZweitstimmenSieger")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class ZweitstimmenSieger implements Serializable {
	private static final long serialVersionUID = -741341741279837680L;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Landesliste", updatable = false, insertable = false)
	@Id
	private Landesliste landesliste;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Wahlkreis", updatable = false, insertable = false)
	@Id
	private Wahlkreis wahlkreis;

	private Integer jahr;
	
	@Override
	public String toString() {
		if (getLandesliste() == null) {
			return "";
		}
		return getLandesliste().getParteiName();
	}

	public Landesliste getLandesliste() {
		return landesliste;
	}

	public void setLandesliste(Landesliste landesliste) {
		this.landesliste = landesliste;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
}
