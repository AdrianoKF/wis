package edu.tum.cs.wis.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.tum.cs.wis.model.Quadruple;
import edu.tum.cs.wis.model.Triple;

public class DBTypeUtils {
	protected DBTypeUtils() {
		
	}
	
	/**
	 * Converts a list of Object[] (Key=Object[0], Value=Object[1]) to a
	 * hashmap.
	 * 
	 * @param list
	 * @return
	 */
	public static <K, V> Map<K, V> makeMap(List<Object[]> list) {
		return makeMap(list.iterator());
	}

	/**
	 * Converts a collection of Object[] (Key=Object[0], Value=Object[1])
	 * obtained from an iterator to a hashmap.
	 * 
	 * @param list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> makeMap(Iterator<Object[]> it) {
		Map<K, V> res = new HashMap<K, V>();
		while (it.hasNext()) {
			Object[] current = it.next();
			res.put((K) current[0], (V) current[1]);
		}
		return res;
	}

	/**
	 * Converts a list of triples of objects to a list of triples (implicit
	 * cast)
	 * 
	 * @param list
	 * @return
	 */
	public static <A, B, C> List<Triple<A, B, C>> makeTripleList(
			List<Object[]> list) {
		List<Triple<A, B, C>> res = new LinkedList<Triple<A, B, C>>();
		for (Object[] it : list) {
			res.add(new Triple<A, B, C>(it));
		}
		return res;
	}

	/**
	 * Converts a list of quadruples of objects to a list of quadruples
	 * (implicit cast)
	 * 
	 * @param list
	 * @return
	 */
	public static <A, B, C, D> List<Quadruple<A, B, C, D>> makeQuadrupleList(
			List<Object[]> list) {
		List<Quadruple<A, B, C, D>> res = new LinkedList<Quadruple<A, B, C, D>>();
		for (Object[] it : list) {
			res.add(new Quadruple<A, B, C, D>(it));
		}
		return res;
	}
}
