package edu.tum.cs.wis.dao;

import java.io.Serializable;

import org.hibernate.Session;

public abstract class GenericDAO<T, PK extends Serializable> {
	protected Session session;
	protected Class<T> type;

	protected GenericDAO(Class<T> type) {
		session = HibernateUtil.getSessionFactory().openSession();
		this.type = type;
	}

	/**
	 * Retrieves a persistent entity by its primary key value.
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T get(PK id) {
		session.beginTransaction();
		T result = (T) session.get(type, id);
		session.getTransaction().commit();
		return result;
	}
	
	/**
	 * Persists a fresh entity or updates a persistent entity.
	 * @param t
	 * @return The primary key value of the persisted or updated entity.
	 */
	@SuppressWarnings("unchecked")
	public PK saveOrUpdate(T t) {
		if (t == null) {
			return null;
		}
		
		session.beginTransaction();
		
		session.saveOrUpdate(t);
		PK id = (PK) session.getIdentifier(t);
			
		session.getTransaction().commit();
		return id;
	}
	
	/**
	 * Deletes a persistent entity.
	 * @param t
	 */
	public void delete(T t) {
		if (t == null) {
			return;
		}
		session.beginTransaction();
		session.delete(t);
		session.getTransaction().commit();
	}
}
