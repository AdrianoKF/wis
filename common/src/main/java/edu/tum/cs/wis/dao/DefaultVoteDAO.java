package edu.tum.cs.wis.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Default;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;

import edu.tum.cs.wis.model.Bewerber;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.Erststimmen;
import edu.tum.cs.wis.model.IErststimmen;
import edu.tum.cs.wis.model.IZweitstimmenLandesliste;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Mandat;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Quadruple;
import edu.tum.cs.wis.model.Waehler;
import edu.tum.cs.wis.model.Wahlkreis;
import edu.tum.cs.wis.model.Wahlzettel;
import edu.tum.cs.wis.model.ZweitstimmenLandesliste;
import edu.tum.cs.wis.model.ZweitstimmenPartei;
import edu.tum.cs.wis.model.ZweitstimmenSieger;

@Default
@SuppressWarnings("unchecked")
public class DefaultVoteDAO implements VoteDAO {
	protected Session session;

	public DefaultVoteDAO() {
		session = HibernateUtil.getSessionFactory().openSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getDirektkandidaten(int)
	 */
	@Override
	public List<Direktkandidat> getDirektkandidaten(int year) {
		session.beginTransaction();
		List<Direktkandidat> result = (List<Direktkandidat>) session
				.createQuery(
						"from Direktkandidat dk "
								+ "left join fetch dk.wahlkreis "
								+ "where dk.jahr = ? "
								+ "order by dk.bewerber.name, dk.bewerber.vorname ")
				.setInteger(0, year).list();
		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getWahlkreise()
	 */
	@Override
	public List<Wahlkreis> getWahlkreise() {
		session.beginTransaction();
		List<Wahlkreis> result = (List<Wahlkreis>) session
				.createCriteria(Wahlkreis.class).addOrder(Order.asc("id"))
				.list();
		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getBundeslaender()
	 */
	@Override
	public List<Bundesland> getBundeslaender() {
		session.beginTransaction();
		List<Bundesland> result = (List<Bundesland>) session
				.createCriteria(Bundesland.class)
				.addOrder(Order.asc("name"))
				.list();
		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getParteien()
	 */
	@Override
	public List<Partei> getParteien() {
		session.beginTransaction();
		List<Partei> result = (List<Partei>) session.createQuery("from Partei")
				.list();
		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getParteizugehoerigkeiten(int)
	 */
	@Override
	public Map<Bewerber, Partei> getParteizugehoerigkeiten(int jahr) {
		session.beginTransaction();
		List<Object[]> result = (List<Object[]>) session
				.createQuery(
						"select p.bewerber, p.partei from Parteizugehoerigkeit p where p.jahr = ?")
				.setInteger(0, jahr).list();
		session.getTransaction().commit();

		return DBTypeUtils.makeMap(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getWahlkreis(java.lang.Long)
	 */
	@Override
	public Wahlkreis getWahlkreis(Long id) {
		session.beginTransaction();
		Wahlkreis result = (Wahlkreis) session.get(Wahlkreis.class, id);
		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#findWahlkreisByName(java.lang.String)
	 */
	@Override
	public Wahlkreis findWahlkreisByName(String name) {
		session.beginTransaction();
		Wahlkreis result = (Wahlkreis) session
				.createQuery("from Wahlkreis where name = ?")
				.setString(0, name).uniqueResult();

		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#getDirektkandidaten(edu.tum.cs.wis.model.
	 * Wahlkreis, int)
	 */
	@Override
	public List<Direktkandidat> getDirektkandidaten(Wahlkreis wahlkreis,
			int jahr) {
		session.beginTransaction();
		List<Direktkandidat> result = session
				.createCriteria(Direktkandidat.class)
				.createAlias("bewerber", "b")
				.add(Restrictions.eq("wahlkreis", wahlkreis))
				.add(Restrictions.eq("jahr", jahr))
				.addOrder(Order.asc("b.name")).addOrder(Order.asc("b.vorname"))
				.setFetchMode("wahlkreis", FetchMode.JOIN).list();
		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#findLandesliste(edu.tum.cs.wis.model.Partei,
	 * edu.tum.cs.wis.model.Bundesland, int)
	 */
	@Override
	public Landesliste findLandesliste(Partei partei, Bundesland bundesland,
			int jahr) {
		if (partei == null || bundesland == null) {
			return null;
		}

		session.beginTransaction();
		Landesliste result = (Landesliste) session
				.createQuery(
						"from Landesliste l where l.partei = ? and l.bundesland = ? and l.jahr = ?")
				.setEntity(0, partei).setEntity(1, bundesland)
				.setInteger(2, jahr).uniqueResult();
		session.getTransaction().commit();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#getLandeslisten(edu.tum.cs.wis.model.Wahlkreis
	 * , int)
	 */
	@Override
	public List<Landesliste> getLandeslisten(Wahlkreis wahlkreis, int jahr) {
		if (wahlkreis == null) {
			return Collections.emptyList();
		}

		session.beginTransaction();
		List<Landesliste> result = (List<Landesliste>) session
				.createCriteria(Landesliste.class)
				.add(Restrictions.eq("bundesland", wahlkreis.getBundesland()))
				.add(Restrictions.eq("jahr", jahr))
				.setFetchMode("partei", FetchMode.JOIN).list();

		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#getZweitstimmen(edu.tum.cs.wis.model.Partei,
	 * int)
	 */
	@Override
	public int getZweitstimmen(Partei partei, int jahr) {
		if (partei == null) {
			return -1;
		}

		session.beginTransaction();
		Object result = session
				.createSQLQuery(
						"SELECT stimmen FROM WIS.ZWEITSTIMMENPARTEI WHERE partei = ? and jahr = ?")
				.setLong(0, partei.getId()).setInteger(1, jahr).uniqueResult();
		session.getTransaction().commit();

		if (result == null) {
			return -1;
		}
		return (Integer) result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getAllZweitstimmen(int)
	 */
	@Override
	public Map<Partei, Integer> getAllZweitstimmen(int jahr) {
		session.beginTransaction();
		List<Object[]> result = (List<Object[]>) session
				.createSQLQuery(
						"SELECT p.*, stimmen FROM wis.Partei p LEFT OUTER JOIN wis.zweitstimmenpartei zsp on (p.id = zsp.Partei) WHERE jahr = ?")
				.addEntity(Partei.class)
				.addScalar("Stimmen", StandardBasicTypes.INTEGER)
				.setInteger(0, jahr).list();
		session.getTransaction().commit();
		return DBTypeUtils.makeMap(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getAbgeordnete(int)
	 */
	@Override
	public List<Mandat> getAbgeordnete(int jahr) {
		session.beginTransaction();

		List<Mandat> result = (List<Mandat>) session
				.createCriteria(Mandat.class)
				.add(Restrictions.eq("jahr", jahr))
				.setFetchMode("Bewerber", FetchMode.JOIN)
				.setFetchMode("Bewerber.parteien", FetchMode.JOIN).list();

		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#fetchZweitstimmen(edu.tum.cs.wis.model.Wahlkreis
	 * , int)
	 */
	@Override
	public List<IZweitstimmenLandesliste> fetchZweitstimmen(
			Wahlkreis wahlkreis, int jahr) {
		session.beginTransaction();

		List<IZweitstimmenLandesliste> result = (List<IZweitstimmenLandesliste>) session
				.createQuery(
						"from ZweitstimmenLandesliste where wahlkreis = ? and landesliste.jahr = ? order by stimmen desc")
				.setEntity(0, wahlkreis).setInteger(1, jahr).list();

		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#fetchErststimmen(edu.tum.cs.wis.model.Wahlkreis
	 * , int)
	 */
	@Override
	public List<IErststimmen> fetchErststimmen(Wahlkreis wahlkreis, int jahr) {
		session.beginTransaction();
		List<IErststimmen> result = (List<IErststimmen>) session
				.createQuery(
						"from Erststimmen where kandidat.wahlkreis = ? and kandidat.jahr = ? order by stimmen desc")
				.setEntity(0, wahlkreis).setInteger(1, jahr).list();

		session.getTransaction().commit();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getUeberhangmandateProPartei(int)
	 */
	@Override
	public Map<Partei, Integer> getUeberhangmandateProPartei(int jahr) {
		session.beginTransaction();
		List<Object[]> result = session
				.createSQLQuery(
						"SELECT {p.*}, sum(u.ueberhang) as Ueberhang "
								+ "FROM WIS.Ueberhangmandate_Partei u "
								+ "LEFT JOIN WIS.Partei p on (p.Id = u.Partei) "
								+ "WHERE u.jahr = ? " + "GROUP BY p.id, p.name")
				.addEntity("p", Partei.class)
				.addScalar("Ueberhang", StandardBasicTypes.INTEGER)
				.setInteger(0, jahr).list();
		session.getTransaction().commit();
		return DBTypeUtils.makeMap(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getUeberhangmandateProBundesland(int)
	 */
	@Override
	public Map<Bundesland, Integer> getUeberhangmandateProBundesland(int jahr,
			boolean returnAlsoEmpty) {
		String sqlQuery = "SELECT bl.*, coalesce(sum(ueberhang), 0) as ueberhang ";
		if (!returnAlsoEmpty) {
			// Return all bundeslaender
			sqlQuery += "FROM wis.anzueberhangmandate uh"
					+ " left join wis.landesliste ll on uh.landesliste = ll.id"
					+ " left join wis.bundesland bl on ll.bundesland = bl.id ";
		} else {
			sqlQuery += "FROM wis.anzueberhangmandate uh"
					+ " right join wis.landesliste ll on uh.landesliste = ll.id"
					+ " right join wis.bundesland bl on ll.bundesland = bl.id ";
		}

		sqlQuery += "WHERE jahr = ? GROUP BY bl.id, bl.name";
		session.beginTransaction();
		List<Object[]> result = session.createSQLQuery(sqlQuery)
				.addEntity(Bundesland.class)
				.addScalar("Ueberhang", StandardBasicTypes.INTEGER)
				.setInteger(0, jahr).list();
		session.getTransaction().commit();
		return DBTypeUtils.makeMap(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getKnappsteGewinner(int)
	 */
	@Override
	public List<Quadruple<Bewerber, Partei, BigInteger, Integer>> getKnappsteGewinner(
			int jahr) {
		session.beginTransaction();
		List<Object[]> result = session
				.createSQLQuery(
						"SELECT {b.*}, {p.*}, vorsprung, rang "
								+ "FROM wis.knappsteGewinner k left join wis.bewerber b on b.id = k.bewerber left join wis.partei p on p.id = k.partei "
								+ "WHERE jahr = ? and partei is not null "
								+ "ORDER BY p.name, rang")
				.addEntity("b", Bewerber.class).addEntity("p", Partei.class)
				.addScalar("vorsprung", StandardBasicTypes.BIG_INTEGER)
				.addScalar("rang", StandardBasicTypes.INTEGER)
				.setInteger(0, jahr).list();
		session.getTransaction().commit();
		return DBTypeUtils.makeQuadrupleList(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.tum.cs.wis.dao.IVoteDAO#getTotalSeating(int)
	 */
	@Override
	public Map<Partei, Integer> getTotalSeating(int year) {
		session.beginTransaction();
		List<Object[]> result = (List<Object[]>) session
				.createSQLQuery(
						"SELECT {p.*}, coalesce(sp.sitze + uh.ueberhang, sp.sitze) as sitze"
								+ " FROM WIS.SitzePartei sp"
								+ " JOIN WIS.Partei p ON (p.Id = sp.partei)"
								+ " LEFT OUTER JOIN WIS.Ueberhangmandate_Partei uh ON (uh.Partei = p.Id AND uh.jahr = sp.jahr)"
								+ " WHERE sp.jahr = ?" + " ORDER BY p.name ASC")
				.addEntity("p", Partei.class)
				.addScalar("sitze", StandardBasicTypes.INTEGER)
				.setInteger(0, year).list();
		session.getTransaction().commit();
		return DBTypeUtils.makeMap(result);
	}

	@Override
	public double getWahlbeteiligung(Wahlkreis wahlkreis, int year) {
		session.beginTransaction();
		BigDecimal result = (BigDecimal) session
				.createSQLQuery(
						"SELECT sum(stimmen) * 1.0 / wk.wahlberechtigte"
								+ " FROM wis.zweitstimmen zs"
								+ " JOIN wis.wahlkreis wk on (wk.id = zs.wahlkreis)"
								+ " LEFT JOIN wis.landesliste ll on (ll.id = zs.landesliste AND ll.jahr = ?)"
								+ " WHERE wk.id = ?" + " AND ll.jahr = zs.jahr"
								+ " GROUP BY zs.wahlkreis, wk.wahlberechtigte")
				.setInteger(0, year).setEntity(1, wahlkreis).uniqueResult();
		session.getTransaction().commit();
		return result.doubleValue();
	}

	@Override
	public List<ZweitstimmenSieger> getZweitstimmenSieger(int year) {
		session.beginTransaction();
		List<ZweitstimmenSieger> result = (List<ZweitstimmenSieger>) session
				.createCriteria(ZweitstimmenSieger.class)
				.add(Restrictions.eq("jahr", year))
				.setFetchMode("Wahlkreis", FetchMode.JOIN)
				.setFetchMode("Landesliste", FetchMode.JOIN).list();

		session.getTransaction().commit();
		return result;
	}

	@Override
	public ZweitstimmenSieger getZweitstimmenSieger(int year, Wahlkreis kreis) {
		session.beginTransaction();
		ZweitstimmenSieger result = (ZweitstimmenSieger) session
				.createCriteria(ZweitstimmenSieger.class)
				.add(Restrictions.eq("jahr", year))
				.add(Restrictions.eq("wahlkreis", kreis))
				.setFetchMode("Landesliste", FetchMode.JOIN).uniqueResult();

		session.getTransaction().commit();
		return result;
	}

	private void addErststimme(int year, Wahlkreis wahlkreis,
			Direktkandidat direktkandidat) {
		Erststimmen es = (Erststimmen) session
				.createCriteria(Erststimmen.class)
				.add(Restrictions.eq("kandidat", direktkandidat))
				.add(Restrictions.eq("wahlkreis", wahlkreis))
				.add(Restrictions.eq("jahr", year)).uniqueResult();

		if (es == null) {
			es = new Erststimmen();
			es.setJahr(year);
			es.setKandidat(direktkandidat);
			es.setWahlkreis(wahlkreis);
			es.setStimmen(0);
		}

		Integer stimmen = es.getStimmen();
		if (stimmen == null) {
			stimmen = 0;
		}

		es.setStimmen(stimmen + 1);
		session.saveOrUpdate(es);
	}
	
	private void addZweitstimme(int year, Partei partei) {
		ZweitstimmenPartei zp = (ZweitstimmenPartei) session
				.createCriteria(ZweitstimmenPartei.class)
				.add(Restrictions.eq("partei", partei))
				.add(Restrictions.eq("jahr", year)).uniqueResult();

		if (zp == null) {
			zp = new ZweitstimmenPartei();
			zp.setJahr(year);
			zp.setPartei(partei);
			zp.setStimmen(0);
		}

		Integer stimmen = zp.getStimmen();
		if (stimmen == null) {
			stimmen = 0;
		}

		zp.setStimmen(stimmen + 1);
		session.saveOrUpdate(zp);
	}

	private void addZweitstimme(int year, Wahlkreis wahlkreis,
			Landesliste landesliste) {
		ZweitstimmenLandesliste zs = (ZweitstimmenLandesliste) session
				.createCriteria(ZweitstimmenLandesliste.class)
				.add(Restrictions.eq("landesliste", landesliste))
				.add(Restrictions.eq("wahlkreis", wahlkreis))
				.add(Restrictions.eq("jahr", year)).uniqueResult();

		if (zs == null) {
			zs = new ZweitstimmenLandesliste();
			zs.setJahr(year);
			zs.setLandesliste(landesliste);
			zs.setWahlkreis(wahlkreis);
			zs.setStimmen(0);
		}

		Integer stimmen = zs.getStimmen();
		if (stimmen == null) {
			stimmen = 0;
		}

		zs.setStimmen(stimmen + 1);
		session.saveOrUpdate(zs);
	}

	@Override
	public void addStimmen(int year, Wahlkreis wahlkreis,
			Direktkandidat direktkandidat, Landesliste landesliste, Waehler waehler) {
		session.beginTransaction();
		
		// Save vote
		Wahlzettel wahlzettel = new Wahlzettel();
		wahlzettel.setKandidat(direktkandidat);
		wahlzettel.setLandesliste(landesliste);
		wahlzettel.setJahr(year);
		wahlzettel.setWahlkreis(wahlkreis);
		session.save(wahlzettel);
		
		// Set abgestimmt
		waehler.setAbgestimmt(new Date());
		session.update(waehler);

		// Update aggregated results if applicable
		if (direktkandidat != null) {
			addErststimme(year, wahlkreis, direktkandidat);
		}
		
		if (landesliste != null) {
			addZweitstimme(year, wahlkreis, landesliste);
			addZweitstimme(year, landesliste.getPartei());
		}

		session.getTransaction().commit();
	}
}
