package edu.tum.cs.wis.dao;

import java.util.List;

import org.apache.log4j.Logger;

import edu.tum.cs.wis.model.IErststimmen;
import edu.tum.cs.wis.model.IZweitstimmenLandesliste;
import edu.tum.cs.wis.model.Wahlkreis;

/**
 * @author wolfst
 * This DAO is used by wahlkreisBean to compute the results directly based on the Wahlzettel
 */
@DVoteDAO
@SuppressWarnings("unchecked")
public class DirectVoteDAO extends DefaultVoteDAO  implements VoteDAO{
	private final Logger log = Logger.getLogger(DirectVoteDAO.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#fetchZweitstimmen(edu.tum.cs.wis.model.Wahlkreis
	 * , int)
	 */
	@Override
	public List<IZweitstimmenLandesliste> fetchZweitstimmen(Wahlkreis wahlkreis,
			int jahr) {
		log.info("Fetching Zweitstimmen directly ...");
		session.beginTransaction();
		// d.jahr = landesliste.jahr necessary for db optimizer
		List<IZweitstimmenLandesliste> result = (List<IZweitstimmenLandesliste>) session
				.createQuery(
						"from ZweitstimmenLandeslisteDirect d where wahlkreis = ? and d.jahr = landesliste.jahr and landesliste.jahr = ? order by stimmen desc")
				.setEntity(0, wahlkreis).setInteger(1, jahr).list();

		session.getTransaction().commit();
		return result;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.tum.cs.wis.dao.IVoteDAO#fetchErststimmen(edu.tum.cs.wis.model.Wahlkreis
	 * , int)
	 */
	@Override
	public List<IErststimmen> fetchErststimmen(Wahlkreis wahlkreis, int jahr) {
		log.info("Fetching Erststimmen directly ...");

		session.beginTransaction();
		// The reason for kandidat.wahlkreis = e.wahlkreis and kandidat.jahr = e.jahr is that then the optimizer can use this information and chooses the right index
		List<IErststimmen> result = (List<IErststimmen>) session
				.createQuery(
						"from ErststimmenDirect e where kandidat.wahlkreis = e.wahlkreis and kandidat.jahr = e.jahr and kandidat.wahlkreis = ? and kandidat.jahr = ? order by stimmen desc")
				.setEntity(0, wahlkreis).setInteger(1, jahr).list();

		session.getTransaction().commit();
		return result;
	}
}