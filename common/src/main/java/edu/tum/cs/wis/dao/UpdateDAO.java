package edu.tum.cs.wis.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.enterprise.inject.Default;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;

@Default
public class UpdateDAO {
	protected Session session;

	public UpdateDAO() {
		session = HibernateUtil.createSessionFactory().openSession();
	}

	public void updateErststimmen() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement(
						"DELETE FROM WIS.Erststimmen WHERE jahr = 2009")
						.execute();
				conn.prepareStatement(
						"INSERT INTO WIS.Erststimmen ( "
								+ "SELECT wahlkreis, direktkandidat, jahr, count(*) "
								+ "FROM WIS.Wahlzettel w "
								+ "GROUP BY wahlkreis, direktkandidat, jahr)")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateZweitstimmen() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("DELETE FROM WIS.Zweitstimmen WHERE jahr = 2009")
						.execute();
				conn.prepareStatement("INSERT INTO WIS.Zweitstimmen ( "
						+ "SELECT Wahlkreis, landesliste, jahr, count(*) "
						+ "FROM wis.Wahlzettel w "
						+ "GROUP BY Wahlkreis, jahr, landesliste)")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateZweitstimmenPartei() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("DELETE FROM WIS.ZweitstimmenPartei WHERE jahr = 2009")
						.execute();
				conn.prepareStatement("INSERT INTO WIS.ZweitstimmenPartei ( "
						+ "SELECT p.id, z.jahr, sum(stimmen) "
						+ "FROM WIS.Zweitstimmen z, WIS.Partei p, WIS.Landesliste l "
						+ "WHERE p.id = l.partei AND l.id = z.landesliste "
						+ "GROUP BY z.jahr, p.id " + "UNION "
						+ "SELECT null, z.jahr, sum(stimmen) "
						+ "FROM WIS.Zweitstimmen z " + "WHERE landesliste is null "
						+ "GROUP BY z.jahr)")
						.execute();
				conn.commit();
			}
		});

	}

	public void updateSitzverteilung() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("REFRESH TABLE WIS.SITZVERTEILUNG")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateKnappsteGewinner() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("REFRESH TABLE WIS.KNAPPSTEGEWINNER")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateSitzePartei() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("REFRESH TABLE WIS.SITZEPARTEI")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateZweitstimmenSieger() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("REFRESH TABLE WIS.ZweitstimmenSieger")
						.execute();
				conn.commit();
			}
		});
	}

	public void updateAnzUeberhangmandate() {
		session.doWork(new Work() {
			@Override
			public void execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				conn.prepareStatement("REFRESH TABLE WIS.ANZUEBERHANGMANDATE")
						.execute();
				conn.commit();
			}
		});
	}
}
