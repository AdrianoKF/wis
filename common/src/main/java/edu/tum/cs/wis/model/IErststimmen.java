package edu.tum.cs.wis.model;

import java.io.Serializable;

public interface IErststimmen extends Serializable {
	public Direktkandidat getKandidat();

	public void setKandidat(Direktkandidat kandidat);

	public Wahlkreis getWahlkreis();
	
	public void setWahlkreis(Wahlkreis wahlkreis);

	public Integer getJahr();

	public void setJahr(Integer jahr);

	public Integer getStimmen();

	public void setStimmen(Integer stimmen);
}
