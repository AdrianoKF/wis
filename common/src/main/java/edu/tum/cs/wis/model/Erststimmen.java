package edu.tum.cs.wis.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "Erststimmen")
public class Erststimmen implements IErststimmen {
	private static final long serialVersionUID = -1877058402852354974L;

	@Id
	@OneToOne
	@JoinColumn(name = "Direktkandidat")
	private Direktkandidat kandidat;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "Wahlkreis")
	private Wahlkreis wahlkreis;
	
	@Id
	private Integer jahr;
	
	private Integer stimmen;

	public Direktkandidat getKandidat() {
		return kandidat;
	}

	public void setKandidat(Direktkandidat kandidat) {
		this.kandidat = kandidat;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

	public Integer getStimmen() {
		return stimmen;
	}

	public void setStimmen(Integer stimmen) {
		this.stimmen = stimmen;
	}
}
