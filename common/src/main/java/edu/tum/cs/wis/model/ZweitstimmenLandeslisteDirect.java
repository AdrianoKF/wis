package edu.tum.cs.wis.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "Zweitstimmen_Direkt")
public class ZweitstimmenLandeslisteDirect implements IZweitstimmenLandesliste{
	private static final long serialVersionUID = 7768082963938763737L;

	@Id
	@ManyToOne
	@JoinColumn(name = "Landesliste")
	private Landesliste landesliste;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "Wahlkreis")
	private Wahlkreis wahlkreis;
	
	@Id
	private Integer jahr;
	
	private Integer stimmen;

	public Landesliste getLandesliste() {
		return landesliste;
	}

	public void setLandesliste(Landesliste landesliste) {
		this.landesliste = landesliste;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
	
	public Integer getStimmen() {
		return stimmen;
	}

	public void setStimmen(Integer stimmen) {
		this.stimmen = stimmen;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}
}
