package edu.tum.cs.wis.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(schema = "wis", name = "Waehler")
public class Waehler {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Temporal(TemporalType.DATE)
	private Date geburtstag;

	@Temporal(TemporalType.TIMESTAMP)
	private Date abgestimmt;

	@Column(length = 128)
	private String token;

	@ManyToOne
	@JoinColumn(name = "Wahlkreis")
	private Wahlkreis wahlkreis;

	private String name;
	private String vorname;

	public String getFullName() {
		return StringUtils.normalizeSpace(String.format("%s, %s", name, vorname));
	}

	@Override
	public String toString() {
		return String.format("[ Waehler, id=%d ] %s (* %tD)", id, getFullName(), geburtstag);
	}

	public synchronized boolean validateVotingPermission(String token) {
		boolean permitted = false;

		if (abgestimmt == null && this.token.equals(token)) {
			permitted = true;
		}

		return permitted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Date getGeburtstag() {
		return geburtstag;
	}

	public void setGeburtstag(Date geburtstag) {
		this.geburtstag = geburtstag;
	}

	public Date getAbgestimmt() {
		return abgestimmt;
	}

	public void setAbgestimmt(Date abgestimmt) {
		this.abgestimmt = abgestimmt;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((geburtstag == null) ? 0 : geburtstag.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		result = prime * result + ((wahlkreis == null) ? 0 : wahlkreis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Waehler)) {
			return false;
		}
		Waehler other = (Waehler) obj;
		if (geburtstag == null) {
			if (other.geburtstag != null) {
				return false;
			}
		} else if (!geburtstag.equals(other.geburtstag)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (vorname == null) {
			if (other.vorname != null) {
				return false;
			}
		} else if (!vorname.equals(other.vorname)) {
			return false;
		}
		if (wahlkreis == null) {
			if (other.wahlkreis != null) {
				return false;
			}
		} else if (!wahlkreis.equals(other.wahlkreis)) {
			return false;
		}
		return true;
	}
}
