package edu.tum.cs.wis.model;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(schema = "WIS", name = "Landesliste")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Cacheable
public class Landesliste {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="Bundesland")
	private Bundesland bundesland;
	
	@ManyToOne
	@JoinColumn(name="Partei")
	private Partei partei;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "Landesliste")
	private Set<Listenkandidat> kandidaten;	
	
	private Integer jahr;
	
	@Override
	public String toString() {
		return String.format("[ Landesliste, id=%d, jahr=%d ], %s, %d Kandidaten", id, jahr, partei, kandidaten.size());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bundesland getBundesland() {
		return bundesland;
	}

	public void setBundesland(Bundesland bundesland) {
		this.bundesland = bundesland;
	}
	
	public String getParteiName() {
		Partei p = getPartei();
		if (p == null) {
			return "";
		}
		return p.getName();
	}

	public Partei getPartei() {
		return partei;
	}

	public void setPartei(Partei partei) {
		this.partei = partei;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

	public Set<Listenkandidat> getKandidaten() {
		return kandidaten;
	}

	public void setKandidaten(Set<Listenkandidat> kandidaten) {
		this.kandidaten = kandidaten;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bundesland == null) ? 0 : bundesland.hashCode());
		result = prime * result + ((jahr == null) ? 0 : jahr.hashCode());
		result = prime * result + ((partei == null) ? 0 : partei.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Landesliste)) {
			return false;
		}
		Landesliste other = (Landesliste) obj;
		if (bundesland == null) {
			if (other.bundesland != null) {
				return false;
			}
		} else if (!bundesland.equals(other.getBundesland())) {
			return false;
		}
		if (jahr == null) {
			if (other.jahr != null) {
				return false;
			}
		} else if (!jahr.equals(other.jahr)) {
			return false;
		}
		if (partei == null) {
			if (other.partei != null) {
				return false;
			}
		} else if (!partei.equals(other.partei)) {
			return false;
		}
		return true;
	}
}
