package edu.tum.cs.wis.dao;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory = buildSessionFactory();
	
	protected HibernateUtil() {
		
	}
	
	private static Configuration cfg;
	
	private static void configure(){
		// Create the SessionFactory from hibernate.cfg.xml
		cfg = new Configuration();
		//HibernateProfiler.configure(cfg);
		cfg =  cfg.configure();
	}
	
	private static SessionFactory buildSessionFactory() {
		try {
			configure();
			SessionFactory sf = cfg.buildSessionFactory();
			return sf;
		} catch (Exception ex) {
			// Make sure you log the exception, as it might be swallowed
			Logger.getLogger(HibernateUtil.class).error("Initial SessionFactory creation failed.", ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static SessionFactory createSessionFactory(){
		return cfg.buildSessionFactory();
	}
}