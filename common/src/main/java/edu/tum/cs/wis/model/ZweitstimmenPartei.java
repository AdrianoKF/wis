package edu.tum.cs.wis.model;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "ZweitstimmenPartei")
public class ZweitstimmenPartei implements Serializable {
	private static final long serialVersionUID = 6293531686986940484L;

	@Id
	@ManyToOne
	@JoinColumn(name = "Partei")
	private Partei partei;

	@Id
	private Integer jahr;
	
	private Integer stimmen;
	
	public Partei getPartei() {
		return partei;
	}

	public void setPartei(Partei partei) {
		this.partei = partei;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
	
	public Integer getStimmen() {
		return stimmen;
	}

	public void setStimmen(Integer stimmen) {
		this.stimmen = stimmen;
	}
}
