package edu.tum.cs.wis.model;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "Wahlzettel")
@Cacheable(false)
public class Wahlzettel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Direktkandidat")
	private Direktkandidat kandidat;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Landesliste")
	private Landesliste landesliste;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Wahlkreis")
	private Wahlkreis wahlkreis;
	
	private Integer jahr;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Direktkandidat getKandidat() {
		return kandidat;
	}

	public void setKandidat(Direktkandidat kandidat) {
		this.kandidat = kandidat;
	}

	public Landesliste getLandesliste() {
		return landesliste;
	}

	public void setLandesliste(Landesliste landesliste) {
		this.landesliste = landesliste;
	}

	public Wahlkreis getWahlkreis() {
		return wahlkreis;
	}

	public void setWahlkreis(Wahlkreis wahlkreis) {
		this.wahlkreis = wahlkreis;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
}
