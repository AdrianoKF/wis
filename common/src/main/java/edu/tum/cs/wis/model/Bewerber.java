package edu.tum.cs.wis.model;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(schema = "wis", name = "Bewerber")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Cacheable
public class Bewerber {
	@Id
	@GeneratedValue
	protected Long id;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bewerber")
	private Set<Parteizugehoerigkeit> parteien;

	private String name;
	private String vorname;
	private String titel;
	private Integer geburtsjahr;
	private String beruf;

	@Override
	public String toString() {
		return String.format("[ Bewerber, id=%d ] %s (* %d)", id, getFullName(), geburtsjahr);
	}

	public String getFullName() {
		return StringUtils.normalizeSpace(String.format("%s, %s %s", name, titel, vorname));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Integer getGeburtsjahr() {
		return geburtsjahr;
	}

	public void setGeburtsjahr(Integer geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}

	public String getBeruf() {
		return beruf;
	}

	public void setBeruf(String beruf) {
		this.beruf = beruf;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getParteiName(int jahr) {
		Partei p = getPartei(jahr);
		if (p != null) {
			return p.getName();
		} else {
			return "parteilos";
		}
	}

	public Partei getPartei(int jahr) {
		for (Parteizugehoerigkeit pz : parteien) {
			if (pz.getJahr() == jahr) {
				return pz.getPartei();
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((geburtsjahr == null) ? 0 : geburtsjahr.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof Bewerber)) {
			return false;
		}
		Bewerber other = (Bewerber) obj;
		if (geburtsjahr == null) {
			if (other.geburtsjahr != null) {
				return false;
			}
		} else if (!geburtsjahr.equals(other.geburtsjahr)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (vorname == null) {
			if (other.vorname != null) {
				return false;
			}
		} else if (!vorname.equals(other.vorname)) {
			return false;
		}
		return true;
	}
}
