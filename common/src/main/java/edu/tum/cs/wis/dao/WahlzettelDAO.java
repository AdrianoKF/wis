package edu.tum.cs.wis.dao;

import edu.tum.cs.wis.model.Wahlzettel;

public class WahlzettelDAO extends GenericDAO<Wahlzettel, Long> {
	public WahlzettelDAO() {
		super(Wahlzettel.class);
	}
}
