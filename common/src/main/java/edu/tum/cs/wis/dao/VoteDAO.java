package edu.tum.cs.wis.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import edu.tum.cs.wis.model.Bewerber;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Direktkandidat;
import edu.tum.cs.wis.model.IErststimmen;
import edu.tum.cs.wis.model.IZweitstimmenLandesliste;
import edu.tum.cs.wis.model.Landesliste;
import edu.tum.cs.wis.model.Mandat;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Quadruple;
import edu.tum.cs.wis.model.Waehler;
import edu.tum.cs.wis.model.Wahlkreis;
import edu.tum.cs.wis.model.ZweitstimmenSieger;

public interface VoteDAO {

	/**
	 * Liefert alle in der Datenbank gespeicherten Direktkandidaten für ein
	 * Wahljahr
	 * 
	 * @param year
	 * @return Liste der Direktkandidaten
	 */
	public List<Direktkandidat> getDirektkandidaten(int year);

	/**
	 * Liefert alle in der Datenbank gespeicherten Wahlkreise
	 * 
	 * @return Liste der Wahlkreise
	 */
	public List<Wahlkreis> getWahlkreise();

	/**
	 * Liefert alle in der Datenbank gespeicherten Bundesländer.
	 * 
	 * @return Liste der Bundesländer
	 */
	public List<Bundesland> getBundeslaender();

	/**
	 * Liefert alle in der Datenbank gespeicherten Parteien
	 * 
	 * @return Liste der Parteien
	 */
	public List<Partei> getParteien();

	/**
	 * Liefert die Zuordnung von Bewerbern zu ihren jeweiligen Parteien in einem
	 * Wahljahr
	 * 
	 * @param jahr
	 * @return Zuordnung zwischen Bewerbern und Parteien
	 */
	public Map<Bewerber, Partei> getParteizugehoerigkeiten(int jahr);

	/**
	 * Ermittelt einen Wahlkreis anhand seiner Wahlkreisnummer.
	 * 
	 * @param id
	 * @return Der gesuchte Wahlkreis, <code>null</code> falls Nummer ungültig
	 */
	public Wahlkreis getWahlkreis(Long id);

	/**
	 * Ermittelt einen Wahlkreis anhand seines Namens
	 * 
	 * @param name
	 * @return Der gefundene Wahlkreis, <code>null</code> anderenfalls
	 */
	public Wahlkreis findWahlkreisByName(String name);

	/**
	 * Liefert alle Direktkandidaten eines Wahlkreises für das ausgewählte
	 * Wahljahr
	 * 
	 * @param wahlkreis
	 * @param jahr
	 * @return Liste der Direktkandidaten
	 */
	public List<Direktkandidat> getDirektkandidaten(Wahlkreis wahlkreis, int jahr);

	/**
	 * Ermittelt eine Landesliste anhand der zugehörigen Partei, Bundesland und
	 * Wahljahr
	 * 
	 * @return Die den Kriterien entsprechende Landesliste, <code>null</code>
	 *         anderenfalls
	 */
	public Landesliste findLandesliste(Partei partei, Bundesland bundesland, int jahr);

	/**
	 * Liefert alle Landeslisten zurück, die für einen Wahlkreis in einem
	 * Wahljahr relevant sind.
	 * 
	 * @return Die Landeslisten, die den gewählten Kriterien entsprechen
	 */
	public List<Landesliste> getLandeslisten(Wahlkreis wahlkreis, int jahr);

	/**
	 * Liefert die Anzahl an Zweitstimmen für eine Partei in einem Wahljahr
	 * zurück. Bei ungültigen Parametern wird <code>-1</code> zurückgeliefert.
	 * 
	 * @param partei
	 * @param jahr
	 * @return Die Anzahl der Zweitstimmen, <code>-1</code> im Fehlerfall
	 */
	public int getZweitstimmen(Partei partei, int jahr);

	/**
	 * Liefert die Zuordnung zwischen Parteien und Zweitstimmenergebnissen fuer
	 * ein Wahljahr
	 * 
	 * @param jahr
	 * @return
	 */
	public Map<Partei, Integer> getAllZweitstimmen(int jahr);

	/**
	 * Liefert alle Mitglieder des Bundestages in einem Wahljahr.
	 * 
	 * @param jahr
	 * @return
	 */
	public List<Mandat> getAbgeordnete(int jahr);

	/**
	 * Gibt die aggregierten Zweitstimmen pro Landesliste in einem Wahljahr und
	 * Wahlkreis zurück.
	 * 
	 * @param wahlkreis
	 * @param jahr
	 * @return
	 */
	public List<IZweitstimmenLandesliste> fetchZweitstimmen(Wahlkreis wahlkreis, int jahr);

	/**
	 * Gibt die aggregierten Erststimmen für Direktkandidaten pro Wahlkreis und
	 * Wahljahr zurück.
	 * 
	 * @param wahlkreis
	 * @param jahr
	 * @return
	 */
	public List<IErststimmen> fetchErststimmen(Wahlkreis wahlkreis, int jahr);

	/**
	 * Fetch Überhangmandate per party (only parties with at least one
	 * Überhangmandat is returned)
	 * 
	 * @param jahr
	 * @return
	 */
	public Map<Partei, Integer> getUeberhangmandateProPartei(int jahr);

	/**
	 * Fetch Überhangmandate per Bundesland (only Bundesländer with at least one
	 * Überhangmandat is returned)
	 * 
	 * @param jahr
	 * @return
	 */
	public Map<Bundesland, Integer> getUeberhangmandateProBundesland(int jahr,
			boolean returnAlsoEmpty);

	/**
	 * Fetch knappste Gewinner -> See assignement
	 * 
	 * @param jahr
	 * @return
	 */
	public List<Quadruple<Bewerber, Partei, BigInteger, Integer>> getKnappsteGewinner(int jahr);

	/**
	 * 
	 * @param year
	 * @return
	 */
	public Map<Partei, Integer> getTotalSeating(int year);

	/**
	 * Liefert die Wahlbeteiligung in einem Wahlkreis und einem Wahljahr.
	 * 
	 * @param wahlkreis
	 * @param year
	 * @return
	 */
	public double getWahlbeteiligung(Wahlkreis wahlkreis, int year);

	/**
	 * Liefert die erfolgreichste Landesliste pro Wahlkreis in einem Wahljahr.
	 * @param year
	 * @return
	 */
	public List<ZweitstimmenSieger> getZweitstimmenSieger(int year);

	/**
	 * Liefert die erfolgreichste Landesliste eines Wahlkreises in einem Wahljahr.
	 * @param year
	 * @return
	 */
	ZweitstimmenSieger getZweitstimmenSieger(int year, Wahlkreis kreis);

	/**
	 * Fügt in einem Wahlkreis abgegebene Erst- und Zweitstimmen in die Datenbasis ein.
	 * @param year
	 * @param wahlkreis
	 * @param direktkandidat
	 * @param landesliste
	 */
	void addStimmen(int year, Wahlkreis wahlkreis,
			Direktkandidat direktkandidat, Landesliste landesliste,
			Waehler waehler);
}