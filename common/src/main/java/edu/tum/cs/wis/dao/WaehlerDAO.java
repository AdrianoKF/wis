package edu.tum.cs.wis.dao;

import edu.tum.cs.wis.model.Waehler;

public class WaehlerDAO extends GenericDAO<Waehler, Long> {
	public WaehlerDAO() {
		super(Waehler.class);
	}
}
