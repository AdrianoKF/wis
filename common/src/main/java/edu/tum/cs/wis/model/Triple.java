package edu.tum.cs.wis.model;

/**
 * Class represents Triples of different types
 * 
 * @author wolfst
 * 
 * @param <A>
 * @param <B>
 * @param <C>
 */
public class Triple<A, B, C> {
	private A one;
	private B two;
	private C three;

	/**
	 * Constructor: Pass an object array of at least 3 elements. Implicit cast.
	 */
	@SuppressWarnings("unchecked")
	public Triple(Object[] in) {
		one = (A) in[0];
		two = (B) in[1];
		three = (C) in[2];

	}

	public Triple(A a, B b, C c) {
		one = a;
		two = b;
		three = c;
	}

	// Setter and getter
	public void setFirst(A one) {
		this.one = one;
	}

	public void setSecond(B two) {
		this.two = two;
	}

	public void setThird(C three) {
		this.three = three;
	}

	public A getFirst() {
		return one;
	}

	public B getSecond() {
		return two;
	}

	public C getThird() {
		return three;
	}

}
