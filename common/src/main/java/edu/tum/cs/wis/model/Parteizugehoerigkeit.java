package edu.tum.cs.wis.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "WIS", name = "Parteizugehoerigkeit")
public class Parteizugehoerigkeit {
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Bewerber", nullable = false, updatable = false)
	private Bewerber bewerber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Partei", nullable = false, updatable = false)
	private Partei partei;

	private Integer jahr;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bewerber getBewerber() {
		return bewerber;
	}

	public void setBewerber(Bewerber bewerber) {
		this.bewerber = bewerber;
	}

	public Partei getPartei() {
		return partei;
	}

	public void setPartei(Partei partei) {
		this.partei = partei;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
}
