package edu.tum.cs.wis.model;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(schema = "WIS", name = "Wahlkreis")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Cacheable
public class Wahlkreis {
	@Id
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Bundesland")
	private Bundesland bundesland;

	private String name;
	private Integer flaeche;
	private Integer bevoelkerung;
	private Integer wahlberechtigte;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(schema = "WIS", name = "Direktmandat", joinColumns = @JoinColumn(name = "Wahlkreis"), inverseJoinColumns = @JoinColumn(name = "Direktkandidat"))
	private Set<Direktkandidat> direktmandate;

	@Override
	public String toString() {
		return String.format("[ Wahlkreis, id=%d ] %s", id, name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Bundesland getBundesland() {
		return bundesland;
	}

	public void setBundesland(Bundesland bundesland) {
		this.bundesland = bundesland;
	}

	public Integer getFlaeche() {
		return flaeche;
	}

	public void setFlaeche(Integer flaeche) {
		this.flaeche = flaeche;
	}

	public Integer getBevoelkerung() {
		return bevoelkerung;
	}

	public Integer getWahlberechtigte() {
		return wahlberechtigte;
	}

	public void setWahlberechtigte(Integer wahlberechtigte) {
		this.wahlberechtigte = wahlberechtigte;
	}

	public void setBevoelkerung(Integer bevoelkerung) {
		this.bevoelkerung = bevoelkerung;
	}

	public Set<Direktkandidat> getDirektmandate() {
		return direktmandate;
	}

	public Direktkandidat getDirektmandat(int year) {
		for (Direktkandidat dk : direktmandate) {
			if (dk.getJahr() == year) {
				return dk;
			}
		}
		return null;
	}

	public Partei getDirektmandatPartei(int year) {
		Direktkandidat dk = getDirektmandat(year);
		if (dk == null) {
			return null;
		}
		return dk.getBewerber().getPartei(year);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Wahlkreis)) {
			return false;
		}
		Wahlkreis other = (Wahlkreis) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
