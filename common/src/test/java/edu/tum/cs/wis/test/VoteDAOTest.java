package edu.tum.cs.wis.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.ibm.db2.jcc.am.me;

import edu.tum.cs.wis.dao.DefaultVoteDAO;
import edu.tum.cs.wis.dao.VoteDAO;
import edu.tum.cs.wis.model.Bundesland;
import edu.tum.cs.wis.model.Mandat;
import edu.tum.cs.wis.model.Partei;
import edu.tum.cs.wis.model.Wahlkreis;

import au.com.bytecode.opencsv.CSVReader;

public class VoteDAOTest {
	private VoteDAO db;	

	@Before
	public void setUp() throws Exception {
		db = new DefaultVoteDAO();
	}

	@Test
	public void testUeberhangmandatePartei2009(){
		Map<Partei, Integer> map = db.getUeberhangmandateProPartei(2009);
		assertEquals(2, map.size());
		for(Partei key : map.keySet()){
			if(key.getName().equals("CSU"))
				assertEquals(Integer.valueOf(3), map.get(key));
			else if(key.getName().equals("CDU"))
				assertEquals(Integer.valueOf(21), map.get(key));
			else 
				fail("Bundesland may not have Überhangmandate: '"+key.getName()+"'");
		}
	}
	
	@Test
	public void testUeberhangmandateBL2009(){
		Map<Bundesland, Integer> map = db.getUeberhangmandateProBundesland(2009, false);
		assertEquals(8, map.size());
		for(Bundesland key : map.keySet()){
			if(key.getName().equals("Baden-Württemberg"))
				assertEquals(Integer.valueOf(10), map.get(key));
			else if(key.getName().equals("Sachsen"))
				assertEquals(Integer.valueOf(4), map.get(key));
			else if(key.getName().equals("Bayern"))
				assertEquals(Integer.valueOf(3), map.get(key));
			else if(key.getName().equals("Rheinland-Pfalz"))
				assertEquals(Integer.valueOf(2), map.get(key));
			else if(key.getName().equals("Mecklenburg-Vorpommern"))
				assertEquals(Integer.valueOf(2), map.get(key));
			else if(key.getName().equals("Schleswig-Holstein"))
				assertEquals(Integer.valueOf(1), map.get(key));
			else if(key.getName().equals("Thüringen"))
				assertEquals(Integer.valueOf(1), map.get(key));
			else if(key.getName().equals("Saarland"))
				assertEquals(Integer.valueOf(1), map.get(key));
			else 
				fail("Bundesland may not have Überhangmandate: '"+key.getName()+"'");
		}
	}
	
	@Test
	public void testMandats2009() throws IOException {
		
		// Fetch correct members of parliament
		CSVReader reader = new CSVReader(new BufferedReader(new InputStreamReader(
				new FileInputStream("data/mdb.csv"), "ISO-8859-1")),';');
	    String [] nextLine;
	    List<String> members = new ArrayList<String>();
	    while ((nextLine = reader.readNext()) != null) {
	        members.add(nextLine[1]);
	    }
	    
	    // Fetch calculated members of parliament
	    List<Mandat> result = db.getAbgeordnete(2009);
	    
	    // Extract names
	    List<String> calcMembers = new ArrayList<String>();
	    for(Mandat m : result){
	    	if(m.getBewerber().getTitel().length() >0)
	    		calcMembers.add(m.getBewerber().getName()+", " +m.getBewerber().getTitel()+" "+m.getBewerber().getVorname()+" ");
	    	else
	    		calcMembers.add(m.getBewerber().getName()+", " +m.getBewerber().getVorname()+" ");
	    }
	    System.out.println("test");
	    // Sort
	    Collections.sort(calcMembers);
	    Collections.sort(members);
	    
	    // Check size
	    assertEquals(622, result.size());
	    assertEquals(calcMembers.size(), members.size());
	    for(int i = 0; i<members.size(); i++){
	    	assertEquals(members.get(i), calcMembers.get(i));
	    }
	    
		
	    System.out.println("finished");
	}
	
	@Test
	public void testMandatesPerParty2009() {
		Map<Partei, Integer> result = db.getTotalSeating(2009);
		assertEquals("#(Parties) in BT", 6, result.size());
		
		for (Entry<Partei, Integer> e : result.entrySet()) {
			final String name = e.getKey().getName();
			final int seats = e.getValue();
			
			if ("CDU".equals(name)) {
				assertEquals(194, seats);
			} else if ("CSU".equals(name)) {
				assertEquals(45, seats);
			} else if ("FDP".equals(name)) {
				assertEquals(93, seats);
			} else if ("SPD".equals(name)) {
				assertEquals(146, seats);
			} else if ("DIE LINKE".equals(name)) {
				assertEquals(76, seats);
			} else if ("GRÜNE".equals(name)) {
				assertEquals(68, seats);
			} else {
				fail("Invalid party: " + name);
			}
		}
	}
	
	@Test
	@Ignore
	public void testTotalMandateCount2005() {
		List<Mandat> result = db.getAbgeordnete(2005);
		assertEquals(614, result.size());
	}
	
	@Test
	@Ignore
	public void testMandatesPerParty2005() {
		Map<Partei, Integer> result = db.getTotalSeating(2005);
		assertEquals("#(Parties) in BT", 6, result.size());
		
		for (Entry<Partei, Integer> e : result.entrySet()) {
			final String name = e.getKey().getName();
			final int seats = e.getValue();
			
			if ("CDU".equals(name)) {
				assertEquals(180, seats);
			} else if ("CSU".equals(name)) {
				assertEquals(46, seats);
			} else if ("FDP".equals(name)) {
				assertEquals(61, seats);
			} else if ("SPD".equals(name)) {
				assertEquals(222, seats);
			} else if ("DIE LINKE".equals(name)) {
				assertEquals(54, seats);
			} else if ("GRÜNE".equals(name)) {
				assertEquals(51, seats);
			} else {
				fail("Invalid party: " + name);
			}
		}
	}
	
	@Test
	public void testTurnout2009() {
		Wahlkreis kreis = db.getWahlkreis(257L);
		double turnout = db.getWahlbeteiligung(kreis, 2009);
		
		assertEquals("Turnout in district #257 ", 0.70, turnout, 0.01);
	}
	
	@Test
	public void testTurnout2005() {
		Wahlkreis kreis = db.getWahlkreis(257L);
		double turnout = db.getWahlbeteiligung(kreis, 2005);
		
		assertEquals("Turnout in district #257 ", 0.77, turnout, 0.05);
	}
}
