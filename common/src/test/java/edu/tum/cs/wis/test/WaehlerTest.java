package edu.tum.cs.wis.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.tum.cs.wis.model.Waehler;

public class WaehlerTest {
	private Waehler waehler;
	
	@Before
	public void setUp() {
		waehler = new Waehler();
		waehler.setName("Foo");
		waehler.setVorname("Bar");
		try {
			waehler.setGeburtstag(DateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMAN).parse("15.04.1980"));
		} catch (ParseException e) {
			waehler.setGeburtstag(new Date());
		}
		waehler.setToken("QUIEK");
	}
	
	@Test
	public void testValidVotingPermission() {
		waehler.setAbgestimmt(null);
		Assert.assertTrue(waehler.validateVotingPermission(waehler.getToken()));
	}
	
	@Test
	public void testInvalidVotingPermissionDate() {
		waehler.setAbgestimmt(new Date());
		Assert.assertFalse(waehler.validateVotingPermission(waehler.getToken()));
	}
	
	@Test
	public void testInvalidVotingPermissionToken() {
		waehler.setAbgestimmt(null);
		Assert.assertFalse(waehler.validateVotingPermission(""));
	}
}
